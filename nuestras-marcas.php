<?php include('includes/header.php'); ?>


<section class="container">
	<br><br>
	<?php include('includes/aside.php'); ?>

	<section class="columns eleven"><br>
		<h1>¿NUESTRAS MARCAS?</h1>
		<p>Sundec Decoración en Cancún, es una empresa mexicana establecida en la ciudad de Cancún, Q. Roo, dedicada a la 
		venta de productos de alta calidad que cubran las necesidades más exigentes para el mercado de la decoración 
		residencial, comercial y hotelera, buscando siempre la satisfacción plena del usuario final, a través de dicha 
		calidad y un excelente servicio.</p>
		<p>Sundec Decoración cuenta con varios Productos y Servicios para la Decoración de su casa u Hotel en Cancún, 
		entre los cuales destacan sus Cortinas y Persianas que pueden ser colocadas en cualquier establecimiento de Cancún. 
		Cuentan con Persianas anticiclónicas para ser instaladas cuanddo lo deseen.</p>
		<p>Entre nuestras especialidades de Decoración, contamos con lámparas, candiles, pisos de diferentes materiales, elegantes toldos, alfombras para todo tipo de espacios y una gran variedad de persianas y cortinas. Solo distribuimos productos de alta calidad, por lo que las marcas son sometidas a una evaluación y son seleccionadas cuidadosamente para ofrecerles lo mejor a nuestros clientes.</p>
		<br>
		
		<div class="row listBrands">
			<?php $brands = new Brands(); ?>
			<?php echo $brands->viewBrands(); ?>
		</div>

	</section>
	<div class="clr"></div>
	<br><br><br><br>

</section>
	
<?php include('includes/footer.php'); ?>