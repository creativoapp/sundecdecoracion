<?php
    ob_start(); 
    include('path.php'); 
    include('models/mSlider.php');
    include('models/mSettings.php');
    include('models/mSeo.php');
    include('models/mCatalogo.php');
    include('models/mGalerias.php');
    include('models/mBrands.php');
    include('models/mPromos.php');

    $dates = new Setting();
    $settings = json_decode($dates->showDates());
    $contacto = json_decode($dates->showContacto());
    

    //maniobra web cancun
    //$eroute = explode('/', $route);
    //$nroute = '/'.$eroute[2];

    if($route == '/catalog-page.php'){
        $destroy = explode('/', $_SERVER['QUERY_STRING']);
        $route = '/catalogo/'.$destroy[1];
        //$nroute = '/catalogo/'.$destroy[1]; //webcancun
    }

    

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <?php 
        $seo = new Seo();
        $metas = json_decode($seo->getSeo($route));
        if($metas->{'exist'} == 'true')
        {
    ?>
    <title><?php echo $metas->{'title'}; ?></title>
    <meta name="keywords" content="<?php echo $metas->{'keywords'}; ?>" />
    <meta name="description" content="<?php echo $metas->{'description'}; ?>" />
    <?php 
        }
        else
        {
            //header("Location:404.php");
        }
    ?>

    <meta name="viewport" content="initial-scale=1, maximum-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $path.'sources/favicon.ico'; ?>" />
  
    <!--CSS
    ========================== -->
    <link href="<?php echo $path.'css/pure.css';?>" rel="stylesheet" />
    <link href="<?php echo $path.'css/site.css'; ?>" rel="stylesheet" />
    <link href="<?php echo $path.'css/nivo-slider.css'; ?>" rel="stylesheet" />
    <link href="<?php echo $path.'css/jquery.fancybox.css'; ?>" rel="stylesheet" />

    <!--JS
    ========================== -->
    <script type="text/javascript" src="<?php echo $path.'js/jquery-1.8.3.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'js/jquery.nivo.slider.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'js/galleria-1.3.5.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'js/local.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'js/jquery.fancybox.pack.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'js/gmap3.min.js'; ?>"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
    <script type="text/javascript">
        $(window).load(function() {
            $('#slider').nivoSlider({
                effect: 'fade',
                animSpeed: 300,                 // Slide transition speed
                pauseTime: 5000,
                controlNav: false
            });
        });

        $(document).ready(function(){
            var lat = "<?php echo $contacto->{'latCont'}; ?>";
            var lon = "<?php echo $contacto->{'longCont'}; ?>";
            $("#contact-map").gmap3({
                 map: {
                    options: {
                        center: [lat, lon],
                        zoom: 11
                    }  
                },
                marker:{
                    latLng:[lat, lon]
                }
            });


            $(".fancybox").fancybox();

        });
    </script>

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>
    <header>
        <section class="container">
            <div class="columns five divHeaders">
                <a href="<?php echo $path; ?>">
                    <img src="<?php echo $path.'sources/logo.png'; ?>">
                </a>
            </div>
            <div class="columns eleven">
                <div class="rownex-h">
                    <div class="columns seven">
                        <div class="header-contact">
                            <img src="<?php echo $path.'sources/mail-icon.png'; ?>">
                            <span><?php echo $settings->{'mailH'};?></span>
                            <div class="clr"></div>
                        </div>
                        <div class="header-phone">
                            <img src="<?php echo $path.'sources/phone-icon.png'; ?>">
                            <span><?php echo $settings->{'phoneH'};?></span>
                            <div class="clr"></div>
                        </div>
                        <div class="clr"></div>
                    </div>
                    <div class="columns three nomrg-r">
                        <div class="box-iconsocial">
                            <a href="#" class="icon-social" title="Síguenos en Facebook"><img src="<?php echo $path.'sources/facebook.png'; ?>"></a>
                            <a href="#" class="icon-social" title="Síguenos en Twitter"><img src="<?php echo $path.'sources/twitter.png'; ?>"></a>
                            <a href="#" class="icon-social" title="Síguenos en Pinterest"><img src="<?php echo $path.'sources/pinterest.png'; ?>"></a>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>

                <nav>
                    <!--<div class="boxNavMobile">
                        <select id="navmobile">
                            <option data-navmobile="/" <?php if($page == 'index.php'){?> selected <?php } ?>>INICIO</option>
                            <option data-navmobile="/luxury-yachts" <?php if($page == 'luxury-yachts.php'){?> selected <?php } ?>>QUIENES SOMOS</option>
                            <option data-navmobile="/fishing-charters" <?php if($page == 'fishing-charters.php'){?> selected <?php } ?>>CATALOGO</option>
                            <option data-navmobile="/group-yachts" <?php if($page == 'group-yachts.php'){?> selected <?php } ?>>PROMOCIONES</option>
                            <option data-navmobile="/catamarans" <?php if($page == 'catamarans.php'){?> selected <?php } ?>>CONTACTO</option>
                        </select>
                    </div>-->
                    <ul class="menu">
                        <li><a href="<?php echo $path; ?>" <?php if($page == 'index.php'){?> class="current" <?php } ?>>INICIO</a></li>
                        <li class="diagseparator">/</li>
                        <li><a href="<?php echo $path.'quienes-somos'; ?>" <?php if($page == 'quienes-somos.php'){?> class="current" <?php } ?>>QUIENES SOMOS</a></li>
                        <li class="diagseparator">/</li>
                        <li><a href="<?php echo $path.'catalogo'; ?>" <?php if($page == 'catalogo.php'){?> class="current" <?php } ?>>CATALOGO</a></li>
                        <li class="diagseparator">/</li>
                        <li><a href="<?php echo $path.'nuestras-marcas'; ?>" <?php if($page == 'nuestras-marcas.php'){?> class="current" <?php } ?>>NUESTRAS MARCAS</a></li>
                        <li class="diagseparator">/</li>
                        <li><a href="<?php echo $path.'promociones'; ?>" <?php if($page == 'promociones.php'){?> class="current" <?php } ?>>PROMOCIONES</a></li>
                        <li class="diagseparator">/</li>
                        <li><a href="<?php echo $path.'contacto'; ?>" <?php if($page == 'contacto.php'){?> class="current" <?php } ?>>CONTACTO</a></li>
                    </ul>
                </nav>
            </div>
            
        </section>
    </header>