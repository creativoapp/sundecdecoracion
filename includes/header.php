<?php
    ob_start(); 
    include('path.php'); 
    include('models/mSlider.php');
    include('models/mSettings.php');
    include('models/mSeo.php');
    include('models/mCatalogo.php');
    include('models/mGalerias.php');
    include('models/mBrands.php');
    include('models/mPromos.php');
    include('models/mProjects.php');
    include('models/mCities.php');

    $dates = new Setting();
    $settings = json_decode($dates->showDates());
    $contacto = json_decode($dates->showContacto());
    

    //maniobra web cancun
    //$eroute = explode('/', $route);
    //$nroute = '/'.$eroute[2];

    if($route == '/catalog-page.php'){
        $destroy = explode('/', $_SERVER['QUERY_STRING']);
        $destroy = explode('&', $destroy[0]);
        $param_city = str_replace('city=', '/', $destroy[0]);
        $param_category = str_replace('.php', '', str_replace('categoria=', '/', $destroy[1]));
        if ($param_city == '/cancun') {
            $route = '/catalogo'.$param_category;
        } else {
            $route = $param_city.'/catalogo'.$param_category;
        }
        $mCity = new Cities();
        //Validar Ciudad
        $city_obj = $mCity->ValidateCity($param_city);
        if ($city_obj['status'] == false) {
            //header("Location:/404");
        }
    }

    if($route == '/catalogo.php'){
        $destroy = explode('/', $_SERVER['QUERY_STRING']);
        if (isset($destroy[0]) && trim($destroy[0]) != '') {
            $param_city = str_replace('city=', '/', $destroy[0]);
            //$route = $param_city.'/catalogo.php';
        } else {
            $param_city = '/cancun';
        }
        $mCity = new Cities();
        //Validar Ciudad
        $city_obj = $mCity->ValidateCity($param_city);
        if ($city_obj['status'] == false) {
            //header("Location:/404");
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <?php 
        $seo = new Seo();
        $metas = json_decode($seo->getSeo($route));
        if($metas->{'exist'} == 'true')
        {
    ?>
    <title><?php echo $metas->{'title'}; ?></title>
    <meta name="keywords" content="<?php echo $metas->{'keywords'}; ?>" />
    <meta name="description" content="<?php echo $metas->{'description'}; ?>" />
    <?php 
        }
        else
        {
            //header("Location:/404");
        }
    ?>

    <meta name="viewport" content="initial-scale=1, maximum-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $path.'sources/favicon.ico'; ?>" />
  
    <!--CSS
    ========================== -->
    <link href="<?php echo $path.'css/pure.base.min.css';?>" rel="stylesheet" />
    <!--<link href="<?php echo $path.'css/font.awesome.min.css';?>" rel="stylesheet" />-->
    <link href="<?php echo $path.'css/site.css'; ?>" rel="stylesheet" />
    <link href="<?php echo $path.'css/nivo-slider.css'; ?>" rel="stylesheet" />
    <link href="<?php echo $path.'css/jquery.fancybox.css'; ?>" rel="stylesheet" />

    <!--JS
    ========================== -->
    <script type="text/javascript" src="<?php echo $path.'js/jquery-1.8.3.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'js/jquery.nivo.slider.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'js/galleria-1.3.5.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'js/local.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'js/jquery.fancybox.pack.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'js/gmap3.min.js'; ?>"></script>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
    <script type="text/javascript">
        $(window).load(function() {
            $('#slider').nivoSlider({
                effect: 'fade',
                animSpeed: 300,                 // Slide transition speed
                pauseTime: 5000,
                controlNav: false
            });
        });

        $(document).ready(function(){
            var lat = "<?php echo $contacto->{'latCont'}; ?>";
            var lon = "<?php echo $contacto->{'longCont'}; ?>";
            $("#contact-map").gmap3({
                 map: {
                    options: {
                        center: [lat, lon],
                        zoom: 15
                    }  
                },
                marker:{
                    latLng:[lat, lon]
                }
            });


            $(".fancybox").fancybox();

        });
    </script>

    <script src="https://kit.fontawesome.com/94233a6903.js" crossorigin="anonymous"></script>

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>
    <header>
        <section class="container">
            
            <div class="columns five divHeaders">
                <a href="<?php echo $path; ?>">
                    <img title="Sundec Cancún | Soluciones Decorativas" alt="Decoración en Cancún, empresa de Soluciones Decorativas" src="<?php echo $path.'sources/sundec-decoracion-cancun.png'; ?>">
                </a>
            </div>
            
            <div class="columns eleven boxSocial">
                <div class="box">
                    <a href="mailto:<?php echo $settings->{'mailH'};?>">
                        <i class="far fa-envelope"></i>
                        <?php echo $settings->{'mailH'};?>
                    </a>
                    <a href="tel:9982149274"><i class="fas fa-fax"></i> (998) 214 9274<!--<?php echo $settings->{'phoneH'};?>--></a>
                    <a href="https://wa.me/+19982149274?text=Hola,%20estoy%20interesado%20en%20sus%20productos" target="_blank"><i class="fab fa-whatsapp"></i> (998) 214 9274</a>
                </div>
                <div class="box redes">
                    <a href="https://www.facebook.com/sundecdecoracion" target="_blank" class="icon-social" title="Síguenos en Facebook"><img alt="Empresa en Cancún con Persianas y Cortinas decorativas" src="<?php echo $path.'sources/facebook.png'; ?>"></a>
                    <a href="https://twitter.com/Sundec_cancun" class="icon-social" target="_blank" title="Síguenos en Twitter"><img alt="Empresa que decora con muebles e ilumiacion en Cancún" src="<?php echo $path.'sources/twitter.png'; ?>"></a>
                    <a href="https://www.instagram.com/sundecdecoracion/" class="icon-social" target="_blank" title="Síguenos en Instagram"><img alt="Empresa de Soluciones Decorativas en Cancún" src="<?php echo $path.'sources/instagram.png'; ?>"></a>
                    <a href="http://www.pinterest.com/sundec/" class="icon-social" target="_blank" title="Síguenos en Pinterest"><img alt="Empresa en Cancún de decoración con catálogo en línea" src="<?php echo $path.'sources/pinterest.png'; ?>"></a>
                </div>
                <div class="clr"></div>
            </div>
            <div class="clr"></div>
    
        </section>


        <div class="rowMenu">
        <div class="container">

            <div class="columns sixteen">
                <div class="boxNavMobile">
                        <select id="navmobile">
                            <option data-navmobile="/" <?php if($page == 'index.php'){?> selected <?php } ?>>INICIO</option>
                            <option data-navmobile="/quienes-somos" <?php if($page == 'quienes-somoss.php'){?> selected <?php } ?>>QUIENES SOMOS</option>
                            <option data-navmobile="/catalogo" <?php if($page == 'catalogo.php'){?> selected <?php } ?>>CATALOGO</option>
                            <option data-navmobile="/proyectos" <?php if($page == 'proyectos.php'){?> selected <?php } ?>>PROYECTOS</option>
                            <option data-navmobile="/nuestras-marcas" <?php if($page == 'nuestras-marcass.php'){?> selected <?php } ?>>NUESTRAS MARCAS</option>
                            <option data-navmobile="/promociones" <?php if($page == 'promociones.php'){?> selected <?php } ?>>PROMOCIONES</option>
                            <option data-navmobile="/contacto" <?php if($page == 'contacto.php'){?> selected <?php } ?>>CONTACTO</option>
                        </select>
                    </div>
                <nav>
                    <ul class="menu">
                        <li><a title="Sundec Decoración Cancún" href="<?php echo $path; ?>" <?php if($page == 'index.php'){?> class="current" <?php } ?>>INICIO</a></li>
                        <li><a title="Soluciones Decorativas Sundec Cancún" href="<?php echo $path.'quienes-somos'; ?>" <?php if($page == 'quienes-somos.php'){?> class="current" <?php } ?>>QUIENES SOMOS</a></li>
                        <li><a title="Catálogo Sundec Decoración" href="<?php echo $path.'catalogo'; ?>" <?php if($page == 'catalogo.php'){?> class="current" <?php } ?>>CATÁLOGO</a></li>                        
                        <li><a title="Proyectos Decorativos por Sundec Decoración" href="<?php echo $path.'proyectos'; ?>" <?php if($page == 'proyectos.php'){?> class="current" <?php } ?>>PROYECTOS</a></li>
                        <li><a title="Marcas Decorativas por Sundec Decoración" href="<?php echo $path.'nuestras-marcas'; ?>" <?php if($page == 'nuestras-marcas.php'){?> class="current" <?php } ?>>NUESTRAS MARCAS</a></li>                        
                        <!--<li><a href="<?php echo $path.'nuestros-trabajos'; ?>" <?php if($page == 'nuestros-trabajos.php'){?> class="current" <?php } ?>>NUESTROS TRABAJOS</a></li>-->                     
                        <li><a title="Cotizar Decoración de Interiores Cancún" href="<?php echo $path.'contacto'; ?>" <?php if($page == 'contacto.php'){?> class="current" <?php } ?>>CONTACTO</a></li>
                        <li><a title="Promociones en Decoración en Cancún" href="<?php echo $path.'promociones'; ?>" <?php if($page == 'promociones.php'){?> class="current" <?php } ?>><i class="fa fa-shopping-cart"></i> PROMOCIONES</a></li>
                    </ul>
                </nav>

            </div>
            <div class="clr"></div>

        </div>
        </div>

    </header>