<?php
//session_start();

//$code = substr(md5(uniqid(rand())),0,6);

$code = date("jmy");
// Define el ancho del texto usando la funcion creada anteriormente.
//$_SESSION['captcha'] = $code;

// Crea una imagen gif en memoria.
$captcha = imagecreatefromgif("../sources/bgcaptcha.gif");

// Asigna un color para una imagen
$letras = imagecolorallocate($captcha,21,82,103);

// Unir el texto en la imagen gif creada.
imagestring($captcha,14,12,8,$code,$letras);

// Pone la imagen en cabezera.
header("Content-type: image/gif");

// Muestra la imagen.
imagegif($captcha);
imagedestroy($captcha);

return $code;
?>