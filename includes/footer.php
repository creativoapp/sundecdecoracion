	<footer>
		<section class="container foo">
			<ul class="menu-foo">
				<li><a title="Decoración en Cancún" href="<?php echo $path; ?>" <?php if($page == 'index.php'){?> class="current" <?php } ?>>INICIO</a></li>
	            <li class="diagseparator">/</li>
	            <li><a title="Soluciones Decorativas Sundec Cancún" href="<?php echo $path.'quienes-somos'; ?>" <?php if($page == 'quienes-somos.php'){?> class="current" <?php } ?>>QUIENES SOMOS</a></li>
	            <li class="diagseparator">/</li>
	            <li><a title="Catálogo Sundec Decoración" href="catalogo" <?php if($page == 'fishing-charters.php'){?> class="current" <?php } ?>>CATALOGO</a></li>
	            <li class="diagseparator">/</li>
	            <li><a title="Marcas decorativas por Sundec Decoración" href="nuestras-marcas" <?php if($page == 'fishing-charters.php'){?> class="current" <?php } ?>>NUESTRAS MARCAS</a></li>
	            <li class="diagseparator">/</li>
	            <!--<li><a href="<?php echo $path.'nuestros-trabajos'; ?>" <?php if($page == 'nuestros-trabajos.php'){?> class="current" <?php } ?>>NUESTROS TRABAJOS</a></li>
	            <li class="diagseparator">/</li>-->
	            <li><a title="Contacta y Cotiza para Decorar tu Hogar" href="<?php echo $path.'contacto'; ?>" <?php if($page == 'contacto.php'){?> class="current" <?php } ?>>CONTACTO</a></li>
                <li class="diagseparator">/</li>
                <li><a title="Sundec Decoración Cancún Blog" href="/blog">BLOG</a></li>
                <li class="diagseparator">/</li>
                <li><a title="Promociones en Decoración en Cancún" href="promociones" <?php if($page == 'group-yachts.php'){?> class="current" <?php } ?>><i class="fa fa-shopping-cart"></i> PROMOCIONES</a></li>
            
			</ul>
			<span>© <?=date('Y');?>. Sundec Decoración. Todos los derechos Reservados. <small class="by"><a href="https://www.webcancun.com.mx/paginas-web" target="_blank">Diseño Web</a> y <a href="https://www.webcancun.com.mx/posicionamiento-web" target="_blank">Posicionamiento Web</a>: <a href="https://www.webcancun.com.mx" target="_blank">Web Cancún</a></small></span>
		</section>
	</footer>
</body>
</html>
<?php ob_end_flush(); ?>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f060e1567771f3813c0ab96/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57365544-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>