<?php include('includes/header.php'); ?>


<section class="container pagePromos">
	
	<section class="columns sixteen"><br>
		<h1>PROMOCIONES SUNDEC</h1>
		<h2>Promociones del Mes</h2>
		<br>

	</section>
	<div class="clr"></div>

	<div class="row">	
		<?php $promo =  new Promociones(); ?>
		
		<div class="listPromos">
			<?php echo $promo->showPromos(); ?>
		</div>
		
		
	</div>

</section>
	
<?php include('includes/footer.php'); ?>