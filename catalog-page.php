<?php include('includes/header.php'); ?>


<section class="container">
	<br><br>
	<?php include('includes/aside.php'); ?>

	<?php
		$catalogo = new Catalog();

    	$viewpage = json_decode($catalogo->showCategory($route));
    	//echo $catalogo->showCategory($route);
    ?>
	
	<section class="columns eleven catalogContentPage">
		<div class="breadcrum"><span>Estas ubicado en:</span> <a href="<?php echo $path; ?>">Inicio</a> > <a href="<?php echo $path.'catalogo'; ?>">Catalogo</a><small> > <?php echo $viewpage->{'page'}; ?></small></div>
		<a href="<?php echo $path.'catalogos-pdf'; ?>" class="buttonPdfCatalogo">CATALOGO PDF</a>
		<h1><?php echo $viewpage->{'page'}; ?></h1>
		<?php echo $viewpage->{'content'}; ?>
		<br>
		
		<?php 
			$viewpage = json_decode($catalogo->getGallery($route)); 
			echo $viewpage->{'object'}; 
		?>
		
		
	</section>
	<div class="clr"></div><br><br><br>

</section>
	
<?php include('includes/footer.php'); ?>