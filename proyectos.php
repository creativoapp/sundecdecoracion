<?php include('includes/header.php'); ?>


<section class="container">
	<br><br>

	<section class="columns eleven"><br>
		<h1>NUESTROS PROYECTOS</h1>
		<p>Conoce algunos de nuestros proyectos concluidos y clientes satisfechos.</p>
		<br><br>

		<div class="row listProjects">

			<?php 
			$project = new Projects();
			$view = $project->viewProjects();

			foreach ($view as $html) { ?>
				
				<article class="itemBoxProject">
					<img src="../timthumb.php?src=http://www.sundecdecoracion.com/sources/projects/<?php echo $html['miniatura']; ?>&amp;w=240&amp;h=160&amp;ac=1&amp;q=90" alt="<?php echo $html['altimg']; ?>">
					<div class="information">
						<h2><?php echo $html['proyecto']; ?></h2>
						<p><?php echo $html['contenido']; ?></p>
						<!--<a href="#">Ver Galería <i class="fa fa-angle-right"></i></a>-->
					</div>
				</article>

			<?php
			}
			?>
			

		</div>
		
		

	</section>

	<?php include('includes/aside.php'); ?>
	<div class="clr"></div>
	<br><br><br><br>

</section>
	
<?php include('includes/footer.php'); ?>