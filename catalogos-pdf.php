<?php include('includes/header.php'); ?>


<section class="container">
	<br><br>

	<section class="columns eleven"><br>
		<h1>CATALOGOS EN VERSION PDF</h1>
		<p>Sundec Cancún pone a tu disposición también la versión en PDF de nuestros catalogos de productos y servicios.</p>
		<br><br>

		<?php
			$view = new Catalog();
			//print_r(json_decode($view->viewAllCatalogs()));

			$listview = json_decode($view->viewAllCatalogs());

			foreach ($listview as $item) { ?>
				
			<div class="boxAnclaPdf">
				<h3><?php echo $item->{'categoria'}; ?></h3>

				<ul class="listPdfCatalogs">

					<?php 
					foreach ($item->{'files'} as $pdf) { ?>
					<li>
						<a href="/sources/catalogo/pdf/<?php echo $pdf->{'file'}; ?>" target="_blank"><i class="fa fa-file-pdf-o"></i></a>
						<a href="/sources/catalogo/pdf/<?php echo $pdf->{'file'}; ?>" target="_blank"><span><?php echo $pdf->{'catalogo'}; ?></span></a>
					</li>						
					<?php
					}
					?>
					
					<div class="clr"></div>
				</ul>
			</div>

		<?php	
			}
		?>

		<!--<div class="boxAnclaPdf">
			<h3>Nombre Categoria</h3>

			<ul class="listPdfCatalogs">
				<li>
					<a href="#"><i class="fa fa-file-pdf-o"></i></a>
					<a href="#"><span>TITULO DE CATALOGO</span></a>
				</li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<div class="clr"></div>
			</ul>
		</div>
		
		<div class="boxAnclaPdf">
			<h3>Nombre Categoria</h3>

			<ul class="listPdfCatalogs">
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<div class="clr"></div>
			</ul>
		</div>

		<div class="boxAnclaPdf">
			<h3>Nombre Categoria</h3>

			<ul class="listPdfCatalogs">
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<li><a href="#">
					<i class="fa fa-file-pdf-o"></i>
					<span>TITULO DE CATALOGO</span>
				</a></li>
				<div class="clr"></div>
			</ul>
		</div>-->

	</section>

	<?php include('includes/aside.php'); ?>
	<div class="clr"></div>
	<br><br><br><br>

</section>
	
<?php include('includes/footer.php'); ?>