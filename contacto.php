<?php include('includes/header.php'); ?>

<section class="container">

	
	<div class="row boxContacto">
		<!--BOX DATA OFFICCE-->
		<?php include('includes/aside.php'); ?>
		<!--END DATA OFFICCE-->
		
		<!--BOX FORM CONTACT-->
		<article class="columns eleven">
			<h1>CONTACTO</h1>
			<p>Si necesitas cotizar para algún proyecto decorativo en Cancún, cortinas o persianas, muebles sobre diseño o tienes alguna duda sobre nuestro catálogo de productos decorativos, ponte en contacto con nosotros, en Sundec Decoración estamos seguros de poder ayudarte.</p>
			<form name="contacto" id="contacto" action="" method="post">
				<fieldset class="tquadform">
					<label>Nombre completo *</label>
					<input type="text" name="ipt-name" id="ipt-name" />
				</fieldset>
				<fieldset class="quadform">
					<label>Teléfono</label>
					<input type="text" name="ipt-phone" id="ipt-phone" />
				</fieldset>
				<div class="clr"></div>
				<fieldset>
					<label>E-mail *</label>
					<input type="text" name="ipt-mail" id="ipt-mail" />
				</fieldset>
				<fieldset>
					<label>Mensaje *</label>
					<textarea name="ipt-message" id="ipt-message" rows="5"></textarea>
				</fieldset>
				<fieldset class="formcaptcha">
					<label>Código de Seguridad *</label>
					<input type="text" name="ipt-captcha" id="ipt-captcha" maxlength="6" />
					<input type="text" name="codesec" id="codesec" value="<?php echo date("jmy"); ?>" hidden="hidden" readonly="readonly"/>
					<img id="captcha" src="includes/captcha.php" height="27"  />
					<input type="submit" name="sendForm" id="sendForm" value="ENVIAR" />
				</fieldset>
			</form>
		</article>
		<!--END FORM CONTACT-->
		<div class="clr"></div>
	</div>

	
</section>

<section class="containermap">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3721.6110200157154!2d-86.83870328523494!3d21.12806858594475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4c2bc423aa9cc1%3A0x2ff422a1b20149ac!2sSundec%20Decoracion!5e0!3m2!1ses!2smx!4v1594304905307!5m2!1ses!2smx" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</section>
	
<?php include('includes/footer.php'); ?>