<?php include('includes/header.php'); ?>


<section class="container">
	<br><br>
	<?php include('includes/aside.php'); ?>

	<section class="columns eleven" style="position:relative;"><br>
		
		<h1>¿QUIENES SOMOS?</h1>
		<h2>Sundec Decoración</h2>
		<a href="<?php echo $path.'presentacion-sundec-decoracion.pdf'; ?>" target="_blank" class="buttonPresentation">
			<i class="fa fa-laptop"></i> VER PRESENTACIÓN
		</a>
		<p>Sundec Decoración en Cancún, es una empresa mexicana establecida en la ciudad de Cancún, Q. Roo, dedicada a la 
		venta de productos de alta calidad que cubran las necesidades más exigentes para el mercado de la decoración 
		residencial, comercial y hotelera, buscando siempre la satisfacción plena del usuario final, a través de dicha 
		calidad y un excelente servicio.</p>
		<p>Sundec Decoración cuenta con varios Productos y Servicios para la Decoración de su casa u Hotel en Cancún, 
		entre los cuales destacan sus Cortinas y Persianas que pueden ser colocadas en cualquier establecimiento de Cancún.</p>
        
		<p>Contamos con diferentes artículos de decoración para complementar tu diseño, al igual que persianas y cortinas que harán lucir tus espacios mejor. También tenemos alfombras, toldos, papel tapiz, pisos de madera, pisos vinílicos, decks y muchos productos más. ¡Ven a visitarnos!</p>
		<br>
		<img title="Sundec Cancún Decoración para el Hogar" src="<?php echo $path.'sources/sundec-decoracion-cancun.jpg'; ?>" alt="Tienda de Decoración en Cancún Muebles Papel Tapiz y Persianas" >
		
		<!--VIDEO-->
		<div class="boxvideoqs">
			<video controls >
			  	<source src="sources/sundec.mp4" type="video/mp4">
				Your browser does not support the video tag.
			</video>
		</div>
		<!--END VIDEO-->
	</section>
	<div class="clr"></div>
	<br><br><br><br>

</section>
	
<?php include('includes/footer.php'); ?>