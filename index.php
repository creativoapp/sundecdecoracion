<?php include('includes/header.php'); ?>


<!--BOXSLIDER-->
<div class="boxSlider">
	<?php 
		$slider = new Slider();
		echo $slider->showSlider();
	?>
	<nav class="navcats">
	   	<ul>
	   		<?php 
	   		$catalog = new Catalog();
	   		echo $catalog->listCatalogSlider();
	   		?>
	   	</ul>
   	</nav>
</div>
<!--END BOXSLIDER-->


<section class="container">
	

	<!--CATEGORIAS-->
	<div class="row threeBox-home">
		<article class="column one-third">
			<div class="item-cats">
				<div class="maskBack"></div>
				<div class="mask"><a href="catalogo/persianas-cortinas-cancun">VER MAS</a></div>
				<img title="Persianas en Cancun" src="<?php echo $path.'sources/persianas-en-cancun.jpg'; ?>" alt="Cortinas y Persianas en Cancún Decorativas" />
			</div>
			<h2><a title="Persianas y Cortinas en Cancún" href="catalogo/persianas-cortinas-cancun">PERSIANAS Y CORTINAS</a></h2>
		</article>
		<article class="column one-third">
			<div class="item-cats">
				<div class="maskBack"></div>
				<div class="mask"><a href="catalogo/toldos-cancun">VER MAS</a></div>
				<img title="Toldos en Cancun" src="<?php echo $path.'sources/toldos-en-cancun.jpg'; ?>" alt="Toldos en Cancún para Jardín o alberca de hotel" />
			</div>
			<h2><a title="Celosías en Cancún" href="catalogo/toldos-cancun">TOLDOS</a></h2>
		</article>
		<article class="column one-third">
			<div class="item-cats">
				<div class="maskBack"></div>
				<div class="mask"><a href="catalogo/pisos-de-madera-cancun">VER MAS</a></div>
				<img title="Pisos de Madera en Cancún" src="<?php echo $path.'sources/catalogo/Pisos Creativos 8.jpg'; ?>" alt="Pisos de Madera en Cancún" />
			</div>
			<h2><a title="Pisos de Madera en Cancún" href="catalogo/pisos-de-madera-cancun">PISOS</a></h2>
		</article>
		<div class="clr"></div>
	</div>
	<!--END CATEGORIAS-->

	<section class="columns sixteen sundecHome">
		<h1>DECORACIÓN EN CANCÚN</h1>
		<h2>Sundec Decoración</h2>
		<p>Sundec Decoración en Cancún, es una empresa mexicana establecida en la ciudad de Cancún, Q. Roo, dedicada a la 
		venta de productos de alta calidad que cubran las necesidades más exigentes para el mercado de la decoración 
		residencial, comercial y hotelera, buscando siempre la satisfacción plena del usuario final, a través de dicha 
		calidad y un excelente servicio.</p>
		<p>Sundec Decoración cuenta con varios Productos y Servicios para la Decoración de su casa u Hotel en Cancún, 
		entre los cuales destacan sus Cortinas y Persianas que pueden ser colocadas en cualquier establecimiento de Cancún. Además de contar con una variedad de diseños únicos que hará resaltar cualquier decoración de interiores en Cancún.</p>
		<p>Entre las especialidades de decoración, tenemos artículos decorativos con diseños vanguardistas y únicos, así como alfombras y decks para contrastar cualquier ambiente ya sea en una sala, oficina u hotel.</p>
		<p>Sundec se encarga de proveerte los mejores pisos, cortinas, persianas, toldos, decks, alfombras y artículos decorativos para tu proyecto de diseño de interiores. Tenemos años de experiencia en el mercado, cuenta con que encontrarás lo que necesitas para tener la decoración que deseas.</p>
	</section>
	<div class="clr"></div><br><br>

	<!--VIDEO-->
	<div class="columns sixteen boxvideo">
    
    <iframe width="100%" height="600px" src="//www.youtube.com/embed/WytzmMooQ9U" frameborder="0" allowfullscreen></iframe>
		<!--<video controls >
        	
		  	<source src="sources/sundec.mp4" type="video/mp4">
			Your browser does not support the video tag.
		</video>-->
	</div><div class="clr"></div>
	<br><br><br><br>
	<!--END VIDEO-->

</section>
	
<?php include('includes/footer.php'); ?>