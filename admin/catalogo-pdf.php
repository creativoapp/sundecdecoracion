<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        <section class="topControlls">
            <div class="icon-add action-addMarca"><img src="<?php echo $path.'admin/sources/add.png'; ?>" /></div>
            <a href="/admin/nuevo-catalogo-pdf">AGREGAR CATALOGO</a>
            <a href="#" class="big-link option-modules" data-reveal-id="categories">ADMINISTRAR CATEGORIAS</a>
            <div class="clr"></div>
        </section>
        <section class="viewDash">
            
            <h1>Gestion de Catalogos PDF's</h1>

            <?php
            if(isset($_GET['update']) && $_GET['update'] == 'ok') {
                echo '<div class="msg-success">Catalogo actualizado exitosamente</div>';
                header("Refresh: 1; URL=catalogo-pdf");
            }
            
            
                $file = new Catalog(); 
                $list = $file->listFiles();
                
                
                if($list != null) { ?>

                    <table class="tResults tPages searchResults">
                        <tr>
                            <td>Catalogo</td><td>PDF</td><td>Categoria</td><td colspan="2">&nbsp;</td>
                        </tr>        

                        <?php foreach ($list as $row) {
                    
                            $hide = $_SESSION['rol'] != 3 ? '' : 'style="display:none;"';
                        
                        echo '<tr>
                                    <td>'.$row['catalogo'].'</td>
                                    <td>'.$row['archivo'].'</td>
                                    <td>'.$file->getNameModule($row['fkmodulo']).'</td>
                                    <td><a href="#" '.$hide.' data-reveal-id="editFile" class="big-link editFile" data-idfile="'.MD5($row['pkcatalogo']).'" title="Editar Catalogo"><img src="sources/edit-action.png" width="20"></a></td>
                                    <td><a href="catalogo-pdf?idcatalogo='.MD5($row['pkcatalogo']).'&file='.$row['archivo'].'" '.$hide.' title="Eliminar Catalogo"><img src="sources/delete-action.png" width="20"></a></td>
                               </tr>';
                        } ?>

                    </table>
            <?php           
                }
            ?>
            

            <div id="editFile" class="reveal-modal">
                <h1>Editar información del Catalogo</h1>
                <form name="frmEditCatalogo" id="frmEditCatalogo" action="" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <label>Titulo</label>
                        <input type="text" name="inpCatalogo" id="inpCatalogo" placeholder="Titulo del Catalogo" /><br>
                        <label>Catalogo</label>
                        <input type="file" name="inpFile" id="inpFile" />
                        <input type="hidden" name="inpCurrentFile" id="inpCurrentFile" >
                        <label>Categoria</label>
                        <select name="inpModule" id="inpModule">
                            <option value="0">Seleccione una Categoria</option>
                            <?php
                                $list = $file->listModules();
                                foreach ($list as $option) { ?>
                                    <option value="<?php echo $option['pkmodulo'] ?>"><?php echo $option['modulo']; ?></option>
                            <?php        
                                }
                            ?>
                        </select>
                    </fieldset>
                    <fieldset>
                        <input type="hidden" name="inpUpdate" id="inpUpdate">
                        <input type="submit" name="btnUpdateCatalog" id="btnUpdateCatalog" value="ACTUALIZAR">
                    </fieldset>
                </form>
                <a class="close-reveal-modal">&#215;</a>
            </div>

            <?php
                
                //ELIMINAR PROYECTO
                if(isset($_GET['idcatalogo']) && $_GET['idcatalogo'] != NULL) {

                    $action = json_decode($file->deletePdf($_GET['idcatalogo']));
                    if($action->{'state'} == 'succes') {
                        unlink($_SERVER['DOCUMENT_ROOT'].'/sources/catalogo/pdf/'.$_GET['file']);
                        header('Location:catalogo-pdf');
                    }
                }



                //ACTUALIZAR PROYECTO
                if(isset($_POST['inpUpdate']) && $_POST['inpUpdate'] != NULL) {

                    $folder = $_SERVER['DOCUMENT_ROOT'].'/sources/catalogo/pdf/';

                    $title = filter_input(INPUT_POST, 'inpCatalogo', FILTER_SANITIZE_STRING);
                    $module = filter_input(INPUT_POST, 'inpModule', FILTER_SANITIZE_NUMBER_INT);

                    $thumb = $_FILES['inpFile']['name'] != '' ? $_FILES['inpFile']['name'] : $_POST['inpCurrentFile'];
                    $args = array($title, $thumb, $module, $_POST['inpUpdate']);
                    $action = json_decode($file->updatePdf($args));

                    if($_FILES['inpFile']['name'] != '') {
                        move_uploaded_file($_FILES['inpFile']['tmp_name'], $folder.$thumb);   
                    }

                    if($action->{'state'} == 'succes') { header('Location:catalogo-pdf?update=ok'); }

                }

                
            ?>


            <!--MODULOS-->
            <div id="categories" class="reveal-modal">
                <h1>Administrar Categorias</h1>
                
                <ul class="lista-modulos"> 
                <?php
                    $list = $file->listModules();
                    foreach ($list as $option) { ?>
                        <li id="li-<?php echo MD5($option['pkmodulo']); ?>">
                            <input type="text" id="inp-<?php echo MD5($option['pkmodulo']); ?>" class="inpInlineModulo" value="<?php echo $option['modulo']; ?>">
                            <a href="#" class="editModule" data-editmod="<?php echo MD5($option['pkmodulo']); ?>">Editar</a>
                            <a href="#" class="deleteModule" data-delmod="<?php echo MD5($option['pkmodulo']); ?>">Eliminar</a>
                        </li>
                            
                    <?php } ?>
                    <li>
                        <input type="text" id="inpCreateModule" >
                        <a href="#" id="btnCreateModule">GUARDAR</a>
                    </li>
                </ul>

                <a class="close-reveal-modal">&#215;</a>
            </div>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>