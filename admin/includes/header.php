<?php
    ob_start();
    session_start(); 

    if(!isset($_SESSION['sesion'])){ header('Location:login'); }
    include('../includes/path.php'); 
    include('../models/mSlider.php');
    include('../models/mSettings.php');
    include('../models/mSeo.php');
    include('../models/mYates.php');
    include('../models/mCatalogo.php');
    include('../models/mGalerias.php');
    include('../models/mBrands.php');
    include('../models/mPromos.php');
    include('../models/mProjects.php');
    include('../models/mCities.php');

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <title>Dashboard - Sundec Decoración</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">

    <!--<link rel="shortcut icon" type="image/x-icon" href="<?php echo $path.'srcimages/favicon.ico'; ?>" />-->
  
    <!--CSS
    ========================== -->
    <link href="<?php echo $path.'css/pure.css';?>" rel="stylesheet" />
    <link href="<?php echo $path.'admin/css/dashboard.css'; ?>" rel="stylesheet" />
    <link href="<?php echo $path.'admin/css/reveal.css'; ?>" rel="stylesheet" />
    <link href="<?php echo $path.'admin/css/tinyeditor.css'; ?>" rel="stylesheet" />
    <link href="<?php echo $path.'admin/css/jquery-datepicker.css'; ?>" rel="stylesheet" />

    <!--JS
    ========================== -->
    <script type="text/javascript" src="<?php echo $path.'js/jquery-1.8.3.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'admin/js/jquery.reveal.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'admin/js/tiny.editor.packed.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'admin/js/jquery-datepicker.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'admin/js/models.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'admin/js/controllers.js'; ?>"></script>


    <script type="text/javascript">
        $(document).ready(function(){
            $( "#from, #updfrom" ).datepicker({
                    dateFormat: "yy-mm-dd",
                    defaultDate: "+1w",
                    changeMonth: true,
                    changeYear: true,
                    onClose: function( selectedDate ) {
                        $( "#to" ).datepicker( "option", "minDate", selectedDate );
                    }
            });
            $( "#to, #updto" ).datepicker({
                    defaultDate: "+1w",
                    dateFormat: "yy-mm-dd",
                    changeMonth: true,
                    changeYear: true,
                    onClose: function( selectedDate ) {
                        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
                    }
            });
        });
    </script>

    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>
    <?php include('navigation.php'); ?>