<section class="navigation">
	<div class="boxNavigation">
		<div class="profileNav">
			<img src="<?php echo $path.'admin/sources/profile.png'; ?>" />
			<span><?php echo $_SESSION['sesion']; ?></span>
			<div class="clr"></div>
		</div>
		<ul class="navdashboard">
			<li>
				<img src="<?php echo $path.'admin/sources/seo.png'; ?>" />
				<a href="seo" <?php if($page == 'seo.php'){?> class="current" <?php } ?>>SEO</a>
			</li>
			<li>
				<img src="<?php echo $path.'admin/sources/slider.png'; ?>" />
				<a href="slider" <?php if($page == 'slider.php'){?> class="current" <?php } ?>>SLIDER</a>
			</li>
			<li>
				<img src="<?php echo $path.'admin/sources/catalog.png'; ?>" />
				<a href="catalogo" <?php if($page == 'catalogo.php'){?> class="current" <?php } ?>>CATALOGOS</a>
			</li>
			<li>
				<img src="<?php echo $path.'admin/sources/catalog.png'; ?>" />
				<a href="catalogo-pdf" <?php if($page == 'catalogo-pdf.php'){?> class="current" <?php } ?>>CATALOGOS PDF</a>
			</li>
			<li>
				<img src="<?php echo $path.'admin/sources/camera.png'; ?>" />
				<a href="gallery" <?php if($page == 'gallery.php'){?> class="current" <?php } ?>>GALERIAS</a>
			</li>
			<li>
				<img src="<?php echo $path.'admin/sources/brands.png'; ?>" />
				<a href="brands" <?php if($page == 'brands.php'){?> class="current" <?php } ?>>MARCAS</a>
			</li>
			<li>
				<img src="<?php echo $path.'admin/sources/slider.png'; ?>" />
				<a href="proyectos" <?php if($page == 'proyectos.php'){?> class="current" <?php } ?>>PROYECTOS</a>
			</li>
			<li>
				<img src="<?php echo $path.'admin/sources/deals.png'; ?>" />
				<a href="deals" <?php if($page == 'deals.php'){?> class="current" <?php } ?>>PROMOCIONES</a>
			</li>
			<li class="subnav">
				<img src="<?php echo $path.'admin/sources/setting.png'; ?>" />
				<a href="#" <?php if($page == 'setting.php' || $page == 'contacto.php'){?> class="current" <?php } ?>>CONFIGURACION</a>	
				<ul class="contentsbnav">
					<li><a href="setting">- HEADER</a></li>
					<li><a href="contacto">- CONTACTO</a></li>
				</ul>
			</li>
			<li>
				<img src="<?php echo $path.'admin/sources/close.png'; ?>" />
				<a href="<?php echo $path.'helpers/hLogin.php?logout=ok'; ?>">SALIR</a>
			</li>
		</ul>
	</div>
</section>