//@Model::LOGIN
//@Autor::Alex Jimenez
var hpATH = "../../";
//var hpATH = "http://www.webcancun.com.mx/sundec/";
function loginAdmin(user, pass)
{
	var Jlogin = {"user" : user, "pass" : pass };

	$.ajax({
		data:  Jlogin,
		url:   hpATH + 'helpers/hLogin.php',
		type:  'post',
												
		beforeSend: function(){ 
			$('.loading').fadeIn('last'); 
			$('#btn-login').fadeOut('last');
		},

		success:  function (data) {
			console.log(data);
			var response = $.parseJSON(data);
			$('#username, #password').val('');
			if(response.login == 'True')
			{
				window.location = "dashboard";
			}
			else
			{
				$('#btn-login').fadeIn('last', function(){
					$('.loading').text('Revise sus datos de acceso.')
				});
			}

		}

			
	});
}


//@Model::SLIDER
//@Autor::Alex Jimenez
//@Establece el orden de impresion de los banners
function setOrderSlider(almacen, slider)
{
	var JOrder = {"orden" : almacen, "slider" : slider };

	$.ajax({
		data:  JOrder,
		url:   hpATH + 'helpers/hSlider.php',
		type:  'post',
								
		success:  function (data) {
			var response = $.parseJSON(data);
			alert(response.message);
			
		}

			
	});	

}


//@Model::SLIDERGETEDIT
//@Autor::Alex Jimenez
//@Carga informacion del slider elegido
function getInfoSlider(idslider)
{
	var jSlider = { "idslider" : idslider };

	$.ajax({
			data:  jSlider,
			url:   hpATH + 'helpers/hSlider.php',
			type:  'post',
							
			beforeSend: function(){},

			success:  function (data) {
				var response = $.parseJSON(data);				
				$('#slr-updlink').val(response.linkk);
				$('#slr-updalt').val(response.altt);
				$('#slr-updtitle').val(response.titlee);
				
			}

		});
}


//@Model::GETSEO
//@Autor::Alex Jimenez
//@Recupera la informacion seo de una pagina
function getSeo(idseo)
{
	var jSeo = {"idseo" : idseo }; 
								
		$.ajax({
			data:  jSeo,
			url:   hpATH + 'helpers/hSeo.php',
			type:  'post',
							
			beforeSend: function(){},

			success:  function (data) {
				
				$('#formAjax').html(data);
				
				
			}
		});
}


//@Model::SETSEO
//@Autor::Alex Jimenez
//@Modifica el seo de una pagina
function setSeo(title, keyword, description, id)
{
	var jsSeo = {"title" : title, "keywords" : keyword, "description" : description, "id" : id }; 
								
		$.ajax({
			data:  jsSeo,
			url:   hpATH + 'helpers/hSeo.php',
			type:  'post',
							
			beforeSend: function(){},

			success:  function (data) {
				var response = $.parseJSON(data);				
				if(response.state == 'Edited')
				{
					alert('Modificación exitosa');
					//$('.close-reveal-modal').trigger("click");
					location.href = document.URL;
				}
			}

		});
}


//@Model::LOADPAGES
//@Autor::Alex Jimenez
//@Carga lista de paginas segun categoria
function loadPages(categoria)
{
	var jPages = {"categoria" : categoria};

	$.ajax({
			data:  jPages,
			url:   hpATH + 'helpers/hPages.php',
			type:  'post',
							
			beforeSend: function(){},

			success:  function (data) {
				
				var response = $.parseJSON(data);				
				var size = response.length;
				
				$('#gal-ancla').find('option').remove();

				for (var i = 0; i < size; i++) 
				{
					var destroy = response[i].split('*');
					$('#gal-ancla').append('<option value="'+destroy[1]+'" >'+destroy[0]+'</option>');
				}
			}

		});

}


//@Model::IMAGEEDIT
//@Autor::Alex Jimenez
//@Carga informacion de la imagen elegida segun una galeria
function imageEdit(page)
{
	var jImg = { "idPage" : page };

	$.ajax({
			data:  jImg,
			url:   hpATH + 'helpers/hGallery.php',
			type:  'post',
							
			beforeSend: function(){},

			success:  function (data) {
				var response = $.parseJSON(data);				
				$('#gal-editAlt').val(response.attrAlt);
				$('#gal-editName').val(response.attrName);
			}

		});

}


//@Model::MARCASGETEDIT
//@Autor::Alex Jimenez
//@Carga informacion de la marca elegida
function getInfoMarcas(idMarca)
{
	var jBrand = { "idMarca" : idMarca };

	$.ajax({
			data:  jBrand,
			url:   hpATH + 'helpers/hBrands.php',
			type:  'post',
							
			beforeSend: function(){},

			success:  function (data) {
				var response = $.parseJSON(data);				
				$('#mar-updname').val(response.marca);
				$('#mar-updalt').val(response.attrAlt);
				$('#mar-updurl').val(response.linkto);
				$('#mar-updtitle').val(response.attrTitle);
				$('#mar-upddescr').val(response.descr);
				//alert(response.marca);
			}

		});
}


//PROYECTOS
function getProject(project) {

	$.ajax({
		data:  { 'project' : project },
		url:   hpATH + 'helpers/hBrands.php',
		type:  'post',
							
		beforeSend: function(){},

		success:  function (data) {
			
			var response = $.parseJSON(data);				
			$('#inpProject').val(response.project);
			$('#inpCurrenThumb').val(response.thumb);
			$('#inpAlt').val(response.txalt);
			$('#inpDescription').val(response.description);
			//alert(response.marca);
		}

	});

}

//CATALOGOS PDF
function getCatalogo(catalogo) {

	$.ajax({
		data:  { 'catalogo' : catalogo },
		url:   hpATH + 'helpers/hBrands.php',
		type:  'post',
							
		beforeSend: function(){},

		success:  function (data) {
			
			var response = $.parseJSON(data);				
			$('#inpCatalogo').val(response.catalogo);
			$('#inpCurrentFile').val(response.archivo);
			$('#inpModule option[value="'+response.modulo+'"]').attr('selected','selected');
			
		}

	});

}

//MODULOS PDF
function createModule(modulo) {

	$.ajax({
		data:  { 'modulo' : modulo },
		url:   hpATH + 'helpers/hBrands.php',
		type:  'post',
							
		beforeSend: function(){},

		success:  function (data) {
			
			var response = $.parseJSON(data);				
			
			if(response.state == 'succes') {
				$('.lista-modulos').append('<li>'+modulo+'</li>');
				alert('La categoria '+modulo+' se ha guardado correctamente');
				$('#inpCreateModule').val('');
			}
			
		}

	});

}

function deleteModule(pk) {

	$.ajax({
		data:  { 'deletemod' : pk },
		url:   hpATH + 'helpers/hBrands.php',
		type:  'post',
							
		beforeSend: function(){},

		success:  function (data) {
			
			var response = $.parseJSON(data);				
			if(response.state == 'succes') {
				$('#li-'+pk+'').remove();
				alert('Categoria eliminada correctamente');
			}
			
		}

	});

}

function editModule(module, pk) {

	$.ajax({
		data:  { 'editmod' : pk, 'namemod' : module },
		url:   hpATH + 'helpers/hBrands.php',
		type:  'post',
							
		beforeSend: function(){},

		success:  function (data) {
			
			var response = $.parseJSON(data);				
			if(response.state == 'succes') {
				alert('Categoria actualizada con exito');
			}
			
		}

	});

}


//@Model::PROMOSGETEDIT
//@Autor::Alex Jimenez
//@Carga informacion de la marca elegida
function getInfoPromo(idpromo)
{
	var jPromo = { "idpromo" : idpromo };

	$.ajax({
			data:  jPromo,
			url:   hpATH + 'helpers/hPromos.php',
			type:  'post',
							
			beforeSend: function(){},

			success:  function (data) {
				var response = $.parseJSON(data);				
				$('#pro-updname').val(response.promo);
				$('#updfrom').val(response.inicio);
				$('#updto').val(response.fin);
				$('#pro-updalt').val(response.altt);
				$('#pro-updtitle').val(response.titlee);
				//alert(response.marca);
			}

		});
}