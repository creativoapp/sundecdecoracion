<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        <section class="topControlls">
            <div class="icon-info"><img src="<?php echo $path.'admin/sources/info.png'; ?>" /></div>
            <a href="#">GESTIONE SU SEO</a>
            <form name="seo-search" id="seo-search" method="post" action="seo">
                <input type="text" name="seo-searcht" id="seo-searcht" />
                <input type="submit" name="seo-btnSearch" id="seo-btnSearch" value="BUSCAR" />
            </form>
            <div class="clr"></div>
        </section>
        <section class="viewDash">
            <h1>Gestión de meta etiquetas</h1>

             <?php
                $seo = new Seo();
                $type = '';
                if(!empty($_POST['seo-searcht'])) { $type = $_POST['seo-searcht']; } 
                echo $seo->showList($type);
                
                
             ?>

            <div id="myModal" class="reveal-modal">
                <h1>Editar Información SEO</h1>
                <div id="formAjax"></div>
                <fieldset>
                    <input type="text" name="seo-id" id="seo-id" hidden="hidden" />
                    <?php if($_SESSION['rol'] != 3) {  ?>
                    <input type="submit" name="seo-btnUpd" id="seo-btnUpd" value="EDITAR" />
                    <?php } ?>
                </fieldset>
                <a class="close-reveal-modal">&#215;</a>
            </div>       


            <?php
                //@Controller::ACTUALIZAR CONTACTO
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario y procesa la actualizacion de los datos de la pagina de contacto
                if(isset($_POST['cont-btnUpd']))
                {
                    $arguments = array(
                                    $_POST['cont-addres1'],
                                    $_POST['cont-addres2'],
                                    $_POST['cont-addres3'],
                                    $_POST['cont-mail'],
                                    $_POST['cont-phone'],
                                    $_POST['cont-mailform'],
                                    $_POST['cont-latCont'],
                                    $_POST['cont-longCont'],
                                    $_POST['cont-id']);
                    $update = json_decode($setting->setContacto($arguments));
                    if($update->{'state'} == 'succes')
                    {
                        header('Location:contacto?updok=true');
                    }
                }

                if(isset($_GET['updok']))
                {
                    echo '<div class="msg-success">Se actulizo la información exitosamente</div>';
                    header("Refresh: 3; URL=contacto");
                }

                
            ?>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>