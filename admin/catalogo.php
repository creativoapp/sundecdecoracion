<?php include('includes/header.php'); ?>
<section class="rightPanel">
    <section class="topControlls">
        <div class="icon-add action-addpage"><img src="<?php echo $path . 'admin/sources/add.png'; ?>" /></div>
        <a href="#" class="action-addpage">AGREGAR NUEVA CATEGORIA</a>
        <form name="seo-search" id="seo-search" method="post" action="yates">
            <!--<input type="text" name="yat-searcht" id="yat-searcht" disabled="disabled" />
                <input type="submit" name="yat-btnSearch" id="yat-btnSearch" disabled="disabled" value="BUSCAR" />-->
        </form>
        <div class="clr"></div>
    </section>
    <section class="viewDash">
        <h1>Categorias del Catalogo</h1>

        <?php
        $catalog = new Catalog();

        if (!isset($_GET['edit'])) {
            echo $catalog->listCats();
        }

        $mCity = new Cities();

        $list_cities = $mCity->getCities();

        ?>


        <?php
        if (isset($_GET['edit'])) {
            $update = json_decode($catalog->getCatalog($_GET['edit']));
            //print_r($update);
        ?>
            <form name="frm-updCate" id="frm-updCate" method="post" enctype="multipart/form-data">
                <fieldset>
                    <label>Categoria</label>
                    <input type="text" name="cat-updname" id="cat-updname" value="<?php echo $update->{'name'}; ?>" required />
                    <label>Imagen Perfil <small>420 - 280</small></label>
                    <input type="file" name="cat-updprofile" id="cat-updprofile" />
                    <label>Ciudad</label>
                    <select name="cat-upcity" id="cat-upcity">
                        <?php foreach ($list_cities as $ct) { ?>
                            <option <?= (($ct['id'] == $update->{'city'}) ? 'selected' : '') ?> value="<?= $ct['id'] ?>" data-url="<?= $ct['url'] ?>"><?= $ct['name'] ?></option>
                        <?php } ?>
                    </select>
                    <label>Url</label>
                    <input type="text" name="cat-updurl" id="cat-updurl" value="<?php echo $update->{'url'}; ?>" required />
                    <label>alt Imagen Perfil</label>
                    <input type="text" name="cat-updalt" id="cat-updalt" value="<?php echo $update->{'alt'}; ?>" />
                    <label>title Link</label>
                    <input type="text" name="cat-updtitleLink" id="cat-updtitleLink" value="<?php echo $update->{'title'}; ?>" />
                    <input type="hidden" name="cat-idedit" id="cat-idedit" value="<?php echo $_GET['edit']; ?>" />
                </fieldset>
                <fieldset>
                    <label>Contenido</label>
                    <textarea name="cat-updContent" id="cat-updContent"><?php echo $update->{'content'}; ?></textarea><br>
                    <input type="submit" name="cat-btnUpd" id="cat-btnUpd" value="ACTUALIZAR">
                </fieldset>
            </form>
        <?php } ?>

        <form name="frm-addCate" id="frm-addCate" method="post" enctype="multipart/form-data">
            <fieldset>
                <label>Categoria</label>
                <input type="text" name="cat-name" id="cat-name" placeholder="Categoria" /><br>
                <label>Imagen perfil <small>420 - 280</small></label>
                <input type="file" name="cat-profile" id="cat-profile" />
                <label>Ciudad</label>
                <select name="cat-city" id="cat-city">
                    <?php foreach ($list_cities as $ct) { ?>
                        <option value="<?= $ct['id'] ?>" data-url="<?= $ct['url'] ?>"><?= $ct['name'] ?></option>
                    <?php } ?>
                </select>
                <small id="sw-link-ct">http://www.sundecdecoracion.com/catalogo/</small><span id="url"></span><a href="#" id="replace-url">Definir otra URL</a><br><br>
                <input type="text" name="cat-url" id="cat-url" placeholder="url-de-la-categoria" />
                <label>alt Imagen Perfil</label>
                <input type="text" name="cat-alt" id="cat-alt" placeholder="Alt de la imagen" />
                <label>title Link</label>
                <input type="text" name="cat-titleLink" id="cat-titleLink" placeholder="Title Link" />
            </fieldset>
            <fieldset>
                <label>Descripcion</label>
                <textarea name="cat-content" id="cat-content"></textarea><br>
                <?php if ($_SESSION['rol'] != 3) { ?>
                    <input type="submit" name="cat-btnAdd" id="cat-btnAdd" value="PUBLICAR">
                <?php } ?>
            </fieldset>
        </form>

        <?php
        if (isset($_GET['edit'])) { ?>
            <script>
                var editor = new TINY.editor.edit('editor', {
                    id: 'cat-updContent',
                    width: 650,
                    height: 250,
                    cssclass: 'tinyeditor',
                    controlclass: 'tinyeditor-control',
                    rowclass: 'tinyeditor-header',
                    dividerclass: 'tinyeditor-divider',
                    controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
                        'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
                        'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
                        'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'
                    ],
                    footer: true,
                    fonts: ['Arial', 'Verdana', 'Helvetica', 'Georgia', 'Trebuchet MS'],
                    xhtml: true,
                    cssfile: '',
                    bodyid: 'editor',
                    footerclass: 'tinyeditor-footer',
                    toggle: {
                        text: 'source',
                        activetext: 'wysiwyg',
                        cssclass: 'toggle'
                    },
                    resize: {
                        cssclass: 'resize'
                    }
                });
            </script>
        <?php } else {

        ?>
            <script>
                var editor = new TINY.editor.edit('editor', {
                    id: 'cat-content',
                    width: 650,
                    height: 250,
                    cssclass: 'tinyeditor',
                    controlclass: 'tinyeditor-control',
                    rowclass: 'tinyeditor-header',
                    dividerclass: 'tinyeditor-divider',
                    controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
                        'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
                        'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
                        'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'
                    ],
                    footer: true,
                    fonts: ['Arial', 'Verdana', 'Helvetica', 'Georgia', 'Trebuchet MS'],
                    xhtml: true,
                    cssfile: '',
                    bodyid: 'editor',
                    footerclass: 'tinyeditor-footer',
                    toggle: {
                        text: 'source',
                        activetext: 'wysiwyg',
                        cssclass: 'toggle'
                    },
                    resize: {
                        cssclass: 'resize'
                    }
                });
            </script>
        <?php } ?>

        <?php

        //@Controller::CREAR CATEGORIA
        //@Autor::Alex Jimenez
        //@Recibe datos de formulario y genera la creacion de la categoria

        if (isset($_POST['cat-name']) && isset($_POST['cat-url'])) {
            define('_PATHSources', $_SERVER['DOCUMENT_ROOT'] . '/sources/catalogo/');
            $folio = date('mdY');

            if (isset($_FILES['cat-profile'])) {
                $thumb = $_FILES['cat-profile']['name'];
                if (empty($thumb)) {
                    $thumb = 'default.jpg';
                } else {
                    $thumb = $_FILES['cat-profile']['name'];
                    /*$_thumb = explode('.', $thumb);
                            $thumb = $_thumb[0].'-'.$folio.'.'.$_thumb[1];*/
                    move_uploaded_file($_FILES['cat-profile']['tmp_name'], _PATHSources . $thumb);
                }
            }

            $mCities = new Cities();

            $url_city = '';
            $ciudad = $mCities->getCities($_POST['cat-city']);
            if ($ciudad != [] && $_POST['cat-city'] != 1) {
                $url_city = $ciudad[0]['url'];
            }

            $args = array(
                $_POST['cat-name'],
                $thumb,
                $_POST['cat-content'],
                $_POST['cat-alt'],
                $_POST['cat-titleLink'],
                $url_city.'/catalogo/' . $_POST['cat-url'],
                $_POST['cat-city']
            );

            //print_r($args);

            $insert = json_decode($catalog->insertCatalog($args));
            if ($insert->{'state'} == 'succes') {
                header('Location:catalogo?createok=true');
            }
        }

        if (isset($_GET['createok'])) {
            echo '<div class="msg-success">Se creo la página correctamente</div>';
            header("Refresh: 3; URL=catalogo");
        }


        //@Controller::ELIMINAR CATEGORIA
        //@Autor::Alex Jimenez
        //@Elimina la pagina solicitada via GET
        if (isset($_GET['delete'])) {
            $delete = json_decode($catalog->disabled($_GET['delete']));
            echo $delete->{'state'};
            if ($delete->{'state'} == 'succes') {
                header('Location:catalogo?deleteok=true');
            }
        }

        if (isset($_GET['deleteok'])) {
            echo '<div class="msg-success">Se elimino la página correctamente</div>';
            header("Refresh: 3; URL=catalogo");
        }


        //@Controller::ACTUALIZAR CATEGORIA
        //@Autor::Alex Jimenez
        //@Recibe datos de formulario y actualiza los datos de la categoria

        if (isset($_POST['cat-updname']) && isset($_POST['cat-updurl'])) {
            define('_PATHSources', $_SERVER['DOCUMENT_ROOT'] . '/sources/catalogo/');
            $folio = date('mdY');


            $thumb = '';
            if ($_FILES['cat-updprofile']['tmp_name'] != "") {
                $thumb = $_FILES['cat-updprofile']['name'];
                move_uploaded_file($_FILES['cat-updprofile']['tmp_name'], _PATHSources . $thumb);
            } else {
                $thumb = null;
            }

            $arguments = array(
                $_POST['cat-updname'],
                $thumb,
                $_POST['cat-updContent'],
                $_POST['cat-updalt'],
                $_POST['cat-updtitleLink'],
                $_POST['cat-updurl'],
                $_POST['cat-idedit'],
                $_POST['cat-upcity']
            );

            $updexe = json_decode($catalog->updateCatalog($arguments));
            if ($updexe->{'state'} == 'succes') {
                header('Location:catalogo?updateok=true');
            }
        }

        if (isset($_GET['updateok'])) {
            echo '<div class="msg-success">Se modificaron los datos de la página correctamente</div>';
            header("Refresh: 3; URL=catalogo");
        }

        ?>

    </section>

</section>
<div class="clr"></div>

</body>

</html>
<?php ob_end_flush(); ?>