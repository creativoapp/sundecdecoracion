<?php include('../includes/path.php'); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <title>Login admin - Sundec Decoración</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">

    <!--<link rel="shortcut icon" type="image/x-icon" href="<?php echo $path.'srcimages/favicon.ico'; ?>" />-->
  
    <!--CSS
    ========================== -->
    <link href="<?php echo $path.'css/pure.css';?>" rel="stylesheet" />
    <link href="<?php echo $path.'admin/css/dashboard.css'; ?>" rel="stylesheet" />

    <!--JS
    ========================== -->
    <script type="text/javascript" src="<?php echo $path.'js/jquery-1.8.3.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'admin/js/models.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'admin/js/controllers.js'; ?>"></script>
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body class="login">
    <form name="login" id="login" action="" method="post">
        <h1>Adminitración</h1>
        <fieldset>
            <input type="text" name="username" id="username" placeholder="Usuario" />
            <input type="password" name="password" id="password" placeholder="Password" />
            <input type="submit" name="btn-login" id="btn-login" value="INGRESAR" />
            <span class="loading">Ingresando...</span>
        </fieldset>
    </form>
</body>
</html>