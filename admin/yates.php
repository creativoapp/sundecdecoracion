<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        <section class="topControlls">
            <div class="icon-add action-addpage"><img src="<?php echo $path.'admin/sources/add.png'; ?>" /></div>
            <a href="#" class="action-addpage">AGREGAR NUEVA PAGINA</a>
            <form name="seo-search" id="seo-search" method="post" action="yates">
                <input type="text" name="yat-searcht" id="yat-searcht" />
                <input type="submit" name="yat-btnSearch" id="yat-btnSearch" value="BUSCAR" />
            </form>
            <div class="clr"></div>
        </section>
        <section class="viewDash">
            <h1>Yates producto</h1>
             
            <?php 
                $yates = new Yates();
                if(!isset($_GET['edit'])) { ?>
            <form name="yat-slcategory" id="yat-slcategory" class="searchResults" action="" method="post">
                <select name="yat-slcat" id="yat-slcat">
                        <option value="0">Todas</option>
                        <option value="1">Luxury Yachts</option>
                        <option value="2">Fishing Charters</option>
                        <option value="3">Groups Yachts</option>
                        <option value="4">Catamarans</option>
                        <option value="5">Wakeboard, Ski & More</option>
                    </select>
                <input type="submit" name="yat-searchcat" id="yat-searchcat" value="LISTAR" />
            </form>
            <?php
                !empty($_POST['yat-searcht']) ? $type = $_POST['yat-searcht'] : $type = '';
                if(!empty($_POST['yat-slcat'])){ $type = $_POST['yat-slcat']; }
                echo $yates->listPages($type);
            }
            ?>


            <?php 
                if(isset($_GET['edit'])) { 
                $update = json_decode($yates->getYates($_GET['edit']));
            ?>
            <form name="frm-updYates" id="frm-updYates" method="post" enctype="multipart/form-data">
                <fieldset>
                    <label>Titulo</label>
                    <input type="text" name="yat-updtitulo" id="yat-updtitulo" value="<?php echo $update->{'title'}; ?>" />
                    <label>Url</label>
                    <input type="text" name="yat-updurl" id="yat-updurl" value="<?php echo $update->{'url'}; ?>" />
                    <label>Precio lista</label>
                    <input type="text" name="yat-updprice" id="yat-updprice" value="<?php echo $update->{'precio'}; ?>" />
                    <label>Categoria</label>
                    <select name="yat-updcategory" id="yat-updcategory">
                        <option value="0">Seleccione una categoria</option>
                        <option <?php if($update->{'categoria'} == '1'){ ?> selected <?php } ?> value="1">Luxury Yachts</option>
                        <option <?php if($update->{'categoria'} == '2'){ ?> selected <?php } ?> value="2">Fishing Charters</option>
                        <option <?php if($update->{'categoria'} == '3'){ ?> selected <?php } ?> value="3">Groups Yachts</option>
                        <option <?php if($update->{'categoria'} == '4'){ ?> selected <?php } ?> value="4">Catamarans</option>
                        <option <?php if($update->{'categoria'} == '5'){ ?> selected <?php } ?> value="5">Wakeboard, Ski & More</option>
                    </select>
                    <label>Thumbnail <small>340 - 220</small></label>
                    <input type="file" name="yat-updthumb" id="yat-updthumb" />
                    <label>alt Imagen</label>
                    <input type="text" name="yat-updalthumb" id="yat-updalthumb" value="<?php echo $update->{'alt'}; ?>" />
                    <input type="text" name="yat-idedit" id="yat-idedit" value="<?php echo $_GET['edit']; ?>" />
                </fieldset>
                <fieldset>
                    <label>Contenido</label>
                    <textarea name="yat-updContent" id="yat-updContent"><?php echo $update->{'content'}; ?></textarea><br>
                    <input type="submit" name="yat-btnUpd" id="yat-btnUpd" value="ACTUALIZAR">
                </fieldset>
            </form>
            <?php } ?>

            <form name="frm-addYates" id="frm-addYates" method="post" enctype="multipart/form-data">
                <fieldset>
                    <label>Titulo</label>
                    <input type="text" name="yat-titulo" id="yat-titulo" /><br>
                    <small>http://www.yachtrentalsincancun.com/</small><span id="url"></span><a href="#" id="replace-url">Definir otra URL</a><br><br>
                    <input type="text" name="yat-url" id="yat-url"/>
                    <label>Precio lista</label>
                    <input type="text" name="yat-price" id="yat-price" />
                    <label>Categoria</label>
                    <select name="yat-category" id="yat-category">
                        <option value="0">Seleccione una categoria</option>
                        <option value="1">Luxury Yachts</option>
                        <option value="2">Fishing Charters</option>
                        <option value="3">Groups Yachts</option>
                        <option value="4">Catamarans</option>
                        <option value="5">Wakeboard, Ski & More</option>
                    </select>
                    <label>Thumbnail <small>340 - 220</small></label>
                    <input type="file" name="yat-thumb" id="yat-thumb" />
                    <label>alt Imagen</label>
                    <input type="text" name="yat-althumb" id="yat-althumb" />
                </fieldset>
                <fieldset>
                    <label>Contenido</label>
                    <textarea name="yat-content" id="yat-content"></textarea><br>
                    <input type="submit" name="yat-btnAdd" id="yat-btnAdd" value="PUBLICAR">
                </fieldset>
            </form>
            
            <?php
                if(isset($_GET['edit']))
                { ?>
                <script>
                var editor = new TINY.editor.edit('editor', {
                    id: 'yat-updContent',
                    width: 650,
                    height: 250,
                    cssclass: 'tinyeditor',
                    controlclass: 'tinyeditor-control',
                    rowclass: 'tinyeditor-header',
                    dividerclass: 'tinyeditor-divider',
                    controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
                        'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
                        'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
                        'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'],
                    footer: true,
                    fonts: ['Arial','Verdana','Helvetica','Georgia','Trebuchet MS'],
                    xhtml: true,
                    cssfile: 'custom.css',
                    bodyid: 'editor',
                    footerclass: 'tinyeditor-footer',
                    toggle: {text: 'source', activetext: 'wysiwyg', cssclass: 'toggle'},
                    resize: {cssclass: 'resize'}
                });
            </script>
                <?php }
                else
                {

            ?>
            <script>
                var editor = new TINY.editor.edit('editor', {
                    id: 'yat-content',
                    width: 650,
                    height: 250,
                    cssclass: 'tinyeditor',
                    controlclass: 'tinyeditor-control',
                    rowclass: 'tinyeditor-header',
                    dividerclass: 'tinyeditor-divider',
                    controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
                        'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
                        'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
                        'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'],
                    footer: true,
                    fonts: ['Arial','Verdana','Helvetica','Georgia','Trebuchet MS'],
                    xhtml: true,
                    cssfile: 'custom.css',
                    bodyid: 'editor',
                    footerclass: 'tinyeditor-footer',
                    toggle: {text: 'source', activetext: 'wysiwyg', cssclass: 'toggle'},
                    resize: {cssclass: 'resize'}
                });
            </script>
            <?php } ?>

            <?php
                //@Controller::CREAR PARGINA
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario y genera la creacion de la pagina
                
                if(isset($_POST['yat-titulo']) && isset($_POST['yat-url']))
                {
                    define('_PATHSources', $_SERVER['DOCUMENT_ROOT'].'/sources/yates/');
                    $folio = date('mdY');
                    
                    if(isset($_FILES['yat-thumb']))
                    {
                        $thumb = $_FILES['yat-thumb']['name'];
                        if(empty($thumb))
                        {
                            $thumb = 'default.jpg';
                        }
                        else
                        {
                            $thumb = $_FILES['yat-thumb']['name'];
                            /*$_thumb = explode('.', $thumb);
                            $thumb = $_thumb[0].'-'.$folio.'.'.$_thumb[1];*/
                            move_uploaded_file($_FILES['yat-thumb']['tmp_name'], _PATHSources.$thumb);  
                        }                      

                    }

                    $arguments = array(
                                    $_POST['yat-titulo'],
                                    $thumb,
                                    $_POST['yat-althumb'],
                                    $_POST['yat-price'],
                                    $_POST['yat-content'],
                                    $_POST['yat-category'],
                                    '/'.$_POST['yat-url'],                                    
                                    'Yachts Rentals in Cancun');

                    $insert = json_decode($yates->insertPage($arguments));
                    if($insert->{'state'} == 'succes')
                    {
                        header('Location:yates?createok=true');
                    }
                }

                if(isset($_GET['createok']))
                {
                    echo '<div class="msg-success">Se creo la página correctamente</div>';
                    header("Refresh: 3; URL=yates");
                }


                //@Controller::ELIMINAR PARGINA
                //@Autor::Alex Jimenez
                //@Elimina la pagina solicitada via GET
                if(isset($_GET['delete']))
                {
                    $delete = json_decode($yates->deletePage($_GET['delete']));
                    echo $delete->{'state'};
                    if($delete->{'state'} == 'succes')
                    {
                        header('Location:yates?deleteok=true');
                    }
                }

                if(isset($_GET['deleteok']))
                {
                    echo '<div class="msg-success">Se elimino la página correctamente</div>';
                    header("Refresh: 3; URL=yates");
                }


                //@Controller::ACTUALIZAR PAGINA
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario y actualiza los datos de la pagina
                
                if(isset($_POST['yat-updtitulo']) && isset($_POST['yat-updurl']))
                {
                    define('_PATHSources', $_SERVER['DOCUMENT_ROOT'].'/sources/yates/');
                    $folio = date('mdY');
                    
                    if(isset($_FILES['yat-updthumb']))
                    {
                        $thumb = $_FILES['yat-updthumb']['name'];
                        if(empty($thumb))
                        {
                            $thumb = null;
                        }
                        else
                        {
                            $thumb = $_FILES['yat-updthumb']['name'];
                            /*$_thumb = explode('.', $thumb);
                            $thumb = $_thumb[0].'-'.$folio.'.'.$_thumb[1];*/
                            move_uploaded_file($_FILES['yat-updthumb']['tmp_name'], _PATHSources.$thumb);  
                        }                      

                    }

                    $arguments = array(
                                    $_POST['yat-updtitulo'],
                                    $thumb,
                                    $_POST['yat-updalthumb'],
                                    $_POST['yat-updprice'],
                                    $_POST['yat-updContent'],
                                    $_POST['yat-updcategory'],
                                    $_POST['yat-updurl'],                                    
                                    $_POST['yat-idedit']);

                    $updexe = json_decode($yates->modificPage($arguments));
                    if($updexe->{'state'} == 'succes')
                    {
                        header('Location:yates?updateok=true');
                    }
                }

                if(isset($_GET['updateok']))
                {
                    echo '<div class="msg-success">Se modificaron los datos de la página correctamente</div>';
                    header("Refresh: 3; URL=yates");
                }
                
            ?>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>