<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        <section class="topControlls">
            <div class="icon-add action-addPromo"><img src="<?php echo $path.'admin/sources/add.png'; ?>" /></div>
            <a href="#" class="action-addPromo">AGREGAR PROMOCION</a>
            <div class="clr"></div>
        </section>
        <section class="viewDash">
            
            <h1>Gestion de Promociones</h1>
            
            <?php
                $promo = new Promociones(); 
                echo $promo->listPromos();
                
            ?>
            

            <div id="myModal" class="reveal-modal">
                <h1>Editar Promoción</h1>
                <form name="loadGallery" id="loadGallery" action="" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <label>Promoción</label>
                        <input type="text" name="pro-updname" id="pro-updname" /><br>
                        <label>Imagen lista <small>640 - 200</small></label>
                        <input type="file" name="pro-updimglist" id="pro-updimglist" />
                        <label>Imagen overlay</label>
                        <input type="file" name="pro-updimgoverlay" id="pro-updimgoverlay" />
                        <div class="divider-2">
                            <label>Fecha incio</label>
                            <input type="text" name="pro-updinicio" id="updfrom" />
                        </div>
                        <div class="divider-2">
                            <label>Fecha fin</label>
                            <input type="text" name="pro-updfin" id="updto" />
                        </div>
                        <div class="clr"></div>
                        <label>alt Banner</label>
                        <input type="text" name="pro-updalt" id="pro-updalt" />
                        <label>title Link</label>
                        <input type="text" name="pro-updtitle" id="pro-updtitle" /> 
                    </fieldset>
                    <fieldset>
                        <input type="hidden" name="pro-updid" id="pro-updid" />
                        <input type="submit" name="pro-btnUpd" id="pro-btnUpd" value="PUBLICAR">
                    </fieldset>
                </form>
                <a class="close-reveal-modal">&#215;</a>
            </div>


            <form name="frm-addPromo" id="frm-addPromo" method="post" enctype="multipart/form-data">
                <fieldset>
                    <label>Promoción</label>
                    <input type="text" name="pro-name" id="pro-name" /><br>
                    <label>Imagen lista <small>640 - 200</small></label>
                    <input type="file" name="pro-imglist" id="pro-imglist" />
                    <label>Imagen overlay</label>
                    <input type="file" name="pro-imgoverlay" id="pro-imgoverlay" />
                    <label>Fecha incio</label>
                    <input type="text" name="pro-inicio" id="from" />
                    <label>Fecha fin</label>
                    <input type="text" name="pro-fin" id="to" /> 
                </fieldset>
                <fieldset>
                    <?php if($_SESSION['rol'] != 3) { ?>
                    <input type="submit" name="pro-btnAdd" id="pro-btnAdd" value="PUBLICAR">
                    <?php } ?>
                </fieldset>
            </form>


            <?php
                define('_BRANDSpath', $_SERVER['DOCUMENT_ROOT'].'/sources/promos/');
                //@Controller::CREAR PROMOCION
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario y genera la creacion de promociones
                if(isset($_POST['pro-btnAdd']) && $_FILES['pro-imglist']['tmp_name'] != '' && $_FILES['pro-imgoverlay']['tmp_name'] != '')
                {   
                   
                    //define('_BRANDSpath', $_SERVER['DOCUMENT_ROOT'].'/sources/promos/');

                    $now = empty($_POST['pro-inicio']) ? date('Y-m-d') : $_POST['pro-inicio'];
                    $end = empty($_POST['pro-fin']) ? '2020-01-01' : $_POST['pro-fin'];
                    
                    $imglist = $_FILES['pro-imglist']['name'];
                    $imgoverlay = $_FILES['pro-imgoverlay']['name'];
                    $args = array($_POST['pro-name'], $imglist, $imgoverlay, $now, $end);

                    if(move_uploaded_file($_FILES['pro-imglist']['tmp_name'], _BRANDSpath.$imglist) && move_uploaded_file($_FILES['pro-imgoverlay']['tmp_name'], _BRANDSpath.$imgoverlay))
                    {   
                        $insert = json_decode($promo->insertPromo($args));
                        if($insert->{'state'} == 'succes')
                        {
                            header('Location:deals?insert=ok');
                        }
                    } 

                        
                }


                if(isset($_GET['insert']) && $_GET['insert'] == 'ok')
                {
                    echo '<div class="msg-success">Se creo la promoción correctamente</div>';
                    header("Refresh: 3; URL=deals");
                }


                
                //@Controller::BORRA PROMOCION
                //@Autor::Alex Jimenez
                //@Recibe dato id marca por get para eliminar la marca
                if(isset($_GET['delpromo']) && !empty($_GET['delpromo']))
                {
                    $delPromo = json_decode($promo->deletePromo($_GET['delpromo']));
                    if($delPromo->{'state'} == 'succes')
                    {
                        header('Location:deals');
                    }
                }



                //@Controller::EDITAR PROMOCION
                //@Autor::Alex Jimenez
                //@Recibe dato idpromo por post para editar la promocion
                if(isset($_POST['pro-btnUpd']))
                {
                    
                    $imglist = $_FILES['pro-updimglist']['name'] != '' ? $_FILES['pro-updimglist']['name'] : NULL;
                    $imgoverlay = $_FILES['pro-updimgoverlay']['name'] != '' ? $_FILES['pro-updimgoverlay']['name'] : NULL;                    
                    $fin = empty($_POST['pro-updfin']) ? NULL : $_POST['pro-updfin'];

                    $args = array($_POST['pro-updname'],
                                  $imglist, $imgoverlay,
                                  $_POST['pro-updinicio'],
                                  $_POST['pro-updfin'],
                                  $_POST['pro-updalt'],
                                  $_POST['pro-updtitle'],
                                  $_POST['pro-updid']);

                    $updPromo = json_decode($promo->updatePromo($args));
                    if($updPromo->{'state'} == 'succes')
                    {
                        if($imglist != NULL){
                           move_uploaded_file($_FILES['pro-updimglist']['tmp_name'], _BRANDSpath.$imglist); 
                        }

                        if($imgoverlay != NULL)
                        {
                            move_uploaded_file($_FILES['pro-updimgoverlay']['tmp_name'], _BRANDSpath.$imgoverlay);
                        }

                        header('Location:deals?editpromo=ok');
                    }
                    //echo $updMarca->{'id'};*/
                }


                if(isset($_GET['editpromo']) && $_GET['editpromo'] == 'ok')
                {
                    echo '<div class="msg-success">Se modifico la promoción correctamente</div>';
                    header("Refresh: 3; URL=deals");
                }
                
            ?>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>