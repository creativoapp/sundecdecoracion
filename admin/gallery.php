<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        <section class="topControlls">
            <div class="icon-add action-addGallery big-link" data-reveal-id="loadGallery"><img src="<?php echo $path.'admin/sources/add.png'; ?>" /></div>
            <a href="#" class="action-addGallery" data-reveal-id="loadGallery">AGREGAR FOTOGRAFIAS</a>
            
            <div class="icon-info action-addGallery" style="margin-left:15px;"><img src="<?php echo $path.'admin/sources/INFO.png'; ?>" /></div>
            <a href="anclas" class="action-addGallery">ADMINISTRAR ANCLAS</a>
            <div class="clr"></div>
        </section>
        <section class="viewDash">
            
            <?php    
                $gallery = new Gallerys();
                $catalog = new Catalog();
            ?>

            <h1>Gestion de galerias</h1>
            <form name="frm-searchType" id="frm-searchType" action="gallery" method="post" class="searchResults">
                <label>Categoria</label>
               <?php echo $catalog->frmSlctCats(null); ?>
                <input type="submit" name="gal-btnSearchct" id="gal-btnSearchct" value="BUSCAR" />
            </form>
            <?php 
                if(isset($_GET['setting']))
                {
                    echo $gallery->viewImages($_GET['setting']);
                }
                else
                {
                    $type = isset($_POST['listCtalog']) ? $_POST['listCtalog'] : '';
                    echo $gallery->listGallery($type);
                }
            ?>
               
            
            <div id="loadGallery" class="reveal-modal">
                <h1>Cargar imagenes a galerias</h1>
                <form name="loadGallery" id="loadGallery" action="" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <label>Categoria</label>
                        <?php echo $catalog->frmSlctCats('modal'); ?>
                        <label>Ancla</label>
                        <select name="gal-ancla" id="gal-ancla">
                            
                        </select>
                        <label>Imagen/Imagenes <small>550 - 250 Recomendado</small></label>
                        <input type="file" name="gal-storePhotos[]" id="gal-storePhotos" multiple />
                        <?php if($_SESSION['rol'] != 3) { ?>
                        <input type="submit" name="gal-btnLoad" id="gal-btnLoad" value="CARGAR" />
                        <?php } ?>
                    </fieldset>
                </form>
                <a class="close-reveal-modal">&#215;</a>
            </div>

            <div id="editImage" class="reveal-modal">
                <h1>Editar información de la imagen</h1>
                <form name="loadGallery" id="loadGallery" action="" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <label>Nombre</label>
                        <input type="text" name="gal-editName" id="gal-editName" />
                        <label>alt Imagen</label>
                        <input type="text" name="gal-editAlt" id="gal-editAlt" />
                        <input type="hidden" name="gal-editIdImage" id="gal-editIdImage"  />
                        <input type="submit" name="gal-btnEdit" id="gal-btnEdit" value="CARGAR" />
                    </fieldset>
                </form>
                <a class="close-reveal-modal">&#215;</a>
            </div>


            <?php
                
                //@Controller::CREAR ALBUM
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario y genera la creacion de albums
                if(isset($_POST['gal-btnLoad']) && !empty($_FILES['gal-storePhotos']))
                {   
                    if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/sources/galerias/'.$_POST['listCtalog']))
                    {
                        mkdir($_SERVER['DOCUMENT_ROOT'].'/sources/galerias/'.$_POST['listCtalog'], 0777);
                    }
                    

                    $fileCount = count($_FILES["gal-storePhotos"]["name"]);
                    for($i=0; $i < $fileCount; $i++)
                    {
                        $fileName = $_FILES["gal-storePhotos"]["name"][$i];
                        move_uploaded_file($_FILES["gal-storePhotos"]["tmp_name"][$i], $_SERVER['DOCUMENT_ROOT'].'/sources/galerias/'.$_POST['listCtalog'].'/'.$fileName);
                        $gallery->insertImages($_POST['listCtalog'], $_POST['gal-ancla'], $_POST['listCtalog'].'/'.$fileName);
                        //echo $fileName;
                    }

                        
                }



                //@Controller::MODIFICAR IMAGEN
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario (ajax-modal) y actualiza informacion de la imagen
                if(isset($_POST['gal-editIdImage']))
                {
                    $updImage = json_decode($gallery->setInfoPage($_POST['gal-editName'], $_POST['gal-editAlt'], $_POST['gal-editIdImage']));
                    if($updImage->{'state'} == 'succes')
                    {
                        header('Location:gallery?'.$_SERVER['QUERY_STRING'].'&editok=true');
                    }
                    //header('Location:gallery?'.$_SERVER['QUERY_STRING'].'&editok=true');
                }

                if(isset($_GET['editok']))
                {
                    $destroy = explode('&', $_SERVER['QUERY_STRING']);
                    echo '<div class="msg-success">Se modifico la información de la imagen correctamente</div>';
                    header("Refresh: 3; URL=gallery?".$destroy[0]);
                }


                //@Controller::BORRA IMAGEN
                //@Autor::Alex Jimenez
                //@Recibe dato id foto por get para eliminar imagen
                if(isset($_GET['idImage']))
                {
                    $delImage = json_decode($gallery->deleteImage($_GET['idImage']));
                    if($delImage->{'state'} == 'succes')
                    {
                        $destroy = explode('&', $_SERVER['QUERY_STRING']);
                        header('Location:gallery?'.$destroy[0].'&delImage=ok');
                    }
                }
                
            ?>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>