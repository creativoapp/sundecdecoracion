<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        
        <!--<section class="topControlls">
            <div class="icon-add action-addMarca"><img src="<?php echo $path.'admin/sources/add.png'; ?>" /></div>
            <a href="#" class="action-addMarca">AGREGAR PROYECTO</a>
            <div class="clr"></div>
        </section>-->

        <section class="viewDash">
            
            <h1>Publicar Proyecto</h1>
                       
            <form name="frmNewProject" id="frmNewProject" method="post" action="" enctype="multipart/form-data">
                <fieldset>
                    <label>Proyecto</label>
                    <input type="text" name="inpProject" id="inpProject" placeholder="Nombre del Proyecto" /><br>
                    <label>Imagen destacada <small>Tamaño de imagen requerido(250px - 180px)</small></label>
                    <input type="file" name="inpThumbnail" id="inpThumbnail" />
                    <label>Texto alternativo (alt)</label>
                    <input type="text" name="inpAlt" id="inpAlt" placeholder="Texto alternativo de la imagen destacada" />
                    <label>Descripción del proyecto</label>
                    <textarea name="inpDescription" id="inpDescription" rows="7" style="resize:none;" placeholder="Descripción del Proyecto"></textarea>
                </fieldset>
                <fieldset>
                    <?php if($_SESSION['rol'] != 3) { ?>
                    <input type="submit" name="btnAddProject" id="btnAddProject" value="PUBLICAR">
                    <?php } ?>
                </fieldset>
            </form>


            <?php
                
                //@Controller::CREAR PROYECTO
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario y genera la creacion de proyectos
                $project = new Projects();
                if(isset($_POST['inpProject']) && $_FILES['inpThumbnail']['tmp_name'] != '') {   
                   
                    $folder = $_SERVER['DOCUMENT_ROOT'].'/sources/projects/';

                    $title = filter_input(INPUT_POST, 'inpProject', FILTER_SANITIZE_STRING);
                    $alt = filter_input(INPUT_POST, 'inpAlt', FILTER_SANITIZE_STRING);
                    $description = filter_input(INPUT_POST, 'inpDescription', FILTER_SANITIZE_STRING);

                    $thumb = $_FILES['inpThumbnail']['name'];
                    $args = array($title, $thumb, $alt, $description);

                    if(move_uploaded_file($_FILES['inpThumbnail']['tmp_name'], $folder.$thumb)) {   
                        $action = json_decode($project->insert($args));

                        if($action->{'state'} == 'succes') { header('Location:nuevo-proyecto?insert=ok'); }
                    } 

                        
                }


                if(isset($_GET['insert']) && $_GET['insert'] == 'ok') {
                    echo '<div class="msg-success">Proyecto publicado exitosamente</div>';
                    header("Refresh: 2; URL=proyectos");
                }

            ?>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>