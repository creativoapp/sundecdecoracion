<?php
    ob_start();
    session_start(); 

    if(!isset($_SESSION['sesion'])){ header('Location:login'); }
    include('../includes/path.php'); 

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <title>Dashboard - Yachtrenatlcancun</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">

    <!--<link rel="shortcut icon" type="image/x-icon" href="<?php echo $path.'srcimages/favicon.ico'; ?>" />-->
  
    <!--CSS
    ========================== -->
    <link href="<?php echo $path.'css/pure.css';?>" rel="stylesheet" />
    <link href="<?php echo $path.'admin/css/dashboard.css'; ?>" rel="stylesheet" />

    <!--JS
    ========================== -->
    <script type="text/javascript" src="<?php echo $path.'js/jquery-1.8.3.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'admin/js/models.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $path.'admin/js/controllers.js'; ?>"></script>
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>
    <?php include('includes/navigation.php'); ?>
    <section class="viewDash">
        <h1>DASHBOARD</h1>
    </section>
    <div class="clr"></div>

</body>
</html>