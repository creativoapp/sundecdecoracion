<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        <section class="topControlls">
            <div class="icon-add action-addMarca"><img src="<?php echo $path.'admin/sources/add.png'; ?>" /></div>
            <a href="/admin/nuevo-proyecto">AGREGAR PROYECTO</a>
            <div class="clr"></div>
        </section>
        <section class="viewDash">
            
            <h1>Gestion de Proyectos</h1>

            <?php
            if(isset($_GET['update']) && $_GET['update'] == 'ok') {
                echo '<div class="msg-success">Proyecto actualizado exitosamente</div>';
                header("Refresh: 1; URL=proyectos");
            }
            
            
                $project = new Projects(); 
                $list = $project->listProjects();
                
                
                if($list != null) { ?>

                    <table class="tResults tPages searchResults">
                        <tr>
                            <td>Proyecto</td><td>Imagen</td><td>alt</td><td>Descripción</td><td colspan="2">&nbsp;</td>
                        </tr>        

                        <?php foreach ($list as $row) {
                    
                            $hide = $_SESSION['rol'] != 3 ? '' : 'style="display:none;"';
                        
                        echo '<tr>
                                    <td>'.$row['proyecto'].'</td>
                                    <td>'.$row['miniatura'].'</td>
                                    <td>'.$row['altimg'].'</td>
                                    <td>'.$row['contenido'].'</td>
                                    <td><a href="#" '.$hide.' data-reveal-id="editProject" class="big-link editProject" data-idproject="'.MD5($row['pkproyecto']).'" title="Editar Proyecto"><img src="sources/edit-action.png" width="20"></a></td>
                                    <td><a href="proyectos?idproject='.MD5($row['pkproyecto']).'&file='.$row['miniatura'].'" '.$hide.' title="Eliminar Proyecto"><img src="sources/delete-action.png" width="20"></a></td>
                               </tr>';
                        } ?>

                    </table>
            <?php           
                }
            ?>
            

            <div id="editProject" class="reveal-modal">
                <h1>Editar información del Proyecto</h1>
                <form name="frmEditProject" id="frmEditProject" action="" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <label>Proyecto</label>
                        <input type="text" name="inpProject" id="inpProject" /><br>
                        <label>Imagen destacada <small>Tamaño de imagen requerido(250px - 180px)</small></label>
                        <input type="file" name="inpThumbnail" id="inpThumnail" />
                        <input type="hidden" name="inpCurrenThumb" id="inpCurrenThumb" >
                        <label>Texto alternativo (alt)</label>
                        <input type="text" name="inpAlt" id="inpAlt" />
                        <label>Descripción del proyecto</label>
                        <textarea name="inpDescription" id="inpDescription" rows="7" style="resize:none;"></textarea>
                        <input type="hidden" name="inpUpdate" id="inpUpdate" />
                </fieldset>
                <fieldset>
                    <input type="submit" name="btnUpdateProject" id="btnUpdateProject" value="ACTUALIZAR">
                </fieldset>
                </form>
                <a class="close-reveal-modal">&#215;</a>
            </div>


            <?php
                
                //ELIMINAR PROYECTO
                if(isset($_GET['idproject']) && $_GET['idproject'] != NULL) {

                    $action = json_decode($project->delete($_GET['idproject']));
                    if($action->{'state'} == 'succes') {
                        unlink($_SERVER['DOCUMENT_ROOT'].'/sources/projects/'.$_GET['file']);
                        header('Location:proyectos');
                    }
                }



                //ACTUALIZAR PROYECTO
                if(isset($_POST['inpUpdate']) && $_POST['inpUpdate'] != NULL) {

                    $folder = $_SERVER['DOCUMENT_ROOT'].'/sources/projects/';

                    $title = filter_input(INPUT_POST, 'inpProject', FILTER_SANITIZE_STRING);
                    $alt = filter_input(INPUT_POST, 'inpAlt', FILTER_SANITIZE_STRING);
                    $description = filter_input(INPUT_POST, 'inpDescription', FILTER_SANITIZE_STRING);

                    $thumb = $_FILES['inpThumbnail']['name'] != '' ? $_FILES['inpThumbnail']['name'] : $_POST['inpCurrenThumb'];
                    $args = array($title, $thumb, $alt, $description, $_POST['inpUpdate']);
                    $action = json_decode($project->update($args));

                    if($_FILES['inpThumbnail']['name'] != '') {
                        move_uploaded_file($_FILES['inpThumbnail']['tmp_name'], $folder.$thumb);   
                    }

                    if($action->{'state'} == 'succes') { header('Location:proyectos?update=ok'); }

                }

                
            ?>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>