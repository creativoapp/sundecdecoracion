-- phpMyAdmin SQL Dump
-- version 3.4.11.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 08-01-2015 a las 07:19:47
-- Versión del servidor: 5.5.40
-- Versión de PHP: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sundec_newsite`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `albums`
--

CREATE TABLE IF NOT EXISTS `albums` (
  `idPhoto` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `altPhoto` varchar(255) NOT NULL DEFAULT 'Image galler Sundec Decoracion',
  `fkCategory` int(11) NOT NULL,
  `fkAncla` int(11) NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idPhoto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=131 ;

--
-- Volcado de datos para la tabla `albums`
--

INSERT INTO `albums` (`idPhoto`, `photo`, `name`, `altPhoto`, `fkCategory`, `fkAncla`, `Status`) VALUES
(14, '5/persiana-classic-aluminix.jpg', 'Persianas en Cancún Aluminix Classic', 'Persiana aluminix Cancún Classic', 5, 10, 1),
(15, '5/persianas-aluminix-cancun.jpg', 'Persianas Aluminix Classic - Cancún', 'Persianas en Cancún Decorativas Classic', 5, 10, 1),
(16, '5/persianas-aluminix-cancun-2.jpg', 'Persianas Aluminix Classic - Cancún', 'Persianas en Cancún - Empresa de Decoración', 5, 10, 1),
(19, '5/persianas-cancun-celsus-classic.jpg', 'Persianas Celsus Classic - Cancún', 'Persianas o Cortinas Decorativas en Cancún', 5, 10, 1),
(20, '5/persianas-cancun-celsus-classic-2.jpg', 'Persianas Celsus Classic - Cancún', 'Comprar e instalar persianas en Cancún', 5, 10, 1),
(21, '5/persianas-cancun-celsus-classic-3.jpg', 'Persianas Celsus Classic - Cancún', 'Persianas Blancas Cancún Decoración', 5, 10, 1),
(22, '5/persianas-cancun-celsus-classic-4.jpg', 'Persianas Celsus Classic - Cancún', 'Persianas Decorativas Cancún Blancas', 5, 10, 1),
(23, '5/persianas-cancun-celsus-classic-5.jpg', 'Persianas Celsus Classic - Cancún', 'Persiana Decorativa de Diseño Cancún', 5, 10, 1),
(24, '5/persianas-cancun-celsus-classic-6.jpg', 'Persianas Celsus Classic - Cancún', 'Comprar persianas Decorativas en Cancún', 5, 10, 1),
(25, '5/persianas-cancun-celsus-classic-7.jpg', 'Persianas Celsus Classic - Cancún', 'Persianas decorativas de diseño en Cancún', 5, 10, 1),
(26, '14/candil-en-cancun-decorativo.jpg', 'Candil Decorativo Cancún', 'Lámpara de techo decorativa en Cancún', 14, 12, 1),
(27, '14/candiles-decorativos-cancun.jpg', 'Candil Decorativo Cancún', 'lámpara decorativa en Cancún de techo', 14, 12, 1),
(28, '14/iluminacion-decorativa-cancun.jpg', 'Candil Decorativo redondo Cancún', 'Lámparas decorativas en Cancún', 14, 12, 1),
(29, '14/lampara-decorativa-de-techo-cancun.jpg', 'Candil Decorativo de piedras Cancún', 'Lámpara de techo decorativa de piedras', 14, 12, 1),
(30, '14/lampara-de-techo-decorativa-cancun.jpg', 'Candil Decorativo Cancún', 'Lámpara decorativa de techo con colores', 14, 12, 1),
(31, '14/lamparas-de-diseno-cancun.jpg', 'Candil Decorativo Multicolor Cancún', 'Comprar lamparas decorativas en Cancún', 14, 12, 1),
(32, '7/padel-pared-decorativa-piedra.jpg', '13', 'Image galler Sundec Decoracion', 7, 13, 1),
(33, '7/panel-decorativo-piedra-reino-unido.jpg', '13', 'Image galler Sundec Decoracion', 7, 13, 1),
(34, '7/panel-pared-decoracion-en-piedra.jpg', '13', 'Image galler Sundec Decoracion', 7, 13, 1),
(35, '7/panel-pared-piedra-decoracion.jpg', '13', 'Image galler Sundec Decoracion', 7, 13, 1),
(36, '7/panel-pared-piedra-decorativa.jpg', '13', 'Image galler Sundec Decoracion', 7, 13, 1),
(37, '7/panel-stone-piedra-cancun-1.jpg', '13', 'Image galler Sundec Decoracion', 7, 13, 1),
(38, '8/alfombra-decorativa-cancun.jpg', '14', 'Image galler Sundec Decoracion', 8, 14, 1),
(39, '8/alfombra-modular-cancun.jpg', '14', 'Image galler Sundec Decoracion', 8, 14, 1),
(40, '8/alfombra-modular-cancun-2.jpg', '14', 'Image galler Sundec Decoracion', 8, 14, 1),
(41, '8/alfombra-modular-vital-sala.jpg', '14', 'Image galler Sundec Decoracion', 8, 14, 1),
(42, '8/alfombra-profesional-cancun.jpg', '14', 'Image galler Sundec Decoracion', 8, 14, 1),
(43, '8/alfombras-tapetes-cancun-catalogo.jpg', '14', 'Image galler Sundec Decoracion', 8, 14, 1),
(44, '8/comprar-alfombra-decorativa-cancun.jpg', '14', 'Image galler Sundec Decoracion', 8, 14, 1),
(45, '8/empresas-alfombra-modular-cancun.jpg', '14', 'Image galler Sundec Decoracion', 8, 14, 1),
(46, '8/tapetes-cancun-exterior.jpg', '15', 'Image galler Sundec Decoracion', 8, 15, 1),
(47, '8/tapetes-casa-cancun.jpg', '15', 'Image galler Sundec Decoracion', 8, 15, 1),
(48, '8/tapetes-decorativos-cancun.jpg', '15', 'Image galler Sundec Decoracion', 8, 15, 1),
(49, '8/tapetes-decorativos-cancun-interior.jpg', '15', 'Image galler Sundec Decoracion', 8, 15, 1),
(50, '8/tapetes-para-casa-oficina-cancun.jpg', '15', 'Image galler Sundec Decoracion', 8, 15, 1),
(51, '8/tapetes-para-sala-cancun.jpg', '15', 'Image galler Sundec Decoracion', 8, 15, 1),
(52, '9/cortinas-anticiclonicas-acordeon-2.jpg', '16', 'Image galler Sundec Decoracion', 9, 16, 1),
(53, '9/cortinas-anticiclonicas-acordeon-cancun.jpg', '16', 'Image galler Sundec Decoracion', 9, 16, 1),
(54, '9/cortinas-anticiclonicas-cancun.jpg', '16', 'Image galler Sundec Decoracion', 9, 16, 1),
(55, '9/cortinas-anticiclonicas-cancun-cafe.jpg', '16', 'Image galler Sundec Decoracion', 9, 16, 1),
(56, '9/cortina-anticiclonica-blanca-cancun.jpg', '17', 'Image galler Sundec Decoracion', 9, 17, 1),
(57, '9/persiana-anticiclonia-europea-cancun.jpg', '17', 'Image galler Sundec Decoracion', 9, 17, 1),
(58, '9/persiana-anticiclonica-cancun-cafe.jpg', '17', 'Image galler Sundec Decoracion', 9, 17, 1),
(59, '9/persiana-anticiclonica-cancun-vista-interior.jpg', '17', 'Image galler Sundec Decoracion', 9, 17, 1),
(60, '9/persianas-anticiclonicas-cancun-fachada.jpg', '17', 'Image galler Sundec Decoracion', 9, 17, 1),
(61, '9/persianas-anticiclonicas-cancun-fachadas.jpg', '17', 'Image galler Sundec Decoracion', 9, 17, 1),
(62, '10/celosia-persax-cancun-lujo.jpg', '18', 'Image galler Sundec Decoracion', 10, 18, 1),
(63, '10/celosias-cancun-blancas.jpg', '18', 'Image galler Sundec Decoracion', 10, 18, 1),
(64, '10/celosias-cancun-construccion.JPG', '18', 'Image galler Sundec Decoracion', 10, 18, 1),
(65, '10/celosias-cancun-grandes.JPG', '18', 'Image galler Sundec Decoracion', 10, 18, 1),
(66, '10/celosias-cancun-persax.jpg', '18', 'Image galler Sundec Decoracion', 10, 18, 1),
(67, '10/celosias-decoracion-cancun.JPG', '18', 'Image galler Sundec Decoracion', 10, 18, 1),
(68, '10/celosias-ventanas-cancun.JPG', '18', 'Image galler Sundec Decoracion', 10, 18, 1),
(69, '11/mosquitera-cancun.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(70, '11/mosquitera-cancun-decoracion.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(71, '11/mosquitera-enrollable-cancun.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(72, '11/mosquitera-plisada-cancun.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(73, '11/mosquitera-plisada-cancun-2.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(74, '11/mosquitera-plisada-decorativa.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(75, '11/mosquitera-plisada-exterior.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(76, '11/mosquitera-plisada-teka.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(77, '11/mosquitera-puerta-lateral.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(78, '11/mosquitero-enrollable-cancun-persax.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(79, '11/mosquitero-plisada-teka-cancun.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(80, '11/puerta-mosquitera-cancun.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(81, '11/puerta-mosquitera-lateral-cancun.jpg', '19', 'Image galler Sundec Decoracion', 11, 19, 1),
(82, '12/muebles-butaca-moderna-showtime.jpg', '20', 'Image galler Sundec Decoracion', 12, 20, 1),
(83, '12/muebles-decorativos-cancun-deroviar.jpg', '20', 'Image galler Sundec Decoracion', 12, 20, 1),
(84, '12/muebles-de-diseno-cancun-deroviar.jpg', '20', 'Image galler Sundec Decoracion', 12, 20, 1),
(85, '12/muebles-diseno-cancun.jpg', '20', 'Image galler Sundec Decoracion', 12, 20, 1),
(86, '12/muebles-diseno-sala.jpg', '20', 'Image galler Sundec Decoracion', 12, 20, 1),
(87, '12/muebles-diseno-sala-2.jpg', '20', 'Image galler Sundec Decoracion', 12, 20, 1),
(88, '12/muebles-pedido-diseno-cancun.jpg', '20', 'Image galler Sundec Decoracion', 12, 20, 1),
(89, '12/muebles-sala-sobre-pedido.jpg', '20', 'Image galler Sundec Decoracion', 12, 20, 1),
(90, '12/muebles-sillon-fieltro-gris.jpeg', '20', 'Image galler Sundec Decoracion', 12, 20, 1),
(91, '12/muebles-sobre-diseno-cancun.jpg', '20', 'Image galler Sundec Decoracion', 12, 20, 1),
(92, '13/instalacion-de-toldos-cancun.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(93, '13/precio-toldos-medida-cancun.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(94, '13/toldo-corta-vientos-cancun.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(95, '13/toldo-para-sol-alberca-cancun.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(96, '13/toldos-decorativos-cancun-ceos.jpg', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(97, '13/toldos-decorativos-para-hotel-cancun.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(98, '13/toldos-monoblock-cancun.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(99, '13/toldos-negocio-calle-cancun.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(100, '13/toldos-para-jardin-cancun-palillera.jpg', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(101, '13/toldos-para-negocios-cancun-creta.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(102, '13/toldos-para-sol-cancun.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(103, '13/toldos-para-terrazas-cancun-estor.jpg', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(104, '13/toldos-para-terrazas-punto-recto.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(105, '13/toldos-para-terrazas-restaurante.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(106, '13/toldos-plegables-cancun.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(107, '13/toldos-plegables-cancun-restaurante.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(108, '13/toldos-plegables-exterior-cancun.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(109, '13/toldos-plegables-moviles-cancun.jpg', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(110, '13/toldos-punto-recto-cancun.jpg', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(111, '13/toldos-sombra-cancun.JPG', '21', 'Image galler Sundec Decoracion', 13, 21, 1),
(112, '5/cortina-intimidad-cancun.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(113, '5/cortinas-black-out-cancun.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(114, '5/cortinas-decoracion-cancun.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(115, '5/cortinas-decorativas-cancun-drapes.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(116, '5/cortinas-decorativas-cancun-raso.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(117, '5/cortinas-elegantes-cancun.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(118, '5/cortinas-frescas-cancun.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(119, '5/cortinas-frescas-sala-drapes.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(120, '5/cortinas-frescura-cancun.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(121, '5/cortinas-persianas-frescas.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(122, '5/persianas-bonitas-cancun.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(123, '5/persianas-cancun-blancas-drapes.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(124, '5/persianas-comedor-cancun.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(125, '5/persianas-cortinas-black-out.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(126, '5/persianas-cortinas-habitacion-cancun.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(127, '5/persianas-decorativas-cancun-nuit.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(128, '5/persianas-interiores-cancun.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(129, '5/persianas-lino-cies-cancun.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1),
(130, '5/persianas-luna-bo-cancun.jpg', '11', 'Image galler Sundec Decoracion', 5, 11, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anclas`
--

CREATE TABLE IF NOT EXISTS `anclas` (
  `idAncla` int(11) NOT NULL AUTO_INCREMENT,
  `ancla` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `fkCategory` int(11) NOT NULL,
  PRIMARY KEY (`idAncla`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Volcado de datos para la tabla `anclas`
--

INSERT INTO `anclas` (`idAncla`, `ancla`, `status`, `fkCategory`) VALUES
(10, 'Persianas Classic', 1, 5),
(11, 'Cortinas Drapes by Dues', 1, 5),
(12, 'candiles', 1, 14),
(13, 'Panel Stone', 1, 7),
(14, 'Alfombra Modular', 1, 8),
(15, 'Tapetes', 1, 8),
(16, 'Cortinas Anticiclonicas Cuprum', 1, 9),
(17, 'Persianas Anticiclonicas Persax', 1, 9),
(18, 'Persax', 1, 10),
(19, 'Mosquiteras Persax', 1, 11),
(20, 'Muebles Sundec', 1, 12),
(21, 'Toldos Persax Cancun', 1, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `idCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(255) NOT NULL,
  `photoCategory` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `altProfile` varchar(255) NOT NULL,
  `titleLink` varchar(255) NOT NULL,
  `estado` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCategoria`, `categoria`, `photoCategory`, `descripcion`, `altProfile`, `titleLink`, `estado`) VALUES
(5, 'Persianas y Cortinas', 'persianas-y-cortinas-cancun.jpg', 'Persianas y Cortinas en Cancún, por Sundec Decoración.<div><br /></div><div>La flexibilidad en el diseño de los espacios es clave en las construcciones modernas.<div><br /></div><div>&nbsp;Es por esto que a partir de nuestras líneas de Persianas y Cortinas, desarrollamos una gran variedad de soluciones integrales para la decoración en casas, oficinas, proyectos de arquitectos y decoradores de interiores.</div></div>', 'Persianas y Cortinas en Cancún', 'Persianas y Cortinas en Cancún', 1),
(6, 'Muebles', 'default.jpg', '', '', '', 0),
(7, 'Acabados en Pared', 'panel-pared-piedra-decoracion.jpg', 'Acabados en Pared en Cancún. Por su presencia visual, las paredes adquieren un destacado protagonismo dentro de la decoración.<div><br /></div><div>Contamos con un catálogo completo de Acabados en Pared para la decoración de tu casa u Hotel en Cancún y la Riviera Maya.</div>', 'acabados en pared de piedra', 'Acabados en Pared - Catálogo', 1),
(8, 'Alfombras y Tapetes', 'alfombras-tapetes-cancun-catalogo.jpg', '<div>Alfombras y Tapetes en Cancún.</div><div><br /></div>En Sundec Decoración somos especialistas en Diseño de Interiores y tenemos las mejores opciones en Alfombras y Tapetes para la decoración de tu casa u oficina en Cancún, Riviera Maya y Playa del Carmen.', 'alfombras y tapetes en cancun catalogo', 'Alfombras y Tapetes - Catálogo', 1),
(9, 'Anticiclónicas', 'cortinas-anticiclonicas-cancun.jpg', 'Cortinas Anticiclónicas.<div><br /></div><div>Sundec Decoración provee de cortinas anticiclónicas para tu casa u Hotel en Cancún, con una variedad de opciones para integrarlas con tu arquitectura.</div>', 'Proveedor de cortinas anticiclonicas en Cancún', 'Cortinas Anticiclónicas Cancún - Catálogo', 1),
(10, 'Celosías', 'celosias-cancun-persax.jpg', '<div>Celosías en Cancún.</div><div><br /></div>La solución más elegante para la protección solar.<div><br /></div><div>Con nuestra gama de celosías, Persax ofrece multitud de aplicaciones en cerramientos de fachadas, patios, terrazas etc. Sus ventajas son claras: protección solar, fácil montaje en obra, colores resistentes a la luz y, por supuesto, un gran valor estético.</div>', 'Celosias diseño en Cancún', 'Celosías en Cancún', 1),
(11, 'Mosquiteras', 'mosquiteras-cancun-persax.jpg', '<div>Mosquiteras en Cancún. Enrollables o correderas.</div><div><br /></div>Todos nuestros diseños están estudiados para cumplir con todas las exigencias, tanto estéticas como de calidad. Protección y belleza, además de su función de protección de la entrada de insectos, en PERSAX siempre piensan en la belleza de los acabados.', 'Mosquiteras en Cancún enrollables', 'Mosquiteras enrollables Cancún', 1),
(12, 'Muebles sobre Diseño', 'muebles-sobre-diseno-cancun.jpg', 'En todo proceso de decoración, ya sea un proyecto de cero o una reforma, se necesita elegir los muebles que se van a utilizar.<div><br /></div><div>Si tienes una idea específica de lo que quieres en SUNDEC plasmamos tu idea y creamos tu diseño en conjunto con nuestra selección de telas para tapicería.</div>', 'Muebles de diseño en Cancún', 'Muebles sobre diseño en Cancún', 1),
(13, 'Toldos', 'toldos-cancun-andros.jpg', '<div>Toldos en Cancún.</div><div><br /></div>En Sundec Decoración proveemos e instalamos toldos para tu Casa, Hotel u Oficina, siempre cuidando la línea de diseño. Contamos con marcas como Persax, Andros y más, de una calidad que harán que el ambiente en el que se instalen sea armonioso.', 'Toldos en Cancún para sombra exterior', 'Toldos en Cancún - Sundec', 1),
(14, 'Iluminación', 'iluminacion-candiles-cancun.jpg', 'Iluminación en Cancún.<div><br /></div><div>La luz se convierte en un elemento muy influyente en la decoración y la ambientación de la estancia.&nbsp;</div><div><br /></div><div>Según la cantidad y el tipo de luz que tengamos el espacio puede verse más grande o más chico, los colores se modifican y las sombras nos juegan un efecto visual en la forma de los muebles.</div>', 'Candiles en Cancún decoración interior', 'Candiles en Cancún', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE IF NOT EXISTS `contacto` (
  `idContacto` int(11) NOT NULL AUTO_INCREMENT,
  `firstAddres` varchar(255) NOT NULL,
  `secondAddres` varchar(255) NOT NULL,
  `lastAddres` varchar(255) NOT NULL,
  `mailContacto` varchar(255) NOT NULL,
  `phoneContacto` varchar(255) NOT NULL,
  `mailForm` varchar(255) NOT NULL,
  `latContacto` varchar(255) NOT NULL,
  `longContacto` varchar(255) NOT NULL,
  PRIMARY KEY (`idContacto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`idContacto`, `firstAddres`, `secondAddres`, `lastAddres`, `mailContacto`, `phoneContacto`, `mailForm`, `latContacto`, `longContacto`) VALUES
(1, 'Av. Huayacan Zona 5 ', 'Manzana 101 Lote 29, Planta Alta', 'Cancún, Quintana Roo C.P.77560', 'ventas@sundecdecoracion.com', '(998) 888 9292', 'noelurbainflores@gmail.com;amendez@kaviramx.com', '21.118408', '-86.845741');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria`
--

CREATE TABLE IF NOT EXISTS `galeria` (
  `idPhotoYacht` int(11) NOT NULL AUTO_INCREMENT,
  `idPage` int(11) NOT NULL,
  `imageGallery` varchar(255) NOT NULL,
  `altImageGallery` varchar(255) NOT NULL DEFAULT 'alt of image',
  `viewTitle` varchar(255) NOT NULL DEFAULT 'Title of image',
  `viewDescription` varchar(255) NOT NULL DEFAULT 'Description of image',
  PRIMARY KEY (`idPhotoYacht`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE IF NOT EXISTS `marcas` (
  `idmarca` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(255) NOT NULL,
  `logotipo` varchar(255) NOT NULL,
  `descripcion` text,
  `linkTo` varchar(255) DEFAULT '#',
  `estado` int(1) NOT NULL DEFAULT '1',
  `titleLink` varchar(255) DEFAULT 'Marcas Sundec',
  `altLogotipo` varchar(255) DEFAULT 'Marcas Sundec',
  PRIMARY KEY (`idmarca`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`idmarca`, `marca`, `logotipo`, `descripcion`, `linkTo`, `estado`, `titleLink`, `altLogotipo`) VALUES
(7, 'Classic', 'persianas-cortinas-classic.jpg', 'Classic Persianas y Cortinas de alta calidad. Marca orgullosamente mexicana con más de 23 años en constante innovación para presentar las últimas tendencias en decoración de ventanas.', 'catalogo/persianas-cortinas-cancun#Classic', 1, 'Persianas y Cortinas Classic en Cancún por Sundec Decoración', 'Persianas y cortinas classic en Cancún'),
(8, 'American Blinds', 'marca-american-blinds-persianas-cancun.jpg', NULL, NULL, 1, NULL, 'Hay cortinas american blinds en Cancún'),
(9, 'Arte Línea', 'marca-artelinea-telas-decorativas-cancun.jpg', 'Arte Línea. Telas que seducen.', 'catalogo/muebles-sobre-diseno', 1, 'Muebles sobre diseño en Cancún con Arte Línea', 'Empresa de muebles de diseño con arte linea en Cancún'),
(10, 'Cappa', 'marca-cappa-pisos-cancun.jpg', 'Pisos y acabados de pisos.', NULL, 1, NULL, 'Quien instala pisos en Cancún con la marca cappa'),
(11, 'Coventino', 'marca-coventino-pisos-madera-cancun.jpg', 'Coventino. Pisos de Madera', NULL, 1, NULL, 'Pisos de madera en Cancún marca Coventino'),
(12, 'Deroviar', 'marca-deroviar-muebles-cancun.jpg', 'Deroviar Muebles en Cancún, con Sundec Decoración', 'catalogo/muebles-sobre-diseno', 1, 'Muebles sobre diseño en Cancún con Deroviar', 'Muebles Deroviar en Cancún comprar o mandar pedido'),
(13, 'Dues by Drapes', 'marca-dues-drapes-persianas-cortinas-cancun.jpg', 'Cortinas y Persianas de calidad con Sundec Decoración en Cancún.', 'catalogo/persianas-cortinas-cancun#Persianas Drapes by Dues', 1, 'Persianas y Cortinas Drapes by Dues', 'Cortinas y Persianas Dues by Drapes en Cancún'),
(14, 'Innover', 'marca-innover-decks-madera-cancun.jpg', 'Innover. Decks dde madera en Cancún proporcionados por Sundec Decoración.', NULL, 1, NULL, 'Decks de madera en Cancún de la marca Innover'),
(15, 'Laminatti', 'marca-laminatti-acabados-pared-piedra-cancun.jpg', 'Laminatti. Acabados de piedra para pared en Cancún', 'catalogo/acabados-en-pared', 1, 'Laminatti acabados en Pared Cancún', 'Laminatti acabados de piedra para pared en Cancún'),
(16, 'Le Glans', 'marca-le-glans-textiles-cancun.jpg', 'Le Glands Textiles.', NULL, 1, NULL, 'Le Glans textiles en Cancún decorativos'),
(17, 'Panel Stone', 'marca-panel-piedra-pared-cancun.jpg', 'Panel Stone o Panel Piedra. Acabados de piedra para pared en Cancún.', 'catalogo/acabados-en-pared#Panel Stone', 1, 'Panel Stone Cancún', 'Colocar pared de panel stone en Cancún'),
(18, 'Persax', 'marca-persax-persianas-toldos-cancun.jpg', 'Persax. Toldos, sombrillas, telax y más soluciones para exteriores.', 'catalogo/toldos#Toldos Persax Cancun', 1, 'Toldos Persax en Cancún', 'Toldos, cortinas y mas telas para exteriores de persax en Cancún'),
(19, 'Ranka Follaje Sintético', 'marca-ranka-follaje-sintetico-cancun.jpg', 'Follaje sintético decorativo en Cancún.', NULL, 1, NULL, 'Follaje Sintético en Cancún empresa que instale marca Ranka'),
(20, 'Somfy', 'marca-somfy-motores-cortinas-cancun.jpg', 'Mecanismos de apertura de cortinas, toldos, persianas y puertas.', NULL, 1, NULL, 'Somfy mecanismos para abrir cortinas, persianas, puertas en Cancún'),
(21, 'Sunbrella', 'marca-sunbrella-telas-exteriores.jpg', 'Sunbrella Textiles en Cancún.', NULL, 1, NULL, 'Sunbrella textiles para exteriores decorativos en Cancún'),
(22, 'Top Lighting', 'marca-top-lighting-iluminacion-creativa-cancun.jpg', 'Iluminación creativa. Iluminación decorativa traída por Sundec Decoración en Cancún.', 'catalogo/iluminacion', 1, 'Top Lighting en Cancún', 'Top Lighting iluminacion creativa y decorativa en Cancún');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `idNewsletter` int(11) NOT NULL AUTO_INCREMENT,
  `mailNews` varchar(255) NOT NULL,
  `dateSuscribe` date NOT NULL,
  PRIMARY KEY (`idNewsletter`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `newsletter`
--

INSERT INTO `newsletter` (`idNewsletter`, `mailNews`, `dateSuscribe`) VALUES
(1, 'ajimenez.devs@gmail.com', '2014-10-26'),
(2, 'testnewsletter@tester.com', '2014-10-30'),
(3, 'developer4@dtraveller.com', '2014-12-13'),
(4, 'joce_chio@hotmail.com', '2014-12-13'),
(5, 'developer3@dtraveller.com', '2014-12-13'),
(6, 'developer5@dtraveller.com', '2014-12-13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promos`
--

CREATE TABLE IF NOT EXISTS `promos` (
  `idpromo` int(11) NOT NULL AUTO_INCREMENT,
  `promo` varchar(255) NOT NULL,
  `imglist` varchar(255) NOT NULL,
  `imgoverlay` varchar(255) NOT NULL,
  `finicio` date DEFAULT NULL,
  `ffin` date DEFAULT NULL,
  `altPromo` varchar(255) NOT NULL DEFAULT 'Promociones del mes | Sundec',
  `titlePromo` varchar(255) NOT NULL DEFAULT 'Promociones del mes | Sundec',
  PRIMARY KEY (`idpromo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `promos`
--

INSERT INTO `promos` (`idpromo`, `promo`, `imglist`, `imgoverlay`, `finicio`, `ffin`, `altPromo`, `titlePromo`) VALUES
(3, 'Gran remate de Sala', 'promo-row.jpg', 'promocion-sundec.jpg', '2014-12-21', '2020-01-01', 'Remate de sala', 'Gran remate de sala 50%');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seo`
--

CREATE TABLE IF NOT EXISTS `seo` (
  `idSeo` int(11) NOT NULL AUTO_INCREMENT,
  `urlPage` varchar(255) NOT NULL,
  `titlePage` varchar(255) NOT NULL DEFAULT 'Sundec Decoracion',
  `keywordsPage` varchar(255) NOT NULL DEFAULT 'Sundec Decoracion',
  `descriptionPage` text,
  `idYacht` int(11) NOT NULL,
  PRIMARY KEY (`idSeo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Volcado de datos para la tabla `seo`
--

INSERT INTO `seo` (`idSeo`, `urlPage`, `titlePage`, `keywordsPage`, `descriptionPage`, `idYacht`) VALUES
(1, '/index.php', 'Decoración en Cancún | Persianas, Cortinas y Muebles Decorativos | Sundec Decoración', 'decoracion cancun, decoracion en cancun, decoracion de interiores cancun, diseño de interiores cancun, decoracion de cocinas cancun, diseño de cocinas cancun, decoracion de cocinas modernas cancun, decoracion de salas en cancun, persianas decorativas en c', 'Sundec Decoración Cancún. Empresa especializada en Decoración ubicada en Cancún, cuenta con todo tipo de cortinas y persianas decorativas. También se especializa en pisos de madera y muebles Decorativos.', 0),
(20, '/quienes-somos.php', 'Somos una Empresa con Soluciones en Decoración en Cancún | Sundec Decoración', 'empresa de decoracion cancun, decoracion de interiores cancun, diseño de interiores cancun', 'Sundec Decoración. Somos una empresa especializada que da soluciones en Decoración para tus áreas externas e internas, ya sea una casa, Hotel u Oficina. Contamos con Cortinas y Persianas decorativas y toda una solución en iluminación.', 0),
(21, '/contacto.php', 'Contáctanos y te daremos una Solución Decorativa para tu casa o proyecto en Cancún | Sundec Decoración', 'decoracion cancun, decoracion en cancun, decoracion de interiores cancun, diseño de interiores cancun, decoracion de cocinas cancun, diseño de cocinas cancun, decoracion de cocinas modernas cancun, decoracion de salas en cancun, persianas decorativas', 'Contacta con nosotros en Sundec Decoración. Estamos listos para darte la mejor solución en tu proyecto decorativo de interiores o exteriores. Tenemos lo necesario en cortinas, pisos, alfombras, paredes y más en Cancún.', 0),
(25, '/catalogo.php', 'Catálogo de Productos | Persianas | Muebles | Pisos | Lámparas | Sundec Decoración', 'productos decorativos cancun, articulos decorativos cancun, cortinas en cancun, persianas en cancun, empresa de decoracion cancun, lamparas decorativas, muebles de diseño', 'Encuentra en el catálogo de Sundec Decoración una completa gama de productos de Decoración entre Persianas, Muebles y lámparas decorativas.', 0),
(26, '/nuestras-marcas.php', 'Nuestras Marcas | Marcas de Decoración que usamos en Cancún | Sundec Decoración', 'empresas de decoracion, empresas de decoracion en cancun, decoracion en cancun, marcas de decoracion en cancun, marcas de muebles cancun, marcas de pisos cancun, sundec decoracion', 'Encuentra las marcas que Sundec Decoración maneja en Cancún, tenemos una amplia gama de productos para cortinas y persianas, pisos de madera o laminados, toldos. Marcas desde Persax, Classic hasta Ranka y más.', 0),
(27, '/promociones.php', 'Promociones de Persianas y Cortinas en Cancún o Soluciones en Decoración | Sundec Decoración', 'muebles en promocion, promocion de muebles en cancun, promocion de cortinas en cancun, promocion de persianas cancun, outlet de muebles en cancun, promo en muebles cancun, muebles baratos cancun', 'Encuentra Promociones en Cancún de Cortinas, Persianas, Muebles decorativos, Diseño de Interiores, Acabados en pared y más, aprovecha nuestros outlets de temporada. Sundec Decoración', 0),
(29, '/catalogo/persianas-cortinas-cancun', 'Persianas y Cortinas en Cancún | Cortinas para sala, comedor, habitación | Enrollable, PVC | Sundec Decoración', 'persianas en cancun, cortinas en cancun, cortinas decorativas, persianas decorativas, cortinas de diseno,  cortinas modernas, modelos de cortinas, cortinas romanas, cortinas para sala, cortinas enrollables, cortinas black out', 'Persianas y Cortinas en Cancún. Contamos con Persianas y Cortinas para sala, cocina, comedor, hotel u oficina, de todo tipo, romana, enrollable, bamboo, black out, de madera, pvc y más.', 5),
(30, '/catalogo/muebles', 'Muebles de Diseño en Cancún | Muebles decorativos | Sundec Decoracion', 'muebles decorativos, muebles sobre pedido, muebles de diseno cancun, muebles cancun, muebles baratos cancun, comprar muebles cancun, muebles bonitos cancun', 'Muebles decorativos en Cancún. Contamos con muebles sobre diseño que puedes hacer sobre pedido como tú gustes. Sundec Decoración.', 6),
(31, '/catalogo/acabados-en-pared-cancun', 'Acabados en Pared | Cancún | Acabados de pared en Piedra, madera | Sundec Decoracion', 'acabados en pared, pared laminada cancun, decoracion de pared cancun', 'Acabados en Pared. Pared decorativa en Cancún de Piedra, Madera, etc. Sundec Decoración', 7),
(32, '/catalogo/alfombras-tapetes-cancun', 'Alfombras y tapetes en Cancún | Tapetes para Sala o Alfombra Modular para Negocio | Sundec Decoracion', 'tapetes en cancun, alfombras en cancun, alfombra modular, tapetes para sala, comprar tapetes, comprar alfombra, donde comprar alfombra cancun, alfombra cancun, tapete cancun, tapete decorativo cancun', 'Alfombras y tapetes decorativos en Cancún. Tenemos la Solución para tu sala o Negocio con tapetes decorativos o Alfombras modulares que decorarán tu espacio interior.', 8),
(33, '/catalogo/cortinas-anticiclonicas-cancun', 'Cortinas anticiclónicas en Cancún | Persianas anticiclónicas | Sundec Decoracion', 'cortinas anticiclonicas, persianas anticiclonicas, cortinas anticiclonias cancun, anticiclonicas cancun, anticiclonicas', 'Cortinas anticiclónicas de alta seguridad. Encuentra Cortinas y Persianas anticiclónicas de diversas marcas y la más alta seguridad. Hacemos presupuesto e instalación.', 9),
(34, '/catalogo/celosias-cancun', 'Celosías en Cancún | Instalación y Cotización de Celosías | Sundec Decoracion', 'celosias cancun, celosias, celosias decorativas, instalacion de celosias, comprar celosias, celosías de madera, celosias decorativas exterior, celosia pvc, celosias de madera para jardin, celosias de hormigon, celosias para jardin, celosías de aluminio', 'Celosías en Cancún. Contamos con un catálogo surtido y la experiencia en instalación de Celosías en Cancún. Contamos con modelos de Celposías decorativas y resistentes al ambiente duro.', 10),
(35, '/catalogo/mosquiteras-cancun', 'Mosquiteras en Cancún | Decorativas | Enrollables | Correderas y más | Sundec Decoracion', 'mosquiteras en cancun, mosquiteras, instalacion de mosquiteras, mosquiteras decorativas, mosquiteras enrollables, mosquiteras baratas, mosquiteras para ventanas, mosquitera, mosquiteras correderas, mosquiteras para puertas, cortinas mosquiteras, mosquiter', 'Mosquiteras en Cancún o Mosquiteros. Contamos con un catálogo extenso de mosquiteras enrollables, correderas, para puertas o ventanas y son decorativas para el espacio que tu quieras.', 11),
(36, '/catalogo/muebles-sobre-diseno', 'Muebles sobre diseño en Cancún | Muebles sobre pedido a tu medida | Sundec Decoracion', 'muebles baratos, muebles sobre diseño, diseño de muebles, muebles cancun, muebles de diseño, muebles modernos, fabrica de muebles, tiendas de muebles, muebles minimalistas, muebles de oficina, muebles de salon de diseño, muebles contemporaneos, ', 'Muebles en Cancún. Tenemos y creamos muebles sobre diseño o sobre pedido para tu casa, oficina u hotel. Contamos con un catálogo de muebles contemporáneos y minimalistas, modernos.', 12),
(37, '/catalogo/toldos-cancun', 'Toldos en Cancún | Enrollables, plegables y durables | Sundec Decoracion', 'toldos en cancun, toldos, toldos para terrazas, toldos para ventanas, toldos para patios, toldos para balcones, toldos para negocios, toldo para terraza, toldo, toldos plegables, toldos para bares, toldos terraza, toldos para pergolas de madera', 'Toldos en Cancún. Cotización e instalación de toldos en Cancún. Hacemos el toldo decorativo a tu medida, ya sea para una terraza, un balcón o tu negocio en la calle. Son seguros, plegables o enrollables.', 13),
(38, '/catalogo/iluminacion-cancun', 'Iluminación decorativa en Cancún | Lámparas decorativas en Cancún | Sundec Decoracion', 'iluminacion cancun, lamparas cancun, iluminacion, lamparas, lamparas decorativas, iluminacion de interiores, iluminacion decorativa, candelabros decorativos, lamparas de techo decorativas, lampara de techo, lamparas colgantes, lamparas modernas, luminaria', 'Iluminación y lámparas decorativas en Cancún. En Sundec somos expertos en el arte de la iluminación de interiores por lo que contamos con lámparas y diseños decorativos que atraerán a todos tus invitados.', 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `idSetting` int(11) NOT NULL AUTO_INCREMENT,
  `mailHeader` varchar(255) NOT NULL,
  `phoneHeader` varchar(255) NOT NULL,
  `addresFooter` varchar(255) NOT NULL,
  `mailFooter` varchar(255) NOT NULL,
  `phoneFooter` varchar(255) NOT NULL,
  PRIMARY KEY (`idSetting`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`idSetting`, `mailHeader`, `phoneHeader`, `addresFooter`, `mailFooter`, `phoneFooter`) VALUES
(1, 'ventas@sundecdecoracion.com', '8 88 9292', 'Boulevardo Kukulcan Km. 7.5 Zona Hotelera', 'ventas@sundecdecoracion.com', '8 88 9292');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `idSlider` int(11) NOT NULL AUTO_INCREMENT,
  `imageSlider` varchar(255) NOT NULL,
  `textSlider` varchar(255) DEFAULT NULL,
  `altSlider` varchar(255) DEFAULT NULL,
  `linkSlider` int(1) NOT NULL DEFAULT '0',
  `urlToSlider` varchar(255) DEFAULT NULL,
  `titleSlider` varchar(255) DEFAULT NULL,
  `orderSlider` int(11) NOT NULL,
  PRIMARY KEY (`idSlider`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Volcado de datos para la tabla `slider`
--

INSERT INTO `slider` (`idSlider`, `imageSlider`, `textSlider`, `altSlider`, `linkSlider`, `urlToSlider`, `titleSlider`, `orderSlider`) VALUES
(22, 'sundec-decoracion-cortinas.jpg', NULL, 'Cortinas y Persianas en Cancún', 1, 'http://www.sundecdecoracion.com/catalogo/persianas-cortinas-cancun', 'Cortinas y Persianas en Cancún', 1),
(23, 'acabados-en-pared-cancun-panel-stone.jpg', NULL, 'Acabados en Pared decorativos en Cancun de Panel Stone por Sundec', 1, 'www.sundecdecoracion.com/catalogo/acabados-en-pared', 'Acabados en pared en Cancún por Sundec', 2),
(24, 'alfombras-en-cancun-modular-sala.jpg', NULL, 'Donde comprar alfombras en Cancún para sala y Modular', 1, 'www.sundecdecoracion.com/catalogo/alfombras-tapetes', 'Alfombras y tapetes en Cancún', 3),
(25, 'cortina-anticiclonica-cancun-europea.jpg', NULL, 'Cortina Anticiclónica en Cancún para instalar', 1, 'www.sundecdecoracion.com/catalogo/cortinas-anticiclonicas-cancun', 'Cortinas y Persianas anticiclónicas en Cancún', 4),
(26, 'celosias-en-cancun-ventana.jpg', NULL, 'Comprar e instalar celosías e Cancún', 1, 'www.sundecdecoracion.com/catalogo/celosias', 'Celosías en Cancún', 5),
(27, 'mosquiteras-en-cancun-plisada.jpg', NULL, 'Comprar e instalar Mosquiteras en Cancún', 1, 'www.sundecdecoracion.com/catalogo/mosquiteras', 'Mosquiteras en Cancún', 6),
(28, 'muebles-en-cancun-sobre-diseno-interiores.jpg', NULL, 'Muebles sobre diseño para interiores en Cancún', 1, 'www.sundecdecoracion.com/catalogo/muebles-sobre-diseno', 'Muebles sobre Diseño en Cancún', 7),
(29, 'toldos-en-cancun-exterior.JPG', NULL, 'Toldos y Sombrillas en Cancún para terrazas y Jardines', 1, 'www.sundecdecoracion.com/catalogo/toldos', 'Toldos en Cancún', 8),
(30, 'lamparas-decorativas-cancun-iluminacion.jpg', NULL, 'Iluminacion de interiores y lamparas decorativas en Cancún', 1, 'www.sundecdecoracion.com/catalogo/iluminacion', 'Iluminación y lámparas decorativas en Cancún', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `idUsers` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mailUser` varchar(255) NOT NULL,
  PRIMARY KEY (`idUsers`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsers`, `username`, `password`, `mailUser`) VALUES
(1, 'devealex', '5e8edd851d2fdfbd7415232c67367cc3', 'ajimenez.devs@gmail.com'),
(2, 'emarketing', '30dd0ca73c5d7388757579cc2cc99f9a', 'noelurbainflores@gmail.com'),
(3, 'sundecdecoracion', 'fbaadf0092f5cd6d0295e9728605f4eb', 'amendez@kaviramx.com'),
(5, 'kaviramx', 'da71033e4e66ad8a8ede30cbc825d2c6', 'amendez@kaviramx.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE IF NOT EXISTS `visitas` (
  `idVisita` int(11) NOT NULL AUTO_INCREMENT,
  `pageVisit` varchar(255) NOT NULL,
  `dateVisit` date NOT NULL,
  PRIMARY KEY (`idVisita`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `yates`
--

CREATE TABLE IF NOT EXISTS `yates` (
  `idYacht` int(11) NOT NULL AUTO_INCREMENT,
  `titleYacht` varchar(255) NOT NULL,
  `imageYacht` varchar(255) NOT NULL,
  `altThumb` varchar(255) DEFAULT NULL,
  `priceYacht` varchar(255) DEFAULT NULL,
  `contentYacht` text NOT NULL,
  `categoryAcht` int(11) NOT NULL,
  PRIMARY KEY (`idYacht`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
