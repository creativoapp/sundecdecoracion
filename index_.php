<?php include('includes/header.php'); ?>


<section class="container">
	<!--BOXSLIDER-->
	<div class="boxSlider">
		<?php 
   			$slider = new Slider();
   			echo $slider->showSlider();
   		?>
   		<nav class="navcats">
   			<ul>
   				<li><a href="#">Toldos</a></li>
   				<li><a href="#">Tapiceria</a></li>
   				<li><a href="#">Puertas y Mosquiteros</a></li>
   				<li><a href="#">Pisos</a></li>
   				<li><a href="#">Persianas y Cortinas</a></li>
   				<li><a href="#">Pared</a></li>
   				<li><a href="#">Muebles sobre diseño</a></li>
   				<li><a href="#">Iluminación</a></li>
   				<li><a href="#">Celosias</a></li>
   				<li><a href="#">Decoración</a></li>
   			</ul>
   		</nav>
	</div>
	<div class="sombra-slider"><img src="<?php echo $path.'sources/sombra-slider.png'; ?>" /></div>
	<!--END BOXSLIDER-->

	<!--CATEGORIAS-->
	<div class="row threeBox-home">
		<article class="column one-third">
			<div class="item-cats">
				<div class="mask"><a href="#"><span>+</span> VER MAS</a></div>
				<img src="<?php echo $path.'sources/persianas.jpg'; ?>" alt="" />
			</div>
			<h1><a href="#">PERSIANAS</a></h1>
		</article>
		<article class="column one-third">
			<div class="item-cats">
				<div class="mask"><a href="#"><span>+</span> VER MAS</a></div>
				<img src="<?php echo $path.'sources/celosias.jpg'; ?>" alt="" />
			</div>
			<h1><a href="#">CELOSIAS</a></h1>
		</article>
		<article class="column one-third">
			<div class="item-cats">
				<div class="mask"><a href="#"><span>+</span> VER MAS</a></div>
				<img src="<?php echo $path.'sources/muebles.jpg'; ?>" alt="" />
			</div>
			<h1><a href="#">MUEBLES</a></h1>
		</article>
		<div class="clr"></div>
	</div>
	<!--END CATEGORIAS-->

	<section class="columns sixteen sundecHome">
		<h1>DECORACIÓN EN CANCÚN</h1>
		<h2>Sundec Decoración</h2>
		<p>Sundec Decoración en Cancún, es una empresa mexicana establecida en la ciudad de Cancún, Q. Roo, dedicada a la 
		venta de productos de alta calidad que cubran las necesidades más exigentes para el mercado de la decoración 
		residencial, comercial y hotelera, buscando siempre la satisfacción plena del usuario final, a través de dicha 
		calidad y un excelente servicio.</p>
		<p>Sundec Decoración cuenta con varios Productos y Servicios para la Decoración de su casa u Hotel en Cancún, 
		entre los cuales destacan sus Cortinas y Persianas que pueden ser colocadas en cualquier establecimiento de Cancún. 
		Cuentan con Persianas anticiclónicas para ser instaladas cuanddo lo deseen.</p>
		<p>Entre las especialidades de Decoración, tienen Lámparas decorativas con la última nota en Diseño, así como con 
		Muebles Decorativos de diseño que encajará perfectamente con tu sala o el cuarto de un Hotel.</p>
	</section>
	<div class="clr"></div>

	<!--VIDEO-->
	<div class="columns sixteen boxvideo">
		<video controls >
		  	<source src="sources/sundec.mp4" type="video/mp4">
			Your browser does not support the video tag.
		</video>
	</div><div class="clr"></div>
	<br><br><br><br>
	<!--END VIDEO-->

</section>
	
<?php include('includes/footer.php'); ?>