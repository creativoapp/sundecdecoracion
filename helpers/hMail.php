<?php
	require_once('../models/BD.php');

	/**
	* noelurbainflores@gmail.com;amendez@kaviramx.com
	*/
	class sendMail extends BD
	{
		function getSendMail()
		{
			$bd = $this->openBD();
			$query = $bd->prepare('SELECT mailForm FROM contacto');
			$query->execute();	
		
			$field = $query->fetch(PDO::FETCH_ASSOC);
			return $field['mailForm'];
			$this->closeBD($bd);
		}


		function sends($destino, $mail, $from)
		{
			$headers = 'MIME-Version:1.0'. "\r\n".
			   			'Content-type:text/html; charset=UTF-8'."\r\n".
			   			'From: info@sundecdecoracion.com'. "\r\n" ;
		
			$itera = explode(';', $destino);
			for ($i = 0; $i < count($itera); $i++) { 
				
				mail($itera[$i], 'Contacto Sitio Web', $mail, $headers);
				
			}

			$response = json_encode(array('state' => 'Send', 'message' => 'Hemos recibido su información satisfactoriamente, nos comunicaremos con usted a la brevedad posible.'));
			return $response;
			
		}


		function makeMail($name, $phone, $message, $from)
		{
			if(empty($phone)){ $phone = 'No proporciono'; }
			
			$body = '<span style="font-size:24px;">Formulario de contacto del sitio web</span>
					 <br><br>
					 <p>El usuario con los siguientes datos nos contacto:</p><br>			 
					 <strong>Nombre:</strong> '.$name.'<br>
					 <strong>Correo Electrónico:</strong> '.$from.'<br>
					 <strong>Teléfono:</strong>'.$phone.'<br>
					 <strong>Mensaje:</strong> 
					 <p>'.$message.'</p>';

			return $body;
		
		}	


	}
	

	if(isset($_POST['mail']) & !empty($_POST['message']))
	{
		$sendMail = new sendMail();
		$destino = $sendMail->getSendMail();

		$mail = $sendMail->makeMail($_POST['name'], $_POST['phone'], $_POST['message'], $_POST['mail']);
		echo $sendMail->sends($destino, $mail, $_POST['mail']);
	}

?>