<?php
	require_once('../models/mBrands.php');
	$brand = new Brands();
	
	if(isset($_POST['idMarca']))
	{

		echo $brand->getInfoMarca($_POST['idMarca']);		

	}

	if(isset($_POST['project']) && $_POST['project'] != NULL) {

		require_once('../models/mProjects.php');
		$project = new Projects();

		$projectid = filter_input(INPUT_POST, 'project', FILTER_SANITIZE_STRING);
		echo $project->getProject($projectid);
	}


	if(isset($_POST['catalogo']) && $_POST['catalogo'] != NULL) {

		require_once('../models/mCatalogo.php');
		$catalog = new Catalog();

		$catalogoid = filter_input(INPUT_POST, 'catalogo', FILTER_SANITIZE_STRING);
		echo $catalog->getCatalogoPdf($catalogoid);
			
	}


	if(isset($_POST['modulo']) && $_POST['modulo'] != NULL) {

		require_once('../models/mCatalogo.php');
		$catalog = new Catalog();

		$modulo = filter_input(INPUT_POST, 'modulo', FILTER_SANITIZE_STRING);
		echo $catalog->createModule($modulo);
			
	}


	if(isset($_POST['deletemod']) && $_POST['deletemod'] != NULL) {

		require_once('../models/mCatalogo.php');
		$catalog = new Catalog();

		echo $catalog->deleteModule($_POST['deletemod']);
			
	}

	if(isset($_POST['editmod']) && $_POST['editmod'] != NULL) {

		require_once('../models/mCatalogo.php');
		$catalog = new Catalog();

		echo $catalog->updateModule($_POST['namemod'], $_POST['editmod']);
			
	}



?>