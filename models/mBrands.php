<?php
require_once('BD.php');

//@Class::BRANDS
//@Autor::Alex Jimenez
//@Clase para la gestion/creacion de marcas
class Brands extends BD
{

	private $folderSources;

	function __construct()
	{
	 	$this->folderSources = 'http://www.sundecdecoracion.com/sources/marcas/';
	}


	//@Method::listPages
	//@Autor::Alex Jimenez
	//@Metodo que lista las paginas creadas
	function listBrands()
	{
		$bd = $this->openBD();
		$filter = $bd->prepare('SELECT  * FROM marcas WHERE estado = 1');
		$filter->execute();

		define('_PAGES', 30);
		if(isset($_GET['page']))
		{
		  	$page = $_GET['page'];
		}
		else
		{
		    $page = 1;
		}
		
		$inicio = ($page - 1) * _PAGES;
		$registros = $filter->rowCount();
		$noPages = ceil($registros / _PAGES);
		
		
		$query = $bd->prepare('SELECT * FROM marcas WHERE estado = 1 ORDER BY marca ASC LIMIT :inicio,'._PAGES.' ');	
		$query->bindParam('inicio', $inicio, PDO::PARAM_INT);
		$query->execute();

		$table = '<table class="tResults tPages searchResults">
				  <tr>
				  		<td>Marca</td><td>Link a</td><td>title Link</td><td>alt Logotipo</td><td colspan="2">&nbsp;</td>
				  </tr>';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

			$hide = $_SESSION['rol'] != 3 ? '' : 'style="display:none;"';
						
			$table .= '<tr>
							<td>'.$row['marca'].'</td>
							<td>'.$row['linkTo'].'</td>
							<td>'.$row['titleLink'].'</td>
							<td>'.$row['altLogotipo'].'</td>
							<td><a href="#" '.$hide.' data-reveal-id="editMarca" class="big-link edimarc" data-idmarca="'.$row['idmarca'].'" title="Editar Marca"><img src="sources/edit-action.png" width="20"></a></td>
							<td><a href="brands?idMarca='.$row['idmarca'].'" '.$hide.' title="Eliminar Marca"><img src="sources/delete-action.png" width="20"></a></td>
					   </tr>';
		}

		$table .= '</table>';

		if($noPages > 1)
			{
			    for($y = 1; $y <= $noPages; $y++ )
			    {
			        if($page == $y)
			        {
			            $table .= '<a class="page currentpag" href="brands?page='.$y.'">'.$y.'</a>';
			        }
			        else
			        {
			            $table .= '<a class="page" href="brands?page='.$y.'">'.$y.'</a>';
			        }
			    }
			}
		$this->closeBD($bd);
		return $table;
		
		
		
	}


	//@Method::insertImages
	//@Autor::Alex Jimenez
	//@Metodo para la insercion de imagenes para las galerias
	function insertMarca($args)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('INSERT INTO marcas (marca, logotipo, descripcion, linkTo, titleLink, altLogotipo) VALUES (:marc, :log, :descr, :link, :tit, :alt)');
		$run = $query->execute(array(
									'marc' => $args[0],
									'log' => $args[1],
									'descr' => $args[5],
									'link' => $args[2],
									'tit' => $args[3],
									'alt' => $args[4] ));
		
		if($run == 1)
		{
			$response = json_encode(array('state' => 'succes'));
		}
		else
		{
			$response = json_encode(array('state' => 'failed'));
		}
		
		return $response;
		$this->closeBD($bd);	
	}



	//@Method::objectGallery
	//@Autor::Alex Jimenez
	//@Metodo que imprime la galeria
	function viewBrands()
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM marcas WHERE estado = 1 ORDER BY marca ASC');
		$run = $query->execute();
		
		if($run == 1)
		{
			$brands = '';
			$cont = 1;
			while ($row = $query->fetch(PDO::FETCH_ASSOC)) 
				{
					//$listImages .= '[ENTER]'.$object['imageGallery'];
					$brands .= '<article>
									<div>
										<a href="'.$row['linkTo'].'" title="'.$row['titleLink'].'">
											<img src="'.$this->folderSources.$row['logotipo'].'" alt="'.$row['altLogotipo'].'" />
										</a>
									</div>
									<div>
										<h2><a href="'.$row['linkTo'].'" title="'.$row['titleLink'].'">'.$row['marca'].'</a><h2>
										<p>'.$row['descripcion'].'</p>
										<a href="'.$row['linkTo'].'" title="'.$row['titleLink'].'" class="brandMore">IR A GALERIA</a>
									</div>
								</article>';
					
					if ($cont == 2) {
						$brands .= '<div class="clr"></div>';
						$cont = 1;
					} else {
						$cont++;
					}
				
				}
			return $brands;
				

		}
		
		$this->closeBD($bd);	
	}



	//@Method::getInfoMarca
	//@Autor::Alex Jimenez
	//@Metodo que recupera la informacion de cada marca
	function getInfoMarca($id)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM marcas WHERE idmarca = :id');
		$query->bindParam(':id', $id);
		$run = $query->execute();

		if($run == 1)
		{
			$row = $query->fetch(PDO::FETCH_ASSOC);
			$response = json_encode(array(
										'rState' => 'succes',
										'marca' => $row['marca'],
										'linkto' => $row['linkTo'],
										'attrTitle' => $row['titleLink'],
										'attrAlt' => $row['altLogotipo'],
										'descr' => $row['descripcion']));
		}
		else
		{
			$response = json_encode(array('rState' => 'failed', 'message' => 'Ocurrio un problema, por favor intentelo más tarde.'));
		}

		return $response;
		$this->closeBD($bd);
	}
	


	//@Method::setInfoImage
	//@Autor::Alex Jimenez
	//@Metodo que actuliza la informacion de las imagenes de la galeria
	function editBrand($marca, $alt, $link, $title, $id, $descr)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('UPDATE marcas SET marca = :marca, descripcion = :des, linkTo = :link, titleLink = :title, altLogotipo = :alt WHERE idmarca = :id');
		$run = $query->execute(array(
									':marca' => $marca,
									':des' => $descr,
									':link' => $link,
									':title' => $title,
									':alt' => $alt,
									':id' => $id));

		if($run == 1)
		{
			$response = json_encode(array('state' => 'succes'));
		}
		else
		{
			$response = json_encode(array('state' => 'failed'));
		}

		return $response;
		//return json_encode(array('id' => $id));;
		$this->closeBD($bd);
	}


	//@Method::deleteImage
	//@Autor::Alex Jimenez
	//@Metodo que eliminar la imagen seleccionada
	function deleteBrand($id)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('DELETE FROM marcas WHERE idmarca = :id');
		$query->bindParam(':id', $id);
		$run = $query->execute();

		if($run == 1)
		{
			$response = json_encode(array('state' => 'succes'));
		}
		else
		{
			$response = json_encode(array('state' => 'failed'));
		}

		return $response;
		$this->closeBD($bd);
	}


}

?>