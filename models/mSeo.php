<?php
require_once('BD.php');

//@Class::SEO
//@Autor::Alex Jimenez
//@Clase para la gestion del seo en sitio
class Seo extends BD
{

	private $folderSlider;

	function __construct()
	{
	 	$this->folderSlider = 'http://www.yachtrentalsincancun.com/sources/slider/';
	}	
	//@Method::showList
	//@Autor::Alex Jimenez
	//@Metodo la impresion de todas las paginas con seo	
	function showList($type)
	{
		
		
		$bd = $this->openBD();
		if(!empty($type))
		{
			$filter = $bd->prepare('SELECT * FROM seo WHERE urlPage LIKE "%":search"%"');
			$filter->bindParam('search', $type);
		}
		else
		{
			$filter = $bd->prepare('SELECT * FROM seo');
		}
		$filter->execute();

		define('_PAGES', 25);
		if(isset($_GET['page']))
		{
		  	$page = $_GET['page'];
		}
		else
		{
		    $page = 1;
		}
		
		$inicio = ($page - 1) * _PAGES;
		$registros = $filter->rowCount();
		$noPages = ceil($registros / _PAGES);
		
		if(!empty($type))
		{
			$query = $bd->prepare('SELECT * FROM seo WHERE urlPage LIKE "%":search"%" ORDER BY idSeo ASC LIMIT :inicio,'._PAGES.' ');
			$query->bindParam('search', $type);
		}
		else
		{
			$query = $bd->prepare('SELECT * FROM seo ORDER BY idSeo ASC LIMIT :inicio,'._PAGES.' ');
		}
		
		$query->bindParam('inicio', $inicio, PDO::PARAM_INT);
		$query->execute();

		$table = '<table class="tResults tSeo">
				  <tr>
				  		<td>URL</td><td>Title</td><td>Keywords</td><td>Description</td><td>&nbsp;</td>
				  </tr>';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
			$table .= '<tr>
							<td>'.$row['urlPage'].'</td>
							<td>'.$row['titlePage'].'</td>
							<td>'.$row['keywordsPage'].'</td>
							<td>'.$row['descriptionPage'].'</td>
							<td><a href="#" data-reveal-id="myModal" class="big-link seo" data-idseo="'.$row['idSeo'].'" title="Edita SEO"><img src="sources/edit-action.png" width="20"></a></td>
					   </tr>';
			}

		$table .= '</table>';

		if($noPages > 1)
			{
			    for($y = 1; $y <= $noPages; $y++ )
			    {
			        if($page == $y)
			        {
			            $table .= '<a class="page currentpag" href="seo?page='.$y.'">'.$y.'</a>';
			        }
			        else
			        {
			            $table .= '<a class="page" href="seo?page='.$y.'">'.$y.'</a>';
			        }
			    }
			}
		$this->closeBD($bd);
		return $table;

		
		
	}

	function getSeo($page)
	{
		$bd = $this->openBD();
		$seo = $bd->prepare('SELECT * FROM seo WHERE urlPage = :page');
		$seo->bindParam('page', $page);
		$seo->execute();

		while ($row = $seo->fetch(PDO::FETCH_ASSOC)) 
		{
			$tags = json_encode(array(
									'exist' => 'true',
									'title' => $row['titlePage'],
									'keywords' => $row['keywordsPage'],
									'description' => $row['descriptionPage']));
			/*$tagSeo .= '<title>'.$row['titlePage'].'</title>';
			$tagSeo .= '<meta name="keywords" content="'.$row['keywordsPage'].'" />';
			$tagSeo .= '<meta name="description" content="'.$row['descriptionPage'].'" />';*/
		}
		$this->closeBD($bd);
		return $tags;
	}

	
	function formSeo($idseo)
	{
		$bd = $this->openBD();
		$stmt = $bd->prepare('SELECT * FROM seo WHERE idSeo = :idseo');
		$stmt->bindParam('idseo', $idseo);
		$stmt->execute();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) 
		{
			$title = $row['titlePage'];
			$keywords = $row['keywordsPage'];
			$description = $row['descriptionPage'];
		}

		$formedit = '<form name="editseo" id="editseo" action="" method="post">
						<fieldset>
							<label>Title</label>
							<input type="text" name="seo-title" id="seo-title" value="'.$title.'" />
							<label>Keywords</label>
							<textarea name="seo-keywords" id="seo-keywords" rows="3">'.$keywords.'</textarea>
							<label>Description</label>
							<textarea name="seo-description" id="seo-description" rows="5">'.$description.'</textarea>
							
						</fieldset>
					</form>';
		$this->closeBD($bd);
		return $formedit; 

	}

	function setSeo($arguments)
	{
		$bd = $this->openBD();
		$stmt = $bd->prepare('UPDATE seo SET  titlePage = :title, keywordsPage = :keywords, descriptionPage = :description WHERE idSeo = :id');
		$stmt->bindParam('title', $arguments[0]);
		$stmt->bindParam('keywords', $arguments[1]);
		$stmt->bindParam('description', $arguments[2]);
		$stmt->bindParam('id', $arguments[3]);
		$stmt->execute();

		$response = json_encode(array('state' => 'Edited', ));
		$this->closeBD($bd);
		return $response;
	}


}

?>