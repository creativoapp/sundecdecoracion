<?php
require_once('BD.php');

//@Class::CATALOGO
//@Autor::Alex Jimenez
//@Clase para la gestion/creacion de categorias del catalogo
class Catalog extends BD
{

	private $folderSources;
	private $srcgalls;

	function __construct()
	{
	 	$this->folderSources = 'http://www.sundecdecoracion.com/sources/catalogo/';
	 	//$this->folderSources = 'http://www.webcancun.com.mx/sundec/sources/catalogo/';
	 	$this->srcgalls = 'http://www.sundecdecoracion.com/sources/galerias/';
	 	//$this->srcgalls = 'sources/galerias/';
	}


	//@Method::listPages
	//@Autor::Alex Jimenez
	//@Metodo que lista las paginas creadas
	function listCats()
	{
		$bd = $this->openBD();
		$filter = $bd->prepare('SELECT * FROM categorias, seo WHERE categorias.idCategoria = seo.idYacht AND categorias.estado = 1');
		$filter->execute();

		define('_PAGES', 30);
		if(isset($_GET['page']))
		{
		  	$page = $_GET['page'];
		}
		else
		{
		    $page = 1;
		}
		
		$inicio = ($page - 1) * _PAGES;
		$registros = $filter->rowCount();
		$noPages = ceil($registros / _PAGES);
		
		$query = $bd->prepare('SELECT * FROM categorias, seo WHERE categorias.idCategoria = seo.idYacht AND categorias.estado = 1 ORDER BY seo.idSeo ASC LIMIT :inicio,'._PAGES.' ');		
		$query->bindParam('inicio', $inicio, PDO::PARAM_INT);
		$query->execute();

		$table = '<table class="tResults tPages searchResults">
				  <tr>
				  		<td>URL</td><td>Titulo</td><td>alt Imagen</td><td>title Link</td><td colspan="2">&nbsp;</td>
				  </tr>';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
			
			$hide = $_SESSION['rol'] != 3 ? '' : 'style="display:none;"';

			$table .= '<tr>
							<td>'.$row['urlPage'].'</td>
							<td>'.$row['categoria'].'</td>
							<td>'.$row['altProfile'].'</td>
							<td>'.$row['titleLink'].'</td>
							<td><a href="catalogo?edit='.$row['idCategoria'].'" '.$hide.' title="Editar Categoria"><img src="sources/edit-action.png" width="20"></a></td>
							<td><a href="catalogo?delete='.$row['idCategoria'].'" '.$hide.' title="Eliminar Categoria"><img src="sources/delete-action.png" width="20"></a></td>
					   </tr>';
			}

		$table .= '</table>';

		if($noPages > 1)
			{
			    for($y = 1; $y <= $noPages; $y++ )
			    {
			        if($page == $y)
			        {
			            $table .= '<a class="page currentpag" href="yates?page='.$y.'">'.$y.'</a>';
			        }
			        else
			        {
			            $table .= '<a class="page" href="yates?page='.$y.'">'.$y.'</a>';
			        }
			    }
			}
		$this->closeBD($bd);
		return $table;
		
		
		
	}


	//@Method::getSelectPages
	//@Autor::Alex Jimenez
	//@Metodo que obtiene la lista de paginas segun su categoria
	function listCatalogSlider()
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM categorias AS C INNER JOIN seo AS S WHERE C.idCategoria = S.idYacht AND C.estado = 1 ORDER BY categoria ASC');
		$run = $query->execute();


		$listHtml = '';
		if($run == 1)
		{
			while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
			
			$listHtml .= '<li><a href="'.$row['urlPage'].'" title="'.$row['titlePage'].'">'.$row['categoria'].'</a></li>';
			}
		}
		
		$this->closeBD($bd);
		return $listHtml;

	}

	
	//@Method::getSelectPages
	//@Autor::Alex Jimenez
	//@Metodo que obtiene la lista de paginas segun su categoria
	function getSelectPages($categoria)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM yates WHERE categoryAcht = :categoria');
		$query->bindParam(':categoria', $categoria);
		$run = $query->execute();
		
		if($run == 1)
		{
			$listPages = array();
			while($row = $query->fetch(PDO::FETCH_ASSOC))
			{
				array_push($listPages, $row['titleYacht'] .'*'. $row['idYacht']);
			}
			
		}
		
		return json_encode($listPages);
		$this->closeBD($bd);	
	}


	//@Method::insertPage
	//@Autor::Alex Jimenez
	//@Metodo para la creacion de paginas
	function insertCatalog($arguments)
	{
		$bd = $this->openBD();
		$query = $bd->prepare('INSERT INTO categorias (categoria, photoCategory, descripcion, altProfile, titleLink, fkcity) VALUES (:cat, :image, :descr, :alt, :title, :fkcity)');
		$run = $query->execute(array(
					':cat' => $arguments[0], 
                    ':image' => $arguments[1],
                    ':descr' => $arguments[2],
                    ':alt' => $arguments[3],
					':title' => $arguments[4],
					':fkcity' => $arguments[6]));

		if($run == 1)
		{
			$page = $bd->lastInsertId();
			$seo = $bd->prepare('INSERT INTO seo (urlPage, idYacht) VALUES (:url, :idcatalog)');
			$runseo = $seo->execute(array(
					':url' => $arguments[5], 
                    ':idcatalog' => $page));

			if($runseo == 1)
			{
				$response = json_encode(array('state' => 'succes', 'message' => 'Se agrego la categoria '.$arguments[0].' al catalogo correctamente.' ));
			}
			else
			{
				$delete = $bd->prepare('DELETE FROM categorias WHERE idCategoria = :idcatalog');
				$delete->bindParam('idcatalog', $page);
				$delete->execute();
				$response = json_encode(array('state' => 'failed', 'message' => '<strong>Lo sentimos, ocurrio un problema. Por favor intentelo más tarde.</strong>' ));
			}
			
		}
		else
		{
			$response = json_encode(array('state' => 'failed', 'message' => '<strong>Lo sentimos, ocurrio un problema. Por favor intentelo más tarde.</strong>' ));
		}

		return $response;
	    $this->closeBD($bd);
	}


	//@Method::showArticles
	//@Autor::Alex Jimenez
	//@Metodo para impresion de las paginas
	function showCatalog($fkcity = 1)
	{

		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM categorias AS C INNER JOIN seo AS S WHERE C.estado = 1 AND C.idCategoria = S.idYacht AND C.fkcity = :city');
		$run = $query->execute(array('city' => $fkcity));
		
		if($run == 1)
		{
			$articles = '';
			$cont = 1;
			while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

				$clrHtml = strip_tags($row['descripcion']);
				$destroy = explode(' ', $clrHtml);

				count($destroy) < 30 ? $limite = count($destroy) : $limite = 30;

				$extracto = '';
				for ($i = 0; $i < $limite; $i++) { 
					$extracto .= ' '.$destroy[$i];
				}

				$articles .= '<article class="column one-third boxesCatalog">
									<a href="'.$row['urlPage'].'" title="'.$row['titleLink'].'">
										<img src="'.$this->folderSources.$row['photoCategory'].'" alt="'.$row['altProfile'].'" />
									</a>
							  		<div class="maskcat">
										<div>
											<p>'.$extracto.'...</p>
											<a href="'.$row['urlPage'].'" class="read-more">IR A '.$row['categoria'].'</a>
										</div>
										<h2>'.$row['categoria'].'</h2>
									</div>
							</article>';

				if($cont == 3) {
					$articles .= '<div class="clr"></div>';
					$cont = 1;
				} else { $cont++; }
			}
			return $articles;
		}
		else
		{
			$response = json_encode(array('state' => 'failed', 'message' => '<strong>Lo sentimos, ocurrio un problema. Por favor intentelo más tarde.</strong>' ));
		}
		
		return $articles;
		$this->closeBD($bd);
		
		
	}


	
	//@Method::getYates
	//@Autor::Alex Jimenez
	//@Metodo que obtine la información de una pagina para su edicion
	function getCatalog($id)
	{
		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM categorias, seo WHERE categorias.idCategoria = :id AND categorias.idCategoria = seo.idYacht');
		$query->bindParam('id', $id);
		$exe = $query->execute();

		if($exe == 1){ 
			
			while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
				$response = json_encode(array(
										'name' => $row['categoria'],
										'content' => $row['descripcion'],
										'url' => $row['urlPage'],
										'alt' => $row['altProfile'],
										'title' => $row['titleLink'],
										'city' => $row['fkcity'])); 	
			}  
		}
		else {
			$response = json_encode(array('state' => 'failed'));
		}

		return $response;
		$bd->closeBD();
	}

	//@Method::delPage
	//@Autor::Alex Jimenez
	//@Metodo para eliminar una pagina
	function disabled($id)
	{
		$bd = $this->openBD();
		//$query = $bd->prepare('DELETE FROM Y, S USING yates AS Y INNER JOIN seo AS S WHERE Y.idYacht = :id AND Y.idYacht = S.idYacht');
		$query = $bd->prepare('UPDATE categorias SET estado = 0 WHERE idCategoria = :id');
		$query->bindParam('id', $id);
		$exe = $query->execute();

		if($exe == 1){ $response = json_encode(array('state' => 'succes', )); }  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}

		return $response;
		$bd->closeBD();
	}


	//@Method::updateCatalog
	//@Autor::Alex Jimenez
	//@Metodo para modificar el catalogo
	function updateCatalog($arguments)
	{
		$bd = $this->openBD();
		if(!empty($arguments[1]))
		{			
			$query = $bd->prepare('UPDATE categorias, seo SET categorias.fkcity = :city, categorias.categoria = :name, categorias.photoCategory = :image, categorias.descripcion = :descr, categorias.altProfile = :alt, categorias.titleLink = :title, seo.urlPage = :url  WHERE categorias.idCategoria = :id AND categorias.idCategoria = seo.idYacht');
			$exe = $query->execute(array(
										'name' => $arguments[0],
										'image' => $arguments[1],
										'descr' => $arguments[2],
										'alt' => $arguments[3],
										'title' => $arguments[4],
										'url' => $arguments[5],
										'id' => $arguments[6],
										'city' => $arguments[7]));
		}
		else
		{
			$query = $bd->prepare('UPDATE categorias, seo SET categorias.fkcity = :city, categorias.categoria = :name, categorias.descripcion = :descr, categorias.altProfile = :alt, categorias.titleLink = :title, seo.urlPage = :url  WHERE categorias.idCategoria = :id AND categorias.idCategoria = seo.idYacht');
			$exe = $query->execute(array(
										'name' => $arguments[0],
										'descr' => $arguments[2],
										'alt' => $arguments[3],
										'title' => $arguments[4],
										'url' => $arguments[5],
										'id' => $arguments[6],
										'city' => $arguments[7]));
		}
		
		

		if($exe == 1){ $response = json_encode(array('state' => 'succes', )); }  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}

		return $response;
		$bd->closeBD();
	}

	
	//@Method::showCategory
	//@Autor::Alex Jimenez
	//@Metodo para mostrar en la pagina de categoria los elementos de la pagina el catalogo
	public function showCategory($categoria)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM categorias AS C INNER JOIN seo AS S WHERE S.urlPage = :cat AND C.idCategoria = S.idYacht');
		$query->bindParam('cat', $categoria);
		$run = $query->execute();


		if($run == 1){ 
			$row = $query->fetch(PDO::FETCH_OBJ);
			$response = json_encode(array(
									'page' => $row->categoria,
									'content' => $row->descripcion )); 
		}  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}

		return $response;
		$bd->closeBD();
	}


	//@Method::frmSlctCats
	//@Autor::Alex Jimenez
	//@proporciona select con el catalogo
	public function frmSlctCats($type)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM categorias WHERE estado = 1');
		$query->execute();

		$jQuery = ($type == 'modal') ? 'modaList' : 'listCtalog';
		$select = '<select name="listCtalog" id="'.$jQuery.'">';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
			if(!empty($type))
			{
				if($row['idCategoria'] == $type)
				{
					$select.= '<option value="'.$row['idCategoria'].'" selected>'.$row['categoria'].'</option>';
				}
				else
				{
					$select.= '<option value="'.$row['idCategoria'].'">'.$row['categoria'].'</option>';
				}
			}
			else
			{
				$select.= '<option value="'.$row['idCategoria'].'">'.$row['categoria'].'</option>';
			}
			
		}
		$select.= '</select>';

		return $select;
		$bd->closeBD();

	}


	//@Method::insertAncla
	//@Autor::Alex Jimenez
	//@Metodo para agregar anclas
	public function insertAncla($catalogo, $ancla)
	{
		$bd = $this->openBD();
		$query = $bd->prepare('INSERT INTO anclas (ancla, fkCategory) VALUES (:anc, :cat)');
		$query->bindParam('anc', $ancla);
		$query->bindParam('cat', $catalogo);
		$run = $query->execute();

		if($run == 1)
		{	
			$response = json_encode(array('state' => 'succes', 'message' => 'El ancla '.$ancla.' se añadio correctamente.' ));	
		}
		else
		{
			$response = json_encode(array('state' => 'failed', 'message' => '<strong>Lo sentimos, ocurrio un problema. Por favor intentelo más tarde.</strong>' ));
		}

		return $response;
		$bd->closeBD();

	}



	//@Method::viewAnclas
	//@Autor::Alex Jimenez
	//@Metodo para visualizar y administrar anclas
	public function viewAnclas()
	{
		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM anclas AS A INNER JOIN categorias AS C WHERE A.status = 1 AND A.fkCategory = C.idCategoria ORDER BY C.idCategoria ASC');
		$query->execute();

		$table = '<table class="tResults tPages searchResults">
				  <tr>
				  		<td>Categoria</td><td>Ancla</td><td colspan="2">&nbsp;</td>
				  </tr>';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

			$hide = $_SESSION['rol'] != 3 ? '' : 'style="display:none;"';
			
			$table .= '<tr>
							<td>'.$row['categoria'].'</td>
							<td>'.$row['ancla'].'</td>
							<td><a href="anclas?edit='.$row['idAncla'].'" '.$hide.' title="Editar Ancla"><img src="sources/edit-action.png" width="20"></a></td>
							<td><a href="anclas?delete='.$row['idAncla'].'" '.$hide.' title="Eliminar Ancla"><img src="sources/delete-action.png" width="20"></a></td>
					   </tr>';
			}

		$table .= '</table>';

		return $table;
		$bd->closeBD();


	}


	//@Method::disabledAN
	//@Autor::Alex Jimenez
	//@Metodo para desactivar anclas
	public function disabledAN($id)
	{
		$bd = $this->openBD();
		$query = $bd->prepare('UPDATE anclas SET status = 0 WHERE idAncla = :id');
		$query->bindParam('id', $id);
		$exe = $query->execute();
			

		if($exe == 1){ $response = json_encode(array('state' => 'succes', )); }  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}

		return $response;
		$bd->closeBD();
	}



	//@Method::getAncla
	//@Autor::Alex Jimenez
	//@Metodo para obtener los detalles del ancla
	public function getAncla($id)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM anclas WHERE idAncla = :id');
		$query->bindParam('id', $id);
		$run = $query->execute();


		if($run == 1){ 
			$row = $query->fetch(PDO::FETCH_OBJ);
			$response = json_encode(array(
									'ancla' => $row->ancla,
									'fk' => $row->fkCategory,
									'id' => $row->idAncla )); 
		}  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}

		return $response;
		$bd->closeBD();
	}



	//@Method::updateAncla
	//@Autor::Alex Jimenez
	//@Metodo para modificar el ancla
	public function updateAncla($category, $ancla, $id)
	{
		$bd = $this->openBD();
		$query = $bd->prepare('UPDATE anclas SET ancla = :anc, fkCategory = :fk WHERE idAncla = :id');
		$query->bindParam('anc', $ancla);
		$query->bindParam('fk', $category);
		$query->bindParam('id', $id);
		$exe = $query->execute();
			

		if($exe == 1){ $response = json_encode(array('state' => 'succes', )); }  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}

		return $response;
		$bd->closeBD();
	}



	//@Method::getSelectPages
	//@Autor::Alex Jimenez
	//@Metodo que obtiene la lista de paginas segun su categoria
	function getSelectAnclas($categoria)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM anclas WHERE fkCategory = :categoria AND status = 1');
		$query->bindParam(':categoria', $categoria);
		$run = $query->execute();
		
		if($run == 1)
		{
			$listPages = array();
			while($row = $query->fetch(PDO::FETCH_ASSOC))
			{
				array_push($listPages, $row['ancla'] .'*'. $row['idAncla']);
			}
			
		}
		
		return json_encode($listPages);
		$this->closeBD($bd);	
	}



	//@Method::getGallery
	//@Autor::Alex Jimenez
	//@Metodo que obtiene la galeria completa de la pagina
	function getGallery($page)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM seo, anclas WHERE seo.urlPage = :page AND seo.idYacht = anclas.fkCategory AND anclas.status = 1');
		$query->bindParam(':page', $page);
		$run = $query->execute();
		
		if($run == 1)
		{
			$albums = '';
			while($row = $query->fetch(PDO::FETCH_ASSOC))
			{
				$albums .= '<div id="'.$row['ancla'].'" class="boxAncla"><h3>'.$row['ancla'].'</h3>';
				
				$subquery = $bd->prepare('SELECT * FROM albums WHERE fkAncla = :idancla');
				$subquery->bindParam('idancla', $row['idAncla']);
				$subquery->execute();

				while ($roww = $subquery->fetch(PDO::FETCH_OBJ)) {
					$albums .= '<a class="fancybox" rel="'.$roww->fkAncla.'" href="'.$this->srcgalls.$roww->photo.'" title="'.$roww->name.'">
									<img src="/timthumb.php?src='.$this->srcgalls.$roww->photo.'&w=155&h=100&ac=1&q=90" alt="'.$roww->altPhoto.'" />
								</a>';
				}
				//<img src="../timthumb.php?src='.$this->srcgalls.$roww->photo.'&w=155&az=1&q=90" alt="'.$roww->altPhoto.'" />
				//<img src="../'.$this->srcgalls.$roww->photo.'" alt="'.$roww->altPhoto.'" />

				$albums .= '</div>';

			}
			
		}
		
		return json_encode(array('object' => $albums));
		$this->closeBD($bd);	
	}


	///////
	//PDF//
	///////

	function listFiles() {

		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM catalogospdf WHERE estado = 1 ORDER BY catalogo ASC');	
		$query->execute();

		$response = null;
		if($query->rowCount() > 0) {
			$response = $query->fetchAll();
		}

		return $response;
		$this->closeBD($bd);		
		
	}

	function insertPdf($args) {

		$bd = $this->openBD();
		$sql = $bd->prepare('INSERT INTO catalogospdf (catalogo, archivo, fkmodulo) VALUES (:a, :b, :c)');
		$sql->bindParam('a', $args[0]);
		$sql->bindParam('b', $args[1]);
		$sql->bindParam('c', $args[2]);
		$sql->execute();

		$state = $sql->rowCount() > 0 ? 'succes' : 'failed';
		$response = json_encode(array('state' => $state));
		
		return $response;
		$this->closeBD($bd);	
	}

	function getCatalogoPdf($id) {

		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM catalogospdf WHERE MD5(pkcatalogo) = :id');
		$query->bindParam(':id', $id);
		$query->execute();

		if($query->rowCount() > 0) {

			$row = $query->fetch(PDO::FETCH_ASSOC);
			$response = json_encode(array(
										'catalogo' => $row['catalogo'],
										'archivo' => $row['archivo'],
										'modulo' => $row['fkmodulo']));
		}
		else
		{
			$response = json_encode(array('rState' => 'failed', 'message' => 'Ocurrio un problema, por favor intentelo más tarde.'));
		}

		return $response;
		$this->closeBD($bd);
	}

	function updatePdf($args) {

		$bd = $this->openBD();
		$sql = $bd->prepare('UPDATE catalogospdf SET catalogo = :a, archivo = :b, fkmodulo = :c WHERE MD5(pkcatalogo) = :id');
		$sql->bindParam('a', $args[0]);
		$sql->bindParam('b', $args[1]);
		$sql->bindParam('c', $args[2]);
		$sql->bindParam('id', $args[3]);
		$sql->execute();

		$status = $sql->rowCount() > 0 ? 'succes' : 'failed';
		$response = json_encode(array('state' => $status));
	

		return $response;
		$this->closeBD($bd);
	}

	function deletePdf($id) {

		$bd = $this->openBD();
		$sql = $bd->prepare('DELETE FROM catalogospdf WHERE MD5(pkcatalogo) = :id');
		$sql->bindParam('id', $id);
		$sql->execute();

		$state = $sql->rowCount() > 0 ? 'succes' : 'failed';
		$response = json_encode(array('state' => $state));
		
		return $response;
		$this->closeBD($bd);
	}

	///////////
	//MODULOS//
	///////////

	function listModules() {

		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM modulospdf WHERE estadomod = 1 ORDER BY modulo ASC');	
		$query->execute();

		$response = null;
		if($query->rowCount() > 0) {
			$response = $query->fetchAll();
		}

		return $response;
		$this->closeBD($bd);	

	}

	function createModule($modulo) {

		$bd = $this->openBD();
		$sql = $bd->prepare('INSERT INTO modulospdf (modulo, nivel) VALUES (:a, 1)');
		$sql->bindParam('a', $modulo);
		$sql->execute();

		$state = $sql->rowCount() > 0 ? 'succes' : 'failed';
		$response = json_encode(array('state' => $state));
		
		return $response;
		$this->closeBD($bd);

	}

	function updateModule($modulo, $pk) {

		$bd = $this->openBD();
		$sql = $bd->prepare('UPDATE modulospdf SET modulo = :a WHERE MD5(pkmodulo) = :pk');
		$sql->bindParam('a', $modulo);
		$sql->bindParam('pk', $pk);
		$sql->execute();

		$state = $sql->rowCount() > 0 ? 'succes' : 'failed';
		$response = json_encode(array('state' => $state));
		
		return $response;
		$this->closeBD($bd);

	}

	function deleteModule($pk) {

		$bd = $this->openBD();
		$sql = $bd->prepare('DELETE FROM modulospdf WHERE MD5(pkmodulo) = :pk');
		$sql->bindParam('pk', $pk);
		$sql->execute();

		$state = $sql->rowCount() > 0 ? 'succes' : 'failed';
		$response = json_encode(array('state' => $state));
		
		return $response;
		$this->closeBD($bd);

	}


	function getNameModule($modulo) {

		$bd = $this->openBD();
		$sql = $bd->prepare('SELECT modulo FROM modulospdf WHERE pkmodulo = :modulo');
		$sql->bindParam('modulo', $modulo);
		$sql->execute();

		$response = null;
		if($sql->rowCount() > 0) {
			$row = $sql->fetch(PDO::FETCH_OBJ);
			$response = $row->modulo;
		}

		return $response;
		$this->closeBD();

	}


	//FRONTEND PDF
	function viewAllCatalogs() {

		$bd = $this->openBD();
		$mod = $bd->prepare('SELECT * FROM modulospdf WHERE estadomod = 1 ORDER BY modulo');
		$mod->execute();

		$response = null;
		if($mod->rowCount() > 0) {

			$response = array();
			while ($row = $mod->fetch(PDO::FETCH_OBJ)) {
				
				//GET FILES FOR THIS MODULE
				$file = $bd->prepare('SELECT * FROM catalogospdf WHERE fkmodulo = :mod ORDER BY catalogo');
				$file->bindParam('mod', $row->pkmodulo);
				$file->execute();

				if($file->rowCount() > 0) {
					$files = array();
					while ($roww = $file->fetch(PDO::FETCH_OBJ)) {
						array_push($files, array('catalogo' => $roww->catalogo, 'file' => $roww->archivo));
					}

				}

				if(!empty($files)) {
					array_push($response, array('categoria' => $row->modulo, 'files' => $files));
					$files = null;
				}

			}

		}

		return json_encode($response);
		$this->closeBD($bd);

	}


}

?>