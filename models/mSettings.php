<?php
require_once('BD.php');

//@Class::SETTINGS
//@Autor::Alex Jimenez
//@Clase de configuracion
class Setting extends BD
{

	
	//@Method::getDates
	//@Autor::Alex Jimenez
	//@Metodo que recupera la informacion de impresion en header y footer
	function getDates()
	{
		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM settings');
		$query->execute();

		$jDates = '';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
			$jDates = json_encode(array(
									'mailH' => $row['mailHeader'], 
									'phoneH' => $row['phoneHeader'],
									'addresF' => $row['addresFooter'],
									'mailF' => $row['mailFooter'],
									'phoneF' => $row['phoneFooter'],
									'id' => $row['idSetting']));
		}

		$this->closeBD($bd);
		return $jDates;
		
		
	}


	//@Method::getContacto
	//@Autor::Alex Jimenez
	//@Metodo que recupera la informacion de impresion en la pagina de contacto
	function getContacto()
	{
		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM contacto');
		$query->execute();

		$jContacto = '';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
			$jContacto = json_encode(array(
									'addres1' => $row['firstAddres'], 
									'addres2' => $row['secondAddres'],
									'addres3' => $row['lastAddres'],
									'mailContacto' => $row['mailContacto'],
									'phoneContacto' => $row['phoneContacto'],
									'mailForm' => $row['mailForm'],
									'latitud' => $row['latContacto'],
									'longitud' => $row['longContacto'],
									'id' => $row['idContacto']));
		}

		$this->closeBD($bd);
		return $jContacto;
		
		
	}


	//@Method::showDates
	//@Autor::Alex Jimenez
	//@Metodo para la impresion de los datos en header y footer
	function showDates()
	{
		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM settings');
		$query->execute();

		$jDates = '';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
			$jDates = json_encode(array(
									'mailH' => $row['mailHeader'], 
									'phoneH' => $row['phoneHeader'],
									'addresF' => $row['addresFooter'],
									'mailF' => $row['mailFooter'],
									'phoneF' => $row['phoneFooter']));
		}

		$this->closeBD($bd);
		return $jDates;
	}



	//@Method::showContacto
	//@Autor::Alex Jimenez
	//@Metodo para la impresion de los datos en la pagina de contacto
	function showContacto()
	{
		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM contacto');
		$query->execute();

		$jContacto = '';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
			$jContacto = json_encode(array(
									'firstAddres' => $row['firstAddres'], 
									'secondAddres' => $row['secondAddres'],
									'thirdAddres' => $row['lastAddres'],
									'mailCont' => $row['mailContacto'],
									'phoneCont' => $row['phoneContacto'],
									'latCont' => $row['latContacto'],
									'longCont' => $row['longContacto']));
		}

		$this->closeBD($bd);
		return $jContacto;
	}


	

	//@Method::setSettings
	//@Autor::Alex Jimenez
	//@Metodo para la modificacion del orden de aparicion de los banners en el slider
	function setDates($arguments)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('UPDATE settings SET mailHeader = :mh, phoneHeader = :ph, addresFooter = :af, mailFooter = :mf, phoneFooter = :pf WHERE idSetting = :id');
		$run = $query->execute(array(
									':mh' => $arguments[0],
									':ph' => $arguments[1],
									':af' => $arguments[2],
									':mf' => $arguments[3],
									':pf' => $arguments[4],
									':id' => $arguments[5]));
		
		if($run == 1){ $response = json_encode(array('state' => 'succes', )); }  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}
		
		return $response;
		$this->closeBD($bd);
		
		
	}


	//@Method::setContacto
	//@Autor::Alex Jimenez
	//@Metodo para la modificacion del orden de aparicion de los banners en el slider
	function setContacto($arguments)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('UPDATE contacto SET firstAddres = :fa, secondAddres = :sa, lastAddres = :la, mailContacto = :mc, phoneContacto = :pc, mailForm = :mf, latContacto = :lc, longContacto = :loc WHERE idContacto = :id');
		$run = $query->execute(array(
									':fa' => $arguments[0],
									':sa' => $arguments[1],
									':la' => $arguments[2],
									':mc' => $arguments[3],
									':pc' => $arguments[4],
									':mf' => $arguments[5],
									':lc' => $arguments[6],
									':loc' => $arguments[7],
									':id' => $arguments[8]));
		
		if($run == 1){ $response = json_encode(array('state' => 'succes', )); }  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}
		
		return $response;
		$this->closeBD($bd);
		
		
	}

	


}

?>