<?php
require_once('BD.php');

//@Class::VISITAS
//@Autor::Alex Jimenez
//@Clase para la gestion de visitas
class Visitas extends BD
{

	private $folderSources;

	function __construct()
	{
	 	$this->folderSources = 'http://www.sundecdecoracion.com/sources/yates/';
	}


	//@Method::storeVisit
	//@Autor::Alex Jimenez
	//@Metodo que almacena una visita
	function storeVisit($page)
	{
		$hoy = date('Y-m-d');

		$bd = $this->openBD();
		$query = $bd->prepare('INSERT INTO visitas (pageVisit, dateVisit) VALUES (:page, :data)');
		$query->bindParam(':page', $page);
		$query->bindParam(':data', $hoy);
		$query->execute();

		
		
		
		
	}


	//@Method::shoTopVisit
	//@Autor::Alex Jimenez
	//@Metodo que obtiene la lista de paginas mas visitadas
	function showTopVisit()
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM visitas, yates, seo  WHERE visitas.pageVisit = seo.urlPage AND seo.idYacht = yates.idYacht GROUP BY pageVisit ORDER BY COUNT(pageVisit) DESC LIMIT 0,6');
		$query->execute();
		
		$rows = $query->rowCount();	

		if($rows > 0)
		{
			$viewHtml = '';
			while($row = $query->fetch(PDO::FETCH_ASSOC))
			{
				$viewHtml .= '<div>
									<a href="'.$row['urlPage'].'"><img src="'.$this->folderSources.$row['imageYacht'].'" /></a>
									<h4>'.$row['titleYacht'].'</h4>
							  </div>';
			}
			return $viewHtml;
		}
		else
		{
			return 'Unvisited pages even';
		}
		
		$this->closeBD($bd);	
	}


	

	


}

?>