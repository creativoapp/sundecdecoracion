<?php
require_once('BD.php');

//@Class::CATALOGO
//@Autor::Alex Jimenez
//@Clase para la gestion/creacion de categorias del catalogo
class Promociones extends BD
{

	private $folderSources;
	private $srcgalls;

	function __construct()
	{
	 	$this->folderSources = 'http://www.sundecdecoracion.com/sources/promos/';
	 	//$this->folderSources = 'http://www.webcancun.com.mx/sundec/sources/catalogo/';
	 	$this->srcgalls = 'http://www.sundecdecoracion.com/sources/promos/';
	 	//$this->srcgalls = 'sources/galerias/';
	}


	//@Method::listPages
	//@Autor::Alex Jimenez
	//@Metodo que lista las paginas creadas
	function listPromos()
	{
		$bd = $this->openBD();
		$filter = $bd->prepare('SELECT * FROM promos ORDER BY finicio DESC');
		$filter->execute();

		define('_PAGES', 30);
		if(isset($_GET['page']))
		{
		  	$page = $_GET['page'];
		}
		else
		{
		    $page = 1;
		}
		
		$inicio = ($page - 1) * _PAGES;
		$registros = $filter->rowCount();
		$noPages = ceil($registros / _PAGES);
		
		$query = $bd->prepare('SELECT * FROM promos ORDER BY finicio DESC LIMIT :inicio,'._PAGES.' ');		
		$query->bindParam('inicio', $inicio, PDO::PARAM_INT);
		$query->execute();

		$table = '<table class="tResults tPages searchResults">
				  <tr>
				  		<td>Promoción</td><td>Inicia</td><td>Termina</td><td>alt Banner</td><td>title Link</td><td colspan="2">&nbsp;</td>
				  </tr>';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

			$hide = $_SESSION['rol'] != 3 ? '' : 'style="display:none;"';
			
			$table .= '<tr>
							<td>'.$row['promo'].'</td>
							<td>'.$row['finicio'].'</td>
							<td>'.$row['ffin'].'</td>
							<td>'.$row['altPromo'].'</td>
							<td>'.$row['titlePromo'].'</td>
							<td><a href="#" '.$hide.' data-reveal-id="myModal" class="big-link promo" data-idpromo="'.$row['idpromo'].'" title="Editar Promoción"><img src="sources/edit-action.png" width="20"></a></td>
							<td><a href="deals?delpromo='.$row['idpromo'].'" '.$hide.' title="Eliminar Promoción"><img src="sources/delete-action.png" width="20"></a></td>
					   </tr>';
			}

		$table .= '</table>';

		if($noPages > 1)
			{
			    for($y = 1; $y <= $noPages; $y++ )
			    {
			        if($page == $y)
			        {
			            $table .= '<a class="page currentpag" href="deals?page='.$y.'">'.$y.'</a>';
			        }
			        else
			        {
			            $table .= '<a class="page" href="deals?page='.$y.'">'.$y.'</a>';
			        }
			    }
			}
		$this->closeBD($bd);
		return $table;
		
		
		
	}



	//@Method::showPromos
	//@Autor::Alex Jimenez
	//@Metodo para impresion de las promociones
	function showPromos()
	{
		$now = date('Y-m-d');

		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM promos WHERE finicio <= :now AND ffin >= :now  ORDER BY finicio DESC');
		$query->bindParam('now', $now);
		$run = $query->execute();
		
		if($run == 1)
		{
			$promos = '';
			$cont = 1;
			while ($row = $query->fetch(PDO::FETCH_ASSOC)) {


				$promos .= '<article class="columns eight">
								<h3>'.$row['promo'].'</h3>
								<div class="boxRow-promo">
									<a class="fancybox" rel="'.$row['promo'].'" href="'.$this->folderSources.$row['imgoverlay'].'">
										<img src="'.$this->folderSources.$row['imglist'].'" alt="'.$row['altPromo'].'" />
									</a>
									<div class="mask"></div>
									<div class="boxLink">
										<a href="'.$this->folderSources.$row['imgoverlay'].'" class="fancybox" rel="'.$row['promo'].'" title="'.$row['titlePromo'].'">VER PROMOCIÓN</a>
									</div>
								</div>
							</article>';
						
				if($cont == 2) { 
					$promos .= '<div class="clr"></div>';
					$cont = 1;
				} else {
					$cont++;
				}
			}
			return $promos;
		}
		else
		{
			$response = json_encode(array('state' => 'failed', 'message' => '<strong>Lo sentimos, ocurrio un problema. Por favor intentelo más tarde.</strong>' ));
		}
		
		return $promos;
		$this->closeBD($bd);
		
		
	}


	//@Method::insertPromo
	//@Autor::Alex Jimenez
	//@Metodo para agregar promociones
	public function insertPromo($args)
	{
		$bd = $this->openBD();
		$query = $bd->prepare('INSERT INTO promos (promo, imglist, imgoverlay, finicio, ffin) VALUES (:promo, :ilist, :iover, :inicio, :fin)');
		$run = $query->execute(array(
									'promo' => $args[0],
									'ilist' => $args[1],
									'iover' => $args[2],
									'inicio' => $args[3],
									'fin' => $args[4]));

		if($run == 1)
		{	
			$response = json_encode(array('state' => 'succes', 'message' => 'Se añadio la promoción correctamente.' ));	
		}
		else
		{
			$response = json_encode(array('state' => 'failed', 'message' => '<strong>Lo sentimos, ocurrio un problema. Por favor intentelo más tarde.</strong>' ));
		}

		return $response;
		$bd->closeBD();

	}



	//@Method::deletePromo
	//@Autor::Alex Jimenez
	//@Metodo para eliminar promociones
	public function deletePromo($id)
	{
		$bd = $this->openBD();
		$query = $bd->prepare('DELETE FROM promos WHERE idpromo = :id');
		$query->bindParam('id', $id);
		$exe = $query->execute();
			

		if($exe == 1){ $response = json_encode(array('state' => 'succes', )); }  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}

		return $response;
		$bd->closeBD();
	}


	//@Method::getInfoPromo
	//@Autor::Alex Jimenez
	//@Metodo que recupera la informacion de cada promocion
	function getInfoPromo($id)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM promos WHERE idpromo = :id');
		$query->bindParam(':id', $id);
		$run = $query->execute();

		if($run == 1)
		{
			$row = $query->fetch(PDO::FETCH_ASSOC);
			$response = json_encode(array(
										'rState' => 'succes',
										'promo' => $row['promo'],
										'inicio' => $row['finicio'],
										'fin' => $row['ffin'],
										'altt' => $row['altPromo'],
										'titlee' => $row['titlePromo']));
		}
		else
		{
			$response = json_encode(array('rState' => 'failed', 'message' => 'Ocurrio un problema, por favor intentelo más tarde.'));
		}

		return $response;
		$this->closeBD($bd);
	}


	//@Method::updatePromocion
	//@Autor::Alex Jimenez
	//@Metodo para modificar la promocion
	public function updatePromo($args)
	{
		$bd = $this->openBD();

		if($args[1] != NULL && $args[2] != NULL)
		{
			$query = $bd->prepare('UPDATE promos SET promo = :prom, imglist = :il, imgoverlay = :io, finicio = :fi, ffin = :ff, altPromo = :al, titlePromo = :ti WHERE idpromo = :id');
			$exe = $query->execute(array('prom' => $args[0],
										 'il' => $args[1],
										 'io' => $args[2],
										 'fi' => $args[3],
										 'ff' => $args[4],
										 'al' => $args[5],
										 'ti' => $args[6],
										 'id' => $args[7]));
		}
		else
		{
			if($args[1] != NULL){
				$query = $bd->prepare('UPDATE promos SET promo = :prom, imglist = :il, finicio = :fi, ffin = :ff, altPromo = :al, titlePromo = :ti WHERE idpromo = :id');
				$exe = $query->execute(array('prom' => $args[0],
										 'il' => $args[1],
										 'fi' => $args[3],
										 'ff' => $args[4],
										 'al' => $args[5],
										 'ti' => $args[6],
										 'id' => $args[7]));
			}
			elseif ($args[2] != NULL) {
				$query = $bd->prepare('UPDATE promos SET promo = :prom, imgoverlay = :io, finicio = :fi, ffin = :ff, altPromo = :al, titlePromo = :ti WHERE idpromo = :id');
				$exe = $query->execute(array('prom' => $args[0],
										 'io' => $args[2],
										 'fi' => $args[3],
										 'ff' => $args[4],
										 'al' => $args[5],
										 'ti' => $args[6],
										 'id' => $args[7]));	
			}
			else
			{
				$query = $bd->prepare('UPDATE promos SET promo = :prom, finicio = :fi, ffin = :ff, altPromo = :al, titlePromo = :ti WHERE idpromo = :id');
				$exe = $query->execute(array('prom' => $args[0],
										 'fi' => $args[3],
										 'ff' => $args[4],
										 'al' => $args[5],
										 'ti' => $args[6],
										 'id' => $args[7]));
			}
		}

		
			

		if($exe == 1){ $response = json_encode(array('state' => 'succes', )); }  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}

		return $response;
		$bd->closeBD();
	}




}

?>