<?php
require_once('BD.php');

//@Class::SLIDER
//@Autor::Alex Jimenez
//@Clase para la gestion de las imagenes del slider
class Slider extends BD
{

	private $folderSlider;

	function __construct()
	{
	 	//local
	 	$this->folderSlider = 'http://www.sundecdecoracion.com/sources/slider/';
		//online web-cancun
		//$this->folderSlider = 'http://www.webcancun.com.mx/sundec/sources/slider/';
	}
	//@Method::insertBanner
	//@Autor::Alex Jimenez
	//@Metodo para la insercion de banners en el slider	
	function tableSlider()
	{
		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM slider ORDER BY orderSlider ASC');
		$query->execute();


		$table = '<table class="tResults tSlider">
				  <tr>
				  		<td>Banner</td><td>alt Imagen</td><td>URL Link</td><td>title Link</td><td colspan="3">Orden</td>
				  </tr>';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

			$hide = $_SESSION['rol'] != 3 ? '' : 'style="display:none;"';
 
			$table .= '<tr>
							<td><img src="'.$this->folderSlider.$row['imageSlider'].'" width="150" /></td>
							<td>'.$row['altSlider'].'</td>
							<td>'.$row['urlToSlider'].'</td>
							<td>'.$row['titleSlider'].'</td>
							<td><input type="text" '.$hide.' class="slr-modOrder" data-idslider="'.$row['idSlider'].'" value="'.$row['orderSlider'].'"></td>
							<td><a href="#" '.$hide.' data-reveal-id="myModal" class="big-link slider" data-idslider="'.$row['idSlider'].'" title="Editar Slider"><img src="sources/edit-action.png" width="20"></a></td>
					   		<td><a href="slider?delete='.$row['idSlider'].'" '.$hide.' title="Eliminar"><img src="sources/delete-action.png" width="20"></a></td>
					   </tr>';
		}

		$table .= '</table>';
		$this->closeBD($bd);
		return $table;
		
		
	}


	//@Method::insertBanner
	//@Autor::Alex Jimenez
	//@Metodo para la insercion de banners en el slider
	function showSlider()
	{
		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM slider ORDER BY orderSlider ASC');
		$query->execute();

		$slider = '<div id="slider" class="nivoSlider theme-default">';
		$caption = '';

		while ($row = $query->fetch(PDO::FETCH_ASSOC)) 
		{
			/*if(empty($row['textSlider'])) { $title = ''; } else {
				$title = 'title="#'.$row['idSlider'].'"';
				
				$caption .= '<div id="'.$row['idSlider'].'" class="nivo-html-caption">
   								<p>'.$row['textSlider'].'</p>
   							</div>';
			}*/
			
			if(empty($row['altSlider'])){ $alt = ''; } else {
				$alt = 'alt="'.$row['altSlider'].'"';
			}

			if($row['linkSlider'] == 1){
				if(empty($row['titleSlider'])) { $titleLink = ''; } else {
					$titleLink = 'title="'.$row['titleSlider'].'"';
				}
				$slider .= '<a href="http://'.$row['urlToSlider'].'" '.$titleLink.'><img '.$alt.' src="'.$this->folderSlider.$row['imageSlider'].'"  /></a>';	
			}
			else
			{
				$slider .= '<img '.$alt.' src="'.$this->folderSlider.$row['imageSlider'].'"  />';
			}

			

		}

		$slider .= '</div>';
		$slider .= $caption;
		
		return $slider;

	    $this->closeBD($bd);
	}


	//@Method::insertBanner
	//@Autor::Alex Jimenez
	//@Metodo para la insercion de banners en el slider
	function insertBanner($arguments)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('INSERT INTO slider (imageSlider, textSlider, altSlider, linkSlider, urlToSlider, titleSlider, orderSlider) VALUES (:image, :texto, :alt, :link, :url, :title, :order)');
		$run = $query->execute(array(
					':image' => $arguments[0], 
                    ':texto' => $arguments[1],
                    ':alt' => $arguments[2],
                    ':link' => $arguments[3],
                    ':url' => $arguments[4],
                    ':title' => $arguments[5],
                    ':order' => $arguments[6]
                    ));
		
		if($run == 1)
		{
			$response = json_encode(array('state' => 'succes', 'message' => 'Se ha añadido el banner al slider correctamente.' ));
		}
		else
		{
			$response = json_encode(array('state' => 'failed', 'message' => '<strong>Lo sentimos, ocurrio un problema. Por favor intentelo más tarde.</strong>' ));
		}
		
		return $response;
		$this->closeBD($bd);
		
		
	}


	//@Method::setOrder
	//@Autor::Alex Jimenez
	//@Metodo para la modificacion del orden de aparicion de los banners en el slider
	function setOrder($orden, $idbanner)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('UPDATE slider SET orderSlider = :order WHERE idSlider = :banner');
		$run = $query->execute(array(':order' => $orden, ':banner' => $idbanner));
		
		if($run == 1){ $response = json_encode(array('state' => 'succes', 'message' => 'Se modifico el orden de aparición del banner correctamente.')); }  else {
			
			$response = json_encode(array('state' => 'failed', 'message' => 'error'));
		}
		
		return $response;
		$this->closeBD($bd);
		
		
	}


	//@Method::delBanner
	//@Autor::Alex Jimenez
	//@Metodo para eliminar banners del slider
	function delBanner($id)
	{
		$bd = $this->openBD();

		$stmt = $bd->prepare('DELETE FROM slider WHERE idSlider = :id');
		$stmt->bindParam('id', $id);
		$exe = $stmt->execute();

		if($exe == 1){ $response = json_encode(array('state' => 'succes', )); }  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}

		return $response;
		$bd->closeBD();
	}




	//@Method::getInfoSlider
	//@Autor::Alex Jimenez
	//@Metodo que recupera la informacion de cada slider
	function getInfoSlider($id)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM slider WHERE idSlider = :id');
		$query->bindParam(':id', $id);
		$run = $query->execute();

		if($run == 1)
		{
			$row = $query->fetch(PDO::FETCH_ASSOC);
			$response = json_encode(array(
										'state' => 'succes',
										'linkk' => $row['urlToSlider'],
										'altt' => $row['altSlider'],
										'titlee' => $row['titleSlider']));
		}
		else
		{
			$response = json_encode(array('state' => 'failed', 'message' => 'Ocurrio un problema, por favor intentelo más tarde.'));
		}

		return $response;
		$this->closeBD($bd);
	}


	//@Method::updateSlider
	//@Autor::Alex Jimenez
	//@Metodo para modificar banner
	public function updateSlider($args)
	{
		$bd = $this->openBD();

		if($args[0] != NULL)
		{
			$query = $bd->prepare('UPDATE slider SET imageSlider = :is, altSlider = :al, linkSlider = :ln, urlToSlider = :ur, titleSlider = :ti WHERE idSlider = :id');
			$exe = $query->execute(array('is' => $args[0],
										 'al' => $args[3],
										 'ln' => $args[1],
										 'ur' => $args[2],
										 'ti' => $args[4],
										 'id' => $args[5]));
		}
		else
		{
			$query = $bd->prepare('UPDATE slider SET altSlider = :al, linkSlider = :ln, urlToSlider = :ur, titleSlider = :ti WHERE idSlider = :id');
			$exe = $query->execute(array('al' => $args[3],
										 'ln' => $args[1],
										 'ur' => $args[2],
										 'ti' => $args[4],
										 'id' => $args[5]));
		}

		
			

		if($exe == 1){ $response = json_encode(array('state' => 'succes', )); }  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}

		return $response;
		$bd->closeBD();
	}

	


}

?>