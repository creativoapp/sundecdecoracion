<?php
require_once('BD.php');

//@Class::ACCESOS
//@Autor::Alex Jimenez
//@Clase para el control de acceso y salida del sistema
class Accesos extends BD
{
	
	private $user;
	private $pass;

	function __construct($username, $password)
	{
		$this->user = $username;
		$this->pass = $password;
	}

	
	//@Function::LOGIN
	//@Autor::Alex Jimenez
	//@Metodo para el acceso al sistema
	function login()
	{
		session_start();
		$passMd5 = MD5($this->pass);

		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM usuarios WHERE username = :user AND password = :pass');
		$query->bindParam('user', $this->user);
		$query->bindParam('pass', $passMd5);
		$query->execute();	
		
		$this->closeBD($bd);

		
		$rows = $query->rowCount();
		if($rows > 0)
		{
			
			while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
				$nameuser = $row['username'];
				$id = $row['idUsers'];
				$rol = 1/*$row['rolUser']*/;
			}

			if(isset($nameuser))
			{
				$_SESSION['sesion'] = $nameuser;
				$_SESSION['IDsesion'] = $id;
				$_SESSION['rol'] = $rol;	
			}

			$login = json_encode(array('login' => 'True'));
		} 
		else
		{
			$login = json_encode(array('login' => 'False'));	
		}

		return $login;
		
	}

	//@Function::LOGIN
	//@Autor::Alex Jimenez
	//@Metodo para salir del sistema
	function logout(){
		session_unset();
		session_destroy();
		@header('Location:../admin/');
	}

	


}

?>