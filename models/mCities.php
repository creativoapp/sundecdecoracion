<?php
require_once('BD.php');

//@Class::CITIES
//@Autor::Julian Canche
//@Clase de configuracion
class Cities extends BD
{
    //@Method::ValidateCity
	//@Autor::Julian Canche
	//@Metodo que valida una ciudad por su url
	function ValidateCity($url)
	{
		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM cities WHERE urlcity = :urlct AND statecity = 1');
		$query->execute(array('urlct' => $url));
        $val = ['status' => false, 'id' => null, 'name' => null, 'url' => $url];
        if ($query->rowCount() > 0) {
			$row = $query->fetch(PDO::FETCH_ASSOC);
            $val = ['status' => true, 'id' => $row['idcity'], 'name' => $row['namecity'], 'url' => $row['urlcity']];
        }
		$this->closeBD($bd);
		return $val;
	}

	//@Method::getCities
	//@Autor::Julian Canche
	//@Metodo que obtiene las ciudades activas
	function getCities($id = null)
	{
		$bd = $this->openBD();
		if ($id != null) {
			$query = $bd->prepare('SELECT * FROM cities WHERE idcity = :idc');
			$query->execute(array('idc' => $id));
		} else {
			$query = $bd->prepare('SELECT * FROM cities WHERE statecity = 1');
			$query->execute();
		}
        $rs = [];
        if ($query->rowCount() > 0) {
			while($row = $query->fetch(PDO::FETCH_ASSOC)){
				array_push($rs, [
					'id' => $row['idcity'],
					'name' => $row['namecity'],
					'url' => $row['urlcity']
				]);
			}
        }
		$this->closeBD($bd);
		return $rs;
	}
}