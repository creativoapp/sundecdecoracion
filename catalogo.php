<?php include('includes/header.php'); ?>


<section class="container catalogoPage">
	
	<section class="columns sixteen"><br>
		<h1>Catálogo de Decoración en <?= $city_obj['name'] ?></h1>
        
		<p>En nuestro <strong>Catálogo de Decoración</strong> por Sundec <?= $city_obj['name'] ?> encontrarás una gran variedad de productos y servicios para la Decoración de Interiores y Exteriores de Casas, Oficinas u Hoteles.</p>
        
        <p>Somos Profesionales de la Decoración brindando Soluciones creativas en todos los aspectos necesarios para crear un ambiente moderno y amigable</p>
        <p>Contamos con los siguientes Servicios de Decoración en <?= $city_obj['name'] ?>: Persianas y Cortinas, Toldos, Pisos de Ingeniería, madera, vinílicos, laminados, Decks Sintéticos y Naturales, Muebles, Telas para tapicería, cojines y cortinas, Papel Tapiz, Candiles, Lámparas Decorativas y Artículos Decorativos </p>
        <p>Sundec se ha dedicado desde hace varios años a brindar soluciones en decoración de interiores en <?= $city_obj['name'] ?> a cada cliente que busca un diseño nuevo para el hogar, despacho, estudio, oficina y cualquier ambiente.</p><br>
	</section>
	<div class="clr"></div>

	<div class="row">
		<?php 
			$catalogo = new Catalog();
			echo $catalogo->showCatalog($city_obj['id']);
		?>
	</div>

</section>
	
<?php include('includes/footer.php'); ?>